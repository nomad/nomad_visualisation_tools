# Visualisation tools for NOMAD

This Python package contains the experimental visualisation tools for NOMAD

## How to setup

Clone or download the visualisation tools from the repository. You can then either install the visualisation tools as a local package using `pip install -e <path to the visualisation tools>`.  The path should be to the folder that contains the setup.cfg file. Alternatively, you can install the required packages using the requirements.txt file via `pip install -r requirements.txt` (make sure your current directory is set to the folder that contains the requirements.txt file).

The visualisation tools need access to NOMAD. You can accomplish this is two ways:
1. Install NOMAD as a local package (`pip install -e <path to NOMAD>`)
2. Add the path to the NOMAD folder to the nomad_location.txt file (replace #NOMAD path). The NOMAD folder is the folder that contains the "NOMAD" folder and the "README.md" file.

## How to use the visualisation tools

The package contains three visualisation tools:
1. Results visualisation tool 
2. Infrastructure visualisation tool
3. Local route choice map visualisation tool


### Results visualisation tool
The results visualisation tool provides the ability to load simulation results and play them as an animation. You can load multiple results at the same time and play them simultaneously.

<img src="./images/results_tool.jpg" alt="picture of the results tool" style="height: auto; width:800px;"/>

### Infrastructure visualisation tool
The infrastucture visualisation tool provides the ability to load an infrastructure xml-file and display the infrastucture. (without having to run a simulation). Its main use is to visually inspect the infrastucture configured in the xml-file. 

<img src="./images/infra_tool.jpg" alt="picture of the infra tool" style="height: auto; width:800px;"/>

### Local route choice map visualisation tool
The local route choice map (lrcm) visualisation tool provides the ability to load the results of a simulation and display the route choice maps. With the slider you can change the destination whose map is shown. This destination is marked red. 

<img src="./images/lrcm_tool.jpg" alt="picture of the lrcm tool" style="height: auto; width:800px;"/>

### Starting the tools
If the visualisation tools have been installed as a local package the visualisation tools will be available as a command line tool. To open a tool use: `nomad_vis <tool>` where `<tool>` can be:
1. `results` or `r` for the results visualisation tool 
2. `infra` or `i` for the infrastructure visualisation tool
3. `lrcm` or `l` for the local route choice map visualisation tool

If NOMAD has not been installed as a local package you can use the following line to start the different tools (first move to the main folder of the visualisation tools):
1. `python start_results_gui.py` for the results visualisation tool 
2. `python start_infra_gui.py` for the infrastructure visualisation tool
3. `python start_lrcm_gui.py` for the local route choice map visualisation tool

# Acknowledgements
Technische Universiteit Delft hereby disclaims all copyright interest in the
program "NOMAD" A microscopic pedestrian simulation model
written by the Author(s).
Stefan Aarninkhof, Dean of Civil Engineering and Geosciences