""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from pathlib import Path
import traceback

from matplotlib.lines import Line2D
from matplotlib.patches import Polygon, Circle, Ellipse
from matplotlib.widgets import Slider, Button
import wx

from NOMAD.output_manager import DebugOutputManager, RCMD_INIT_TYPE, RCMD_FINAL_TYPE # @UnresolvedImport
import matplotlib as mpl
import matplotlib.pyplot as plt


class RcmdVisualizer():

    def __init__(self, scenDataFlNm=None, guiBackend=None, localDir=""):
        if guiBackend is not None:
            mpl.use(guiBackend)

        self.localDir = localDir

        self.fig = plt.figure('Rcmd vis - ')
        self.axInit = self.fig.add_axes((0.06, 0.2, 0.435, 0.73))
        self.axFinal = self.fig.add_axes((0.505, 0.2, 0.435, 0.73))
        self.axes = [self.axInit, self.axFinal]
        
        self.axFinal.yaxis.tick_right()
        self.axInit.get_shared_x_axes().join(self.axInit, self.axFinal)
        self.axInit.get_shared_y_axes().join(self.axInit, self.axFinal)
        
        self.cbAx = self.fig.add_axes((0.06, 0.94, 0.88, 0.04))
        
        self.loadButtonAx = self.fig.add_axes((0.05, 0.025, 0.12, 0.04))
        self.reloadButtonAx = self.fig.add_axes((0.20, 0.025, 0.12, 0.04))

        loadButton = Button(self.loadButtonAx, 'Load')
        loadButton.on_clicked(self.onLoad)
        reloadButton = Button(self.reloadButtonAx, 'Reload')
        reloadButton.on_clicked(self.onReload)

        self.destinationSliderAx = self.fig.add_axes((0.45, 0.07, 0.3, 0.04))
        self.maxCbValueSliderAx = self.fig.add_axes((0.45, 0.025, 0.3, 0.04))
        
        if scenDataFlNm is not None:
            self.load(scenDataFlNm)

        plt.show()

    def onLoad(self, event):
        with wx.FileDialog(None, "Open scenario file", defaultDir=self.localDir, wildcard="scenario files (*.scen)|*.scen",
                       style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            scenDataFlNm = fileDialog.GetPath()
            self.load(scenDataFlNm)

    def load(self, scenDataFlNm):
        try:
            self.clearAxes()
            
            scenDataFlNm = Path(scenDataFlNm)
            self.scenDataFlNm = scenDataFlNm

            self.fig.canvas.set_window_title('Rcmd vis - {}'.format(scenDataFlNm.name))

            self.scenData = DebugOutputManager.readScenarioDataFile(scenDataFlNm)

            rcmdDataFlNm = scenDataFlNm.parent.joinpath(self.scenData.rcmdFile)
            rcmdData, centerCoord = DebugOutputManager.readRcmdDataFile(rcmdDataFlNm)
            self.rcmdInitSets = []
            self.rcmdFinalSets = []
            self.destinations = []
            self.centerCoords = rcmdData
            for destinationID, costMatrix in rcmdData[RCMD_INIT_TYPE].items():
                self.destinations.append(destinationID)
                self.rcmdInitSets.append(costMatrix)
                self.rcmdFinalSets.append(rcmdData[RCMD_FINAL_TYPE][destinationID])

            for ax in self.axes:
                ax.set_xlim((self.scenData.geometry.extent[0], self.scenData.geometry.extent[2]))
                ax.set_ylim((self.scenData.geometry.extent[1], self.scenData.geometry.extent[3]))
                ax.set_aspect('equal')

            self.drawGeometry()

            self.destinationSlider = Slider(self.destinationSliderAx, label='Dest',
                                      valmin=0, valmax=len(self.destinations)-1,
                                      valinit=0, valstep=1)
            self.destinationSlider.on_changed(self.onDestinationSliderChange)
            self.onDestinationSliderChange(None)
        except:
            traceback.print_exc()
            self.scenDataFlNm = None
            self.clearAxes()
            self.axInit.text(0.5, 0.5, "An error occurred!", ha="center", va="center")            
            self.fig.canvas.draw_idle()

    def onReload(self, event):
        if self.scenDataFlNm is not None:
            self.load(self.scenDataFlNm)

    def onDestinationSliderChange(self, event):
        rcmdInd = int(self.destinationSlider.val)

        label = self.destinations[rcmdInd]
        self.destinationSlider.valtext.set_text(label)

        self.fig.canvas.draw_idle()

    def drawGeometry(self):
        for ax in self.axes:
            for walkLevel in self.scenData.geometry.walkLevels:
                for walkableArea in walkLevel.walkableAreas:
                    for coords in walkableArea:
                        pol = Polygon(coords, zorder=1, closed=True, fill=True,facecolor='whitesmoke', edgecolor='k')
                        ax.add_patch(pol)
    
                for obstacle in walkLevel.obstacles:
                    if obstacle.type == 'polygon':
                        drawObj = Polygon(obstacle.coords, zorder=3, closed=True, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')
                    elif obstacle.type == 'line':
                        drawObj = Line2D(obstacle.coords[0], obstacle.coords[1], zorder=3, color='k')
                    elif obstacle.type == 'circle':
                        drawObj = Circle(obstacle.centerCoord, radius=obstacle.radius, zorder=3, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')
                    elif obstacle.type == 'ellipse':
                        drawObj = Ellipse(obstacle.centerCoord, obstacle.semiAxesValues[0]*2, obstacle.semiAxesValues[1]*2,
                                           obstacle.rotation, zorder=3, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')
    
                    if obstacle.type == 'line':
                        ax.add_line(drawObj)
                    else:
                        ax.add_patch(drawObj)
    
            for destination in walkLevel.destinations:
                drawObj = getDestinationObj(destination)
                if destination.type == 'line':
                    ax.add_line(drawObj)
                else:
                    ax.add_patch(drawObj)
    
            for source in walkLevel.sources:
                if source.type == 'polygon':
                    drawObj = Polygon(source.coords, zorder=5, closed=True, fill=True, alpha=0.6, facecolor='0.3', edgecolor='k')
                    ax.add_patch(drawObj)
                elif source.type == 'line':
                    drawObj = Line2D(source.coords[0], source.coords[1], zorder=5, color='0.3')
                    ax.add_line(drawObj)

    def clearAxes(self):
        for ax in self.axes:
                ax.clear()
        self.destinationSliderAx.clear()
        self.maxCbValueSliderAx.clear()
        self.cbAx.clear()

def getDestinationObj(destination):
    hatch = 'x'
    color = '0.3'

    if destination.type == 'polygon':
        return Polygon(destination.coords, zorder=4, closed=True, fill=False, alpha=0.6, hatch=hatch, edgecolor=color)
    elif destination.type == 'line':
        return Line2D(destination.coords[0], destination.coords[1], zorder=4, color=color)
    
def visualize(scenDataFlNm=None, guiBackend='wxAgg'):
    RcmdVisualizer(scenDataFlNm, guiBackend)

if __name__ == '__main__':
    # Get argument in as filename
    RcmdVisualizer()