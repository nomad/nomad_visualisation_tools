""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from ast import literal_eval
from copy import deepcopy
from dataclasses import dataclass
import os
from pathlib import Path
from xml.dom.minidom import parse

from packaging import version

import numpy as np


# ==========================================================================================
def hasElement(collection, elNm, recursive=False):
    if recursive:
        return len(collection.getElementsByTagName(elNm)) > 0

    return len([element for element in collection.getElementsByTagName(elNm)
                         if element.parentNode == collection]) > 0

def hasAttribute(collection, key):
    return collection.hasAttribute(key)

# ==========================================================================================

def getAttribute(collection, key):
    return collection.getAttribute(key)

def getElementValue(element, key=None):
    if key is not None:
        elements = element.getElementsByTagName(key)
        if len(elements) == 0:
            raise Exception('Element is empty')
        if len(elements) > 1:
            raise Exception('Multiple elements')
        element = elements[0]

    return element.childNodes[0].data

def getElements(collection, key):
    return collection.getElementsByTagName(key)

def getSingleElement(collection, key):
    elXmls = collection.getElementsByTagName(key)
    if len(elXmls) != 1:
        raise Exception('The configuration should have exactly one {}!'.format(key))
    return elXmls[0]

# ==========================================================================================

def getString(collection, key=None):
    return str(getElementValue(collection, key))

def getFloat(collection, key=None):
    valueStr = getElementValue(collection, key)
    try:
        value = float(valueStr)
    except ValueError:
        if '*' in valueStr:
            valueStrs = valueStr.split('*')
            value = np.prod([float(valueStr) for valueStr in valueStrs])
        else:
            value = float(literal_eval(valueStr))

    return value

def getInt(collection, key=None):
    valueStr = getElementValue(collection, key)
    try:
        value = int(valueStr)
    except ValueError:
        if '*' in valueStr:
            valueStrs = valueStr.split('*')
            value = np.prod([int(valueStr) for valueStr in valueStrs])
        else:
            value = int(literal_eval(valueStr))

    return value

def getBoolean(collection, key=None):
    valueStr = getElementValue(collection, key)
    return bool(valueStr)

def getFloatTuple(collection, key=None):
    return getTupleEntry(collection, key, entryCastFcn=float)

def getListEntry(collection, key=None, castFcn=None, returnAsTuple=False, isNumerical=False):
    listStr = getElementValue(collection, key)
    listStr = "".join(listStr.split()) # Remove all whitespaces
    if listStr.startswith('('):
        # Remove all whitespace
        if listStr.count('(') != listStr.count(')'):
            raise Exception('Unbalanced parentheses "("={}, ")"={}\nList string:{}'.format(listStr.count('('), listStr.count(')'), listStr))

    listEntry = string2list(listStr, castFcn, returnAsTuple, isNumerical)

    return listEntry

def getNumericTuple(collection, key=None, castFcn=None):
    return getTupleEntry(collection, key, castFcn, True)

def getTupleEntry(collection, key=None, castFcn=None, isNumerical=False):
    return getListEntry(collection, key, castFcn, True, isNumerical)

def getFlNm(collection, key=None, checkExistence=True, pd=None):
    flNm = Path(getElementValue(collection, key))

    if pd is not None:
        pd = Path(pd)
        flNm = pd.joinpath(flNm)

    if checkExistence and not flNm.is_file():
        raise FileNotFoundError('The {} file "{}" does not exist!'.format(key, flNm))

    return flNm

def getPd(collection, key=None, createIfNonExisting=False, mainPd=None):
    pd = Path(getElementValue(collection, key))
    if mainPd is not None:
        mainPd = Path(mainPd)
        pd = mainPd.joinpath(pd)

    if createIfNonExisting and not pd.is_dir():
        pd.mkdir()
    elif not pd.is_dir():
        raise NotADirectoryError('The {} directory "{}" does not exist!'.format(key, pd))

    return pd

# ===============================================================================

def string2list(listStr, castFcn=None, returnAsTuple=False, isNumerical=False):
    if listStr.startswith('(') and listStr.endswith(')'):
        listStr = listStr[1:-1]

    startInd = 0
    entryList = []
    while startInd < len(listStr):
        if listStr[startInd] == '(':
            endInd = listStr.find(')', startInd) + 1
            while listStr[startInd:endInd].count('(') != listStr[startInd:endInd].count(')'):
                endInd = listStr.find(')', endInd) + 1

            entryList.append(string2list(listStr[startInd+1:endInd-1], castFcn, returnAsTuple, isNumerical))
        else:
            endInd = listStr.find(',', startInd)
            if endInd == -1:
                endInd = len(listStr)
            value = listStr[startInd:endInd]
            if isNumerical:
                value = castNumericalEntry(value, castFcn)
            elif castFcn is not None:
                value = castFcn(value)

            entryList.append(value)
        startInd = endInd + 1

    if returnAsTuple:
        entryList = tuple(entryList)
    return entryList

def getListElements(listElStr, delimiter=',', castFcn=None):
    listEntries = [x.strip() for x in listElStr.split(delimiter)]
    if castFcn is not None:
        listEntries = castListEntries(listEntries, castFcn)

    return listEntries

def castListEntries(listEntry, entryCastFcn):
    for ii in range(len(listEntry)):
        if isinstance(listEntry[ii], list):
            listEntry[ii] = castListEntries(listEntry[ii], entryCastFcn)
        else:
            listEntry[ii] = entryCastFcn(listEntry[ii])

    return listEntry

def castNumericalEntry(entryString, castFcn=None):
    if castFcn is not None:
        return castFcn(entryString)
    elif entryString.isdecimal():
        return int(entryString)
    elif '*' in entryString:
        entryStrs = entryString.split('*')
        return np.prod([float(entryStr) for entryStr in entryStrs])
    else:
        return float(entryString)

# ==========================================================================================


@dataclass(frozen=True)
class ElInfo():
    key: str
    type: str
    isOptional: bool = False
    args: tuple = ()

    def __post_init__(self):
        if self.type not in EL_TYPE_2_FCN:
            raise Exception('The type "{}" is not recognized!'.format(self.type))

STRING_TYPE = 'str'
FLOAT_TYPE = 'float'
INT_TYPE = 'int'
FLNM_TYPE = 'flNm'
PD_TYPE = 'pd'
BOOL_TYPE = 'bool'
LIST_TYPE = 'list'
TUPLE_TYPE = 'tuple'
NUMERIC_TUPLE_TYPE = 'numericTuple'

EL_TYPE_2_FCN = {
    STRING_TYPE: getString,
    FLOAT_TYPE: getFloat,
    INT_TYPE: getInt,
    FLNM_TYPE: getFlNm,
    PD_TYPE: getPd,
    BOOL_TYPE: getBoolean,
    LIST_TYPE: getListEntry,
    TUPLE_TYPE: getTupleEntry,
    NUMERIC_TUPLE_TYPE: getNumericTuple,
}

# =====================================================================================



def addMplConfigFromXml(config, collection, baseMplConfig=None):
    mplConfig = getMplConfigsFromXml(collection, baseMplConfig)
    config['mplConfig'] = mplConfig


def getFigLayoutConfigFromXml(collection, baseConfig=None):
    from axes_divider.xml_reader import GRID_DIVIDER_TYPE, AXES_GRID_TYPE, GRID_DIVIDER_FLD_NM, \
                                        AXES_GRID_FLD_NM

    figLayoutConfig = {GRID_DIVIDER_TYPE:None, AXES_GRID_TYPE:None}
    if len(getTopLayerConfigs(collection, GRID_DIVIDER_FLD_NM)):
        from axes_divider.xml_reader import getGridDividerConfigFromXml
        figLayoutConfig[GRID_DIVIDER_FLD_NM] = getGridDividerConfigFromXml(collection)
    elif len(getTopLayerConfigs(collection, AXES_GRID_FLD_NM)):
        dividerConfigsXml = getTopLayerConfigs(collection, AXES_GRID_FLD_NM)
        if len(dividerConfigsXml) != 1:
            raise Exception('Should contain exactly one dividerConfig!')
        from axes_divider.xml_reader import getAxesGridConfigFromXml
        figLayoutConfig['dividerGroup'] = getAxesGridConfigFromXml(dividerConfigsXml[0])

    if baseConfig is not None:
        addBaseConfigValue(figLayoutConfig, baseConfig)

    return figLayoutConfig

def addBaseConfigValue(config, baseConfig):
    for field, value in baseConfig.items():
        if isinstance(value, dict):
            if field not in config or not isinstance(config[field], dict):
                config[field] = {}
            addBaseConfigValue(config[field] , value)
        elif field not in config:
            config[field] = value

def getMplConfigsFromXml(collection, baseMplConfig=None):
    if baseMplConfig is None:
        # Should only be the case for the main mplConfig
        mplConfig = {'figure':{}, 'ax':{}, 'title':{}, 'horText':{}, 'verText':{}}
    else:
        mplConfig = deepcopy(baseMplConfig)

    mplConfigXmls = getTopLayerConfigs(collection, 'mplConfig')
    if not len(mplConfigXmls):
        return mplConfig

    for mplConfigXml in mplConfigXmls:
        mplEl = mplConfigXml.getAttribute('element')
        if mplConfigXml.hasAttribute('type'):
            mplType = mplConfigXml.getAttribute('type')
        else:
            mplType = None
        if mplConfigXml.hasAttribute('subType'):
            mplSubType = mplConfigXml.getAttribute('subType')
        else:
            mplSubType = None

        if mplEl not in mplConfig:
            mplConfig[mplEl] = {}

        mplConfigDict = mplConfig[mplEl]

        if mplType is not None:
            if mplType not in mplConfig[mplEl]:
                mplConfig[mplEl][mplType] = {}
            mplConfigDict = mplConfig[mplEl][mplType]

        if mplSubType is not None:
            if mplSubType not in mplConfig[mplEl][mplType]:
                mplConfig[mplEl][mplType][mplSubType] = {}
            mplConfigDict = mplConfig[mplEl][mplType][mplSubType]

        getMplConfigFromXml(mplConfigXml, mplConfigDict)

    return mplConfig

def getMplConfigFromXml(mplConfigXml, mplConfig={}):
    child = mplConfigXml.firstChild
    while child is not None:
        if child.localName is None:
            child = child.nextSibling
            continue

        fieldType = child.getAttribute('fieldType')
        mplConfig[child.localName] = getField(child.localName, fieldType, mplConfigXml)
        child = child.nextSibling

def getTopLayerConfigs(collection, name):
    configsXml = collection.getElementsByTagName(name)
    configXmls = []
    # Main figure config
    for configXmlLoop in configsXml:
        if configXmlLoop.parentNode.localName == collection.localName:
            configXmls.append(configXmlLoop)
    return configXmls

def getXmlListEntry(listStr, entryCastFcn=None, allList=None):
    if listStr == 'all':
        listEntry = allList
    elif listStr.startswith('[') or listStr.startswith('('):
        listEntry = literal_eval(listStr)
        if not isinstance(listEntry, (list, tuple)) and listStr.startswith('('):
            listEntry = (listEntry, )
    elif ', ' in listStr:
        listEntry = [x.strip() for x in listStr.split(',')]
    else:
        if entryCastFcn is not None:
            listEntry = [entryCastFcn(listStr), ]
        else:
            listEntry = [listStr, ]

    return listEntry

def getXmlBoolEntry(boolStr):
    return str(boolStr).lower() == 'true'

def getFlNmField(collection, fieldNm):
    flNm = getPathField(collection, fieldNm)
    assert(os.path.isfile(flNm)), 'The {} file "{}" does not exist!'.format(fieldNm, flNm)
    return flNm

def getPdField(collection, key, config=None):
    pd = getPathField(collection, key)
    assert(os.path.isdir(pd)), 'The {} directory "{}" does not exist!'.format(key, pd)
    if config is not None:
        config[key] = pd
    else:
        return pd

def getPathField(collection, key):
    path = str(getElementValue(collection, key))

    return path

def getExtFromXmlStr(collection, fieldNm):
    extXmlStr = getElementValue(collection, fieldNm)
    exts = getXmlListEntry(extXmlStr)
    for ii in range(len(exts)):
        if exts[ii][0] == '.':
            exts[ii] = exts[ii][1:]

    return exts

def elementIsNone(collection, key):
    return getElementValue(collection, key).lower() == 'none'

def getElementsValueWithAttr(collection, key, attrKey, valueType):
    values = {}
    els = collection.getElementsByTagName(key)

    for el in els:
        attValue = el.getAttribute(attrKey)
        if attValue in values:
            raise Exception('An element with attr "{}" already exists'.format(attValue))
        if valueType in TYPE_2_FCN:
            values[attValue] = TYPE_2_FCN[valueType](el.childNodes[0].data)
        else:
            values[attValue] = el.childNodes[0].data

    return values


MPL_CONFIG = 'mplConfig'

FL_NM_TYPE = 'flNm'
PD_TYPE = 'pd'
EXT_TYPE = 'ext'

EL_2_FCN = {
    MPL_CONFIG:addMplConfigFromXml,
    }

TYPE_2_FCN = {
    'str':str,
    'int':int,
    'float':float,
    'bool':getXmlBoolEntry,
    'list':getXmlListEntry,
    'literal':literal_eval
    }

DEF_FIELD_NM = 'def'
ALL_FIELD_NM = 'allList'
ENTRY_CAST_FCN = 'entryCastFcn'
CONDITIONAL = 'conditional'

MIN_SUPPORTED_VERSION = '2.0'

def readXml(xmlFlNm, contents):
    config = {}
    DOMTree = parse(xmlFlNm)
    collection = DOMTree.documentElement

    if not collection.hasAttribute('version'):
        raise Exception('No version information')
    versionStr = collection.getAttribute('version')
    if version.parse(versionStr) < version.parse(MIN_SUPPORTED_VERSION):
        raise Exception('Version "{}" is not supported. The current minimal version is "{}"'.format(versionStr, MIN_SUPPORTED_VERSION))

    getFields(contents['fields'], collection, config)
    addElements2config(contents['elements'], collection, config)

    return config

def getFields(fields, collection, config):
    fields = deepcopy(fields)
    fields2Rem = []
    field2fcn = getField2Fcn()
    for fieldNm in field2fcn:
        if fieldNm in fields:
            fields2Rem.append(fieldNm)
            field2fcn[fieldNm](config, collection)

    for fieldNm in fields2Rem:
        fields.remove(fieldNm)

    for field in fields:
        fieldNm = field[0]
        fieldType = field[1]
        if len(field) == 3:
            kwargs = field[2]
        else:
            kwargs = {}

        if DEF_FIELD_NM in kwargs:
            defValue = kwargs[DEF_FIELD_NM]
            kwargs.pop(DEF_FIELD_NM)
            if hasElement(collection, fieldNm):
                config[fieldNm] = getField(fieldNm, fieldType, collection, **kwargs)
            else:
                if fieldNm not in config:
                    config[fieldNm] = defValue
        else:
            config[fieldNm] = getField(fieldNm, fieldType, collection, **kwargs)

def getField(fieldNm, fieldType, collection, **kwargs):
    if elementIsNone(collection, fieldNm):
        return None

    if fieldType == PD_TYPE:
        return getPdField(collection, fieldNm)
    elif fieldType == FL_NM_TYPE:
        return getFlNmField(collection, fieldNm)
    elif fieldType == EXT_TYPE:
        return getExtFromXmlStr(collection, fieldNm)

    if len(kwargs) > 0:
        return TYPE_2_FCN[fieldType](getElementValue(collection, fieldNm), **kwargs)
    else:
        return TYPE_2_FCN[fieldType](getElementValue(collection, fieldNm))

def addElements2config(elements, collection, config):
    for el in elements:
        if isinstance(el, (list, tuple)):
            kwargs = el[1]
            el = el[0]
        else:
            kwargs = {}

        if CONDITIONAL in kwargs:
            if not config[kwargs[CONDITIONAL]]:
                continue
            else:
                kwargs.pop(CONDITIONAL)

        if el in EL_2_FCN:
            EL_2_FCN[el](config, collection, **kwargs)
        elif callable(el):
            el(config, collection, **kwargs)
        else:
            raise ValueError('Element should either be a predefined function or callable')

def getPlotConfigDefDict(defFields):
    defConfig = {}
    for field in defFields:
        if len(field) == 2:
            continue
        if DEF_FIELD_NM in field[2]:
            defConfig[field[0]] = field[2][DEF_FIELD_NM]

    return defConfig

def getField2Fcn():
    return {}
