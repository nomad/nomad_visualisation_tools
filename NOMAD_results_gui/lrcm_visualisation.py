""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from math import inf
from pathlib import Path
from numpy.lib.npyio import NpzFile

from matplotlib.lines import Line2D
from matplotlib.patches import Polygon, Circle, Ellipse
from matplotlib.pyplot import setp
from matplotlib.widgets import Slider, Button
import wx

from NOMAD.output_manager import SimulationFileOutputManager
import matplotlib as mpl
import matplotlib.pyplot as plt
import traceback

from NOMAD_results_gui.infra_visualisation import DEST_CIRCLE_RADIUS, DEST_COLOR, DEST_LINE_WIDTH, drawGeometry

DEST_SELECT_COLOR = 'r'
DEST_SELECT_LINE_WIDTH = 3

class LrcmVisualizer():

    def __init__(self, scenDataFlNm=None, guiBackend=None, localDir=""):
        if guiBackend is not None:
            mpl.use(guiBackend)

        self.localDir = localDir

        self.fig = plt.figure('Lrcm vis - ')
        self.fig.canvas.mpl_connect('key_press_event', self.on_press)
        self.ax = self.fig.add_axes((0.06, 0.2, 0.9, 0.75))

        self.loadButtonAx = self.fig.add_axes((0.05, 0.025, 0.12, 0.04))
        self.reloadButtonAx = self.fig.add_axes((0.20, 0.025, 0.12, 0.04))

        loadButton = Button(self.loadButtonAx, 'Load')
        loadButton.on_clicked(self.onLoad)
        reloadButton = Button(self.reloadButtonAx, 'Reload')
        reloadButton.on_clicked(self.onReload)

        self.lrcmSliderAx = self.fig.add_axes((0.45, 0.025, 0.3, 0.04))
        self.lrcmSlider = None
        self.lrcmQuiverHandle = None
        self.destinationHandles = {}
        self.curDestinationID = None
        
        if scenDataFlNm is not None:
            self.load(scenDataFlNm)

        plt.show()

    def onLoad(self, event):
        with wx.FileDialog(None, "Open scenario file", defaultDir=str(self.localDir), wildcard="scenario files (*.scen)|*.scen",
                       style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            scenDataFlNm = fileDialog.GetPath()
            self.load(scenDataFlNm)

    def load(self, scenDataFlNm):
        try:
            self.ax.clear()
            self.lrcmSliderAx.clear()

            scenDataFlNm = Path(scenDataFlNm)
            self.scenDataFlNm = scenDataFlNm

            self.fig.canvas.manager.set_window_title('Lrcm vis - {}'.format(scenDataFlNm.name))

            self.scenData = SimulationFileOutputManager.readScenarioDataFile(scenDataFlNm)

            lrcmDataFlNm = scenDataFlNm.parent.joinpath(self.scenData.lrcmFile)
            self.lrcmData = SimulationFileOutputManager.readLrcmDataFile(lrcmDataFlNm)
            self.lrcmSets = []
            if isinstance(self.lrcmData, NpzFile):
                for label in self.lrcmData.keys():
                    destinationID = None
                    for walkLevelID in  self.scenData.geometry.walkLevels._fields:
                        if walkLevelID in label:
                            destinationID = label.replace(f'{walkLevelID}_', '')
                            break
                    
                    self.lrcmSets.append((label, walkLevelID, destinationID))
                self.isNpzData = True
            else:
                self.isNpzData = False
                for walkLevelID, destinations in self.lrcmData.items():
                    for destinationID in destinations.keys():
                        label = '{} - {}'.format(walkLevelID, destinationID)
                        self.lrcmSets.append((label, walkLevelID, destinationID))

            self.ax.set_xlim((self.scenData.geometry.extent[0], self.scenData.geometry.extent[2]))
            self.ax.set_ylim((self.scenData.geometry.extent[1], self.scenData.geometry.extent[3]))
            self.ax.set_aspect('equal')

            drawGeometry(self, self.scenData.geometry.walkLevels)

            self.lrcmSlider = Slider(self.lrcmSliderAx, label='Lrcm',
                                      valmin=0, valmax=len(self.lrcmSets)-1,
                                      valinit=0, valstep=1)
            self.lrcmSlider.on_changed(self.onLrcmSliderChange)
            self.onLrcmSliderChange(None)
        except:
            traceback.print_exc()
            self.scenDataFlNm = None
            self.ax.clear()
            self.lrcmSliderAx.clear()
            self.ax.text(0.5, 0.5, "An error occurred!", ha="center", va="center")            
            self.fig.canvas.draw_idle()

    def onReload(self, event):
        if self.scenDataFlNm is not None:
            self.load(self.scenDataFlNm)

    def on_press(self, event):
        if event.key == 'left':
            if self.lrcmSlider is None:
                return
            lrcmInd = int(self.lrcmSlider.val)
            newInd = max(0, lrcmInd - 1)
            self.lrcmSlider.set_val(newInd)
        elif event.key == 'right':
            if self.lrcmSlider is None:
                return
            lrcmInd = int(self.lrcmSlider.val)
            newInd = min(len(self.lrcmSets)-1, lrcmInd + 1)
            self.lrcmSlider.set_val(newInd)

    def onLrcmSliderChange(self, event):
        lrcmInd = int(self.lrcmSlider.val)

        label = self.lrcmSets[lrcmInd][0]
        self.lrcmSlider.valtext.set_text(label)
        walkLevelID = self.lrcmSets[lrcmInd][1]
        destinationID = self.lrcmSets[lrcmInd][2]
            
        if self.isNpzData:
            x = self.lrcmData[label][:,0]
            y = self.lrcmData[label][:,1]
            u = self.lrcmData[label][:,2]
            v = self.lrcmData[label][:,3]
            
        else:
            x = self.lrcmData[walkLevelID][destinationID]['x']
            y = self.lrcmData[walkLevelID][destinationID]['y']
            u = self.lrcmData[walkLevelID][destinationID]['u']
            v = self.lrcmData[walkLevelID][destinationID]['v']

        if self.lrcmQuiverHandle is not None:
            self.lrcmQuiverHandle.remove()
        self.lrcmQuiverHandle = self.ax.quiver(x, y, u, v, angles='uv', zorder=5)

        if self.curDestinationID is not None:
            curDestHandle = self.destinationHandles[self.curDestinationID]
            if isinstance(curDestHandle, Circle):
                setp(curDestHandle, radius=DEST_CIRCLE_RADIUS, color=DEST_COLOR)
            else:
                setp(curDestHandle, linewidth=DEST_LINE_WIDTH, color=DEST_COLOR)

        destHandle =  self.destinationHandles[destinationID]  
        if isinstance(destHandle, Circle):
            setp(curDestHandle, radius=DEST_CIRCLE_RADIUS*DEST_SELECT_LINE_WIDTH, color=DEST_SELECT_COLOR)
        else:
            setp(destHandle, linewidth=DEST_SELECT_LINE_WIDTH, color=DEST_SELECT_COLOR)
        
        self.curDestinationID = destinationID

        self.fig.canvas.draw_idle()

def visualize(scenDataFlNm=None, guiBackend='wxAgg', localDir=""):
    LrcmVisualizer(scenDataFlNm, guiBackend, localDir)

if __name__ == '__main__':
    # Get argument in as filename
    LrcmVisualizer()