""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from copy import deepcopy

from NOMAD_results_gui.axes_divider import TOP_LOC, LEFT_GAP, RIGHT_GAP, H_GAP, BOTTOM_GAP, TOP_GAP, \
    V_GAP, TITLE_GAP, H_TEXT_GAP, V_TEXT_GAP
from NOMAD_results_gui.xml_config_base.xml2config import DEF_FIELD_NM, \
    getTopLayerConfigs, getFields


AXES_GRID_TYPE = 'axesGrid'
GRID_DIVIDER_TYPE = 'axesGridDivider'

GRID_DIVIDER_FLD_NM = 'gridDividerConfig'
AXES_GRID_FLD_NM = 'axesGrid'

GRID_DIVIDER_FIELDS = [
    ('horFactors', 'list'),
    ('verFactors', 'list'),
    ]

AUX_GRID_DIVIDER_FIELDS = [
    ('width', 'float'),
    ('gap', 'float', {DEF_FIELD_NM:0.0}),
    ('locStr', 'str', {DEF_FIELD_NM:TOP_LOC})
    ]

def getGridDividerConfigFromXml(collection):
    gridDividerConfigsXml = getTopLayerConfigs(collection, GRID_DIVIDER_FLD_NM)
    if len(gridDividerConfigsXml) > 1:
        raise Exception('A figure can only contain 1 GridDivider!')

    gridDividerConfigXml = gridDividerConfigsXml[0]
    gridDividerFields = GRID_DIVIDER_FIELDS
    if ignoreEmpty(gridDividerConfigXml):
        gridDividerFields = addDefNoneOption(gridDividerFields)

    config = {}
    getFields(gridDividerFields, gridDividerConfigXml, config, useNew=True)

    # Get aux config (1 ax )
    auxAxConfigsXml = collection.getElementsByTagName('auxAxConfig')
    if len(auxAxConfigsXml) > 1:
        raise Exception('A GridDivider scan only contain one aux ax config!')
    auxAxConfig = {}
    if len(auxAxConfigsXml) == 1:
        getFields(AUX_GRID_DIVIDER_FIELDS, auxAxConfigsXml[0], auxAxConfig, useNew=True)
    config['auxAx'] = auxAxConfig

    dividerConfigsXml = getTopLayerConfigs(gridDividerConfigXml, AXES_GRID_FLD_NM)
    if len(dividerConfigsXml) < 2:
        raise Exception('A GridDivider should at least contain two AxesGrids!')
    if len(dividerConfigsXml) > len(config['horFactors'])*len(config['verFactors']):
        raise Exception('The GridDivider does have fewer spaces than AxesGrids!')

    groupConfigs = []
    for axesGridConfigXml in dividerConfigsXml:
        groupConfigs.append(getAxesGridConfigFromXml(axesGridConfigXml, isChildOfGroup=True))

    config['dividerGroups'] = groupConfigs
    return config

AXES_GRID_FIELDS = [
    ('horFactors', 'list'),
    ('verFactors', 'list'),
    (LEFT_GAP, 'float', {DEF_FIELD_NM:0.1}),
    (RIGHT_GAP, 'float', {DEF_FIELD_NM:0.1}),
    (H_GAP, 'float', {DEF_FIELD_NM:0.1}),
    (BOTTOM_GAP, 'float', {DEF_FIELD_NM:0.1}),
    (TOP_GAP, 'float', {DEF_FIELD_NM:0.1}),
    (V_GAP, 'float', {DEF_FIELD_NM:0.1}),
    (TITLE_GAP, 'float', {DEF_FIELD_NM:None}),
    (H_TEXT_GAP, 'float', {DEF_FIELD_NM:None}),
    (V_TEXT_GAP, 'float', {DEF_FIELD_NM:None}),
    ('axes', 'list', {DEF_FIELD_NM:[]}),
    ('shareX', 'bool', {DEF_FIELD_NM:False}),
    ('shareY', 'bool', {DEF_FIELD_NM:False}),
    ('shareAx', 'str', {DEF_FIELD_NM:None}),
    ('legendInAx', 'bool', {DEF_FIELD_NM:False}),
    ('showAllYticks', 'bool', {DEF_FIELD_NM:True}),
    ('vTextString', 'str', {DEF_FIELD_NM:None}),
    ('hTextString', 'str', {DEF_FIELD_NM:None}),
    ('titleString', 'str', {DEF_FIELD_NM:None}),
    ]

def getAxesGridConfigFromXml(axesGridConfigXml, isChildOfGroup=False):
    axesGridFields = deepcopy(AXES_GRID_FIELDS)
    if isChildOfGroup:
        axesGridFields.append(('loc', 'list'))
        axesGridFields.append(('span', 'list', {DEF_FIELD_NM:(1,1)}))

    if ignoreEmpty(axesGridConfigXml):
        axesGridFields = addDefNoneOption(axesGridFields)

    config = {}
    getFields(axesGridFields, axesGridConfigXml, config, useNew=True)
    processAxes(config, axesGridConfigXml)
    addColorBarConfig(config, axesGridConfigXml)
    addLegendConfig(config, axesGridConfigXml)
    return config


DIVIDER_TYPE_2_FCN = {
    AXES_GRID_TYPE:getAxesGridConfigFromXml,
    GRID_DIVIDER_TYPE:getGridDividerConfigFromXml,
    }

AX_FIELDS = [
    ('loc', 'list'),
    ('span', 'list', {DEF_FIELD_NM:(1,1)}),
    ('containsLegend', 'bool', {DEF_FIELD_NM:False}),
    ('shareDataAx', 'str', {DEF_FIELD_NM:None}),
    ('shareX', 'bool', {DEF_FIELD_NM:False}),
    ('shareY', 'bool', {DEF_FIELD_NM:False}),
    ]

def processAxes(config, axesGridConfigXml):
    # If axes is not an empty list, add the axes in the list
    # from top left to bottom right (empty entry is a gap)
    axes = []
    if len(config['axes']):
        rowCnt = len(config['verFactors'])
        colCnt = len(config['horFactors'])
        if len(config['axes']) > rowCnt*colCnt:
            raise Exception('More axes than locations!')
        rowInd = rowCnt - 1
        colInd = 0
        for axID in config['axes']:
            if len(axID):
                axConfig = {'ID':axID, 'loc':(rowInd, colInd)}
                for axField in AX_FIELDS:
                    if axField[0] not in axConfig:
                        axConfig[axField[0]] = axField[2][DEF_FIELD_NM]
                if config['shareX'] or config['shareY']:
                    if len(axes) or config['shareAx'] is not None:
                        if config['shareAx'] is None:
                            axConfig['shareDataAx'] = axes[0]['ID']
                        else:
                            axConfig['shareDataAx'] = config['shareAx']
                        axConfig['shareX'] = config['shareX']
                        axConfig['shareY'] = config['shareY']

                axes.append(axConfig)

            colInd += 1
            if colInd >= colCnt:
                rowInd -= 1
                colInd = 0
    else:
        axesXml = axesGridConfigXml.getElementsByTagName('ax')
        for axXml in axesXml:
            axConfig = {}
            getFields(AX_FIELDS, axXml, axConfig, useNew=True)
            axConfig['ID'] = axXml.getAttribute('ID')
            if config['shareX'] or config['shareY']:
                if len(axes):
                    axConfig['shareDataAx'] = axes[0]['ID']
                    axConfig['shareX'] = config['shareX']
                    axConfig['shareY'] = config['shareY']

            axes.append(axConfig)

    if config['legendInAx']:
        for axConfig in axes:
            axConfig['containsLegend'] = True;
    config['axes'] = axes


AUX_FIELDS = [
    ('width', 'float'),
    ('gap', 'float', {DEF_FIELD_NM:None}),
    ('all', 'bool', {DEF_FIELD_NM:False}),
    ('locStr', 'str', {DEF_FIELD_NM:''}),
    ('loc', 'list', {DEF_FIELD_NM:[]}),
    ('span', 'list', {DEF_FIELD_NM:(1,1)}),
    ('orientation', 'str', {DEF_FIELD_NM:''}),
    ('alignment', 'str', {DEF_FIELD_NM:None}),
    ('axID', 'str', {DEF_FIELD_NM:''}),
    ]

def ignoreEmpty(configXml):
    return configXml.hasAttribute('ignoreEmpty') and \
        str(configXml.getAttribute('ignoreEmpty')).lower() == 'true'

def addDefNoneOption(fields):
    fields = deepcopy(fields)
    for ii in range(len(fields)):
        if len(fields[ii]) == 2:
            fields[ii] = (fields[ii][0], fields[ii][1], {DEF_FIELD_NM:None})
    return fields

def addColorBarConfig(config, axesGridConfigXml):
    colorBarConfigsXml = axesGridConfigXml.getElementsByTagName('colorBarConfig')

    if len(colorBarConfigsXml) == 1:
        cbConfig = {}
        getFields(AUX_FIELDS, colorBarConfigsXml[0], cbConfig, useNew=True)
    else:
        cbConfig = []
        for colorBarConfigXml in colorBarConfigsXml:
            cbConfigLoop = {}
            getFields(AUX_FIELDS, colorBarConfigXml, cbConfigLoop, useNew=True)
            cbConfig.append(cbConfigLoop)
    cbConfig = doAuxCheck(cbConfig)
    config['colorBarConfig'] = cbConfig

def addLegendConfig(config, axesGridConfigXml):
    legendConfigsXml = axesGridConfigXml.getElementsByTagName('legendConfig')
    legendFields = AUX_FIELDS + [('showAx', 'bool', {DEF_FIELD_NM:False}),
                                 ('labels', 'list', {DEF_FIELD_NM:[]})]

    if len(legendConfigsXml) == 1:
        legendConfig = {}
        getFields(legendFields, legendConfigsXml[0], legendConfig, useNew=True)
    else:
        legendConfig = []
        for legendConfigXml in legendConfigsXml:
            legendConfigLoop = {}
            getFields(legendFields, legendConfigXml, legendConfigLoop, useNew=True)
            legendConfig.append(legendConfigLoop)

    legendConfig = doAuxCheck(legendConfig)

    config['legendConfig'] = legendConfig

def doAuxCheck(auxConfig):
    if len(auxConfig) == 0:
        return None
    if isinstance(auxConfig, dict):
        # dict -> (all, locStr), (locStr), (loc, orientation)
        doAuxDictCheck(auxConfig)
    else:
        # list -> (locStr), (loc, orientation)
        for auxConfigLoop in auxConfig:
            doAuxDictCheck(auxConfigLoop, True)
    return auxConfig

def doAuxDictCheck(auxConfig, inList=False):
    if auxConfig['locStr'] != '' and len(auxConfig['loc']):
        raise Exception('Cannot define an auxiliary axes by both locStr and loc!')
    if auxConfig['locStr'] != '':
        auxConfig.pop('loc')
        auxConfig.pop('span')
        auxConfig.pop('orientation')
        auxConfig.pop('alignment')
        if inList:
            auxConfig.pop('all')
    elif len(auxConfig['loc']):
        auxConfig.pop('all')
        auxConfig.pop('locStr')