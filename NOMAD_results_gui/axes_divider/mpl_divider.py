""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from mpl_toolkits.axes_grid1.axes_divider import Divider
import matplotlib.transforms as mtransforms

class DividerInDivider(Divider):

    def __init__(self, fig, axes, *args, **kwargs):
        Divider.__init__(self, None, *args, **kwargs)
        self._fig = AxesAsFig(axes, fig)

    def get_position_runtime(self, ax, renderer):
        if self._locator is None:
            pos = self.get_position()
        else:
            pos = self._locator(ax, renderer).bounds

        mainAxPos = self._fig.axes.get_position()
        pos = list(pos)

        pos[0] += mainAxPos.x0
        pos[1] += mainAxPos.y0

        return pos

    def get_horizontal_sizes(self, renderer):
        convFactor = self._fig.getWidthConversionFactor()
        return [(s.get_size(renderer)[0]*convFactor, s.get_size(renderer)[1]) for s in self.get_horizontal()]

    def get_vertical_sizes(self, renderer):
        convFactor = self._fig.getHeightConversionFactor()
        return [(s.get_size(renderer)[0]*convFactor, s.get_size(renderer)[1]) for s in self.get_vertical()]

    def locate(self, nx, ny, nx1=None, ny1=None, axes=None, renderer=None):
        """
        Parameters
        ----------
        nx, nx1 : int
            Integers specifying the column-position of the
            cell. When *nx1* is None, a single *nx*-th column is
            specified. Otherwise location of columns spanning between *nx*
            to *nx1* (but excluding *nx1*-th column) is specified.
        ny, ny1 : int
            Same as *nx* and *nx1*, but for row positions.
        axes
        renderer
        """

        figW, figH = self._fig.get_size_inches()
        axW, axH = self._fig.getAxSizeIninches()
        x, y, w, h = self.get_position_runtime(axes, renderer)

        hsizes = self.get_horizontal_sizes(renderer)
        vsizes = self.get_vertical_sizes(renderer)
        k_h = self._calc_k(hsizes, axW*w)
        k_v = self._calc_k(vsizes, axH*h)

        if self.get_aspect():
            k = min(k_h, k_v)
            ox = self._calc_offsets(hsizes, k)
            oy = self._calc_offsets(vsizes, k)

            ww = (ox[-1] - ox[0])/figW
            hh = (oy[-1] - oy[0])/figH
            pb = mtransforms.Bbox.from_bounds(x, y, w, h)
            pb1 = mtransforms.Bbox.from_bounds(x, y, ww, hh)
            pb1_anchored = pb1.anchored(self.get_anchor(), pb)
            x0, y0 = pb1_anchored.x0, pb1_anchored.y0

        else:
            ox = self._calc_offsets(hsizes, k_h)
            oy = self._calc_offsets(vsizes, k_v)
            x0, y0 = x, y

        if nx1 is None:
            nx1 = nx+1
        if ny1 is None:
            ny1 = ny+1

        x1, w1 = x0 + ox[nx]/figW, (ox[nx1] - ox[nx])/figW
        y1, h1 = y0 + oy[ny]/figH, (oy[ny1] - oy[ny])/figH

        return mtransforms.Bbox.from_bounds(x1, y1, w1, h1)

class AxesAsFig():

    WIDTH_TYPE = 0
    HEIGHT_TYPE = 1

    def __init__(self, axes, fig):
        self.axes = axes
        self.fig = fig

    def get_size_inches(self):
        bbox = self.fig.get_window_extent().transformed(self.fig.dpi_scale_trans.inverted())
        return bbox.width, bbox.height

    def getAxSizeIninches(self):
        bbox = self.axes.get_position()
        figW, figH = self.get_size_inches()
        return bbox.width*figW, bbox.height*figH

    def getConversionFactor(self, geomType):
        width, height = self.getAxSizeIninches()
        figWidth, figHeight = self.get_size_inches()

        if geomType == self.WIDTH_TYPE:
            return width/figWidth
        elif geomType == self.HEIGHT_TYPE:
            return height/figHeight

    def getWidthConversionFactor(self):
        return 1 #self.getConversionFactor(self.WIDTH_TYPE)

    def getHeightConversionFactor(self):
        return 1 #self.getConversionFactor(self.HEIGHT_TYPE)