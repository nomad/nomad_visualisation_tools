""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from NOMAD_results_gui.axes_divider import CB_TYPE, LEGEND_TYPE, HOR_ORIENTATION, LEFT_ALIGN, \
    RIGHT_ALIGN, VER_ORIENTATION, BOTTOM_ALIGN, TOP_ALIGN
from NOMAD_results_gui.axes_divider.divider_grid import getDataAxLabel, createAxes
from NOMAD_results_gui.axes_divider.mpl_divider import DividerInDivider
import mpl_toolkits.axes_grid1.axes_size as Size


class AuxAx():
    def __init__(self, ID, loc, span, gap, width, orientation, alignment, auxType):
        self.ID = ID
        self.loc = loc
        self.span = span
        self.orientation = orientation

        self.cbAx = None
        self.cbGap = None
        self.cbWidth = None
        self.cbAlignment = None
        self.hasCb = False

        self.legendAx = None
        self.legendGap = None
        self.legendWidth = None
        self.legendAlignment = None
        self.hasLegend = False

        self.addType(gap, width, alignment, auxType)

    def addType(self, gap, width, alignment, auxType):
        if auxType == CB_TYPE:
            self.cbGap = gap
            self.cbWidth = width
            self.cbAlignment = alignment
            self.hasCb = True

        if auxType == LEGEND_TYPE:
            self.legendGap = gap
            self.legendWidth = width
            self.legendAlignment = alignment
            self.hasLegend = True

    def create(self, fig, parentLoc):
        axLabel = getDataAxLabel(parentLoc, self.loc, self.span)
        self.mainAx = createAxes(fig, axLabel + '_AUX', axIsVis=False)

        ver, hor, verCbInd, horCbInd, verLegendInd, horLegendInd = self.getSizeLists()
        divider = DividerInDivider(fig, self.mainAx, (0,0,1,1), hor, ver, aspect=False)
        if self.hasCb:
            self.cbAx = createAxes(fig, axLabel + '_CB')
            self.cbAx.set_label(axLabel + '_CB')
            self.cbAx.set_axes_locator(divider.new_locator(nx=horCbInd, ny=verCbInd))

        if self.hasLegend:
            self.legendAx = createAxes(fig, axLabel + '_legend', axIsVis=False)
            self.legendAx.set_label(axLabel + '_legend')
            self.legendAx.set_axes_locator(divider.new_locator(nx=horLegendInd, ny=verLegendInd))

    def isOfConfig(self, loc, span, orientation, auxType):
        if set(loc) != set(self.loc):
            return False

        if set(span) != set(self.span):
            raise Exception('Cannot have two auxiliary axes with the same location but different spans!')

        if orientation != self.orientation:
            raise Exception('Cannot have two auxiliary axes with the same location but different orientations!')

        if self.hasAuxType(auxType):
            raise Exception('Cannot have two auxiliary axes with the same location and equal aux types!')

        return True

    def hasAuxType(self, auxType):
        if auxType == CB_TYPE:
            return self.hasCb
        if auxType == LEGEND_TYPE:
            return self.hasLegend

        raise Exception('Unknown aux type "{}"!'.format(auxType))

    def getSizeLists(self):
        verCbInd = None
        horCbInd = None
        verLegendInd = None
        horLegendInd = None

        if self.orientation == HOR_ORIENTATION:
            hor = [Size.Scaled(1)]
            horCbInd = 0
            horLegendInd = 0
        else:
            if self.hasCb and self.hasLegend:
                hor = []
                if self.cbAlignment == LEFT_ALIGN and self.legendAlignment == LEFT_ALIGN:
                    horCbInd = addGap(hor, self.cbGap)
                    hor.append(Size.Fixed(self.cbWidth))
                    horLegendInd = horCbInd + 1 + addGap(hor, self.legendGap)
                    hor.append(Size.Fixed(self.legendWidth))
                elif self.cbAlignment == RIGHT_ALIGN and self.legendAlignment == RIGHT_ALIGN:
                    hor.append(Size.Scaled(1))
                    horLegendInd = 1
                    hor.append(Size.Fixed(self.legendWidth))
                    horCbInd = 2 + addGap(hor, self.legendGap)
                    hor.append(Size.Fixed(self.cbWidth))
                    addGap(hor, self.cbGap)
                elif self.cbAlignment == RIGHT_ALIGN and self.legendAlignment == LEFT_ALIGN:
                    horLegendInd = addGap(hor, self.legendGap)
                    hor.append(Size.Fixed(self.legendWidth))
                    hor.append(Size.Scaled(1))
                    hor.append(Size.Fixed(self.cbWidth))
                    horCbInd = horLegendInd + 2
                    addGap(hor, self.cbGap)
                elif self.cbAlignment == LEFT_ALIGN and self.legendAlignment == RIGHT_ALIGN:
                    horCbInd = addGap(hor, self.cbGap)
                    hor.append(Size.Fixed(self.cbWidth))
                    hor.append(Size.Scaled(1))
                    hor.append(Size.Fixed(self.legendWidth))
                    horLegendInd = horCbInd + 2
                    addGap(hor, self.legendGap)
            elif self.hasCb:
                if self.cbAlignment == RIGHT_ALIGN:
                    hor = [Size.Scaled(1), Size.Fixed(self.cbWidth)]
                    addGap(hor, self.cbGap)
                    horCbInd = 1
                else:
                    hor = []
                    horCbInd = addGap(hor, self.cbGap)
                    hor.append(Size.Fixed(self.cbWidth))
            else:
                if self.legendAlignment == RIGHT_ALIGN:
                    hor = [Size.Scaled(1), Size.Fixed(self.legendWidth)]
                    addGap(hor, self.legendGap)
                    horLegendInd = 1
                else:
                    hor = []
                    horLegendInd = addGap(hor, self.legendGap)
                    hor.append(Size.Fixed(self.legendWidth))


        if self.orientation == VER_ORIENTATION:
            ver = [Size.Scaled(1)]
            verCbInd = 0
            verLegendInd = 0
        else:
            if self.hasCb and self.hasLegend:
                ver = []
                if self.cbAlignment == BOTTOM_ALIGN and self.legendAlignment == BOTTOM_ALIGN:
                    verCbInd = addGap(ver, self.cbGap)
                    ver.append(Size.Fixed(self.cbWidth))
                    verLegendInd = verCbInd + 1 + addGap(ver, self.legendGap)
                    ver.append(Size.Fixed(self.legendWidth))
                elif self.cbAlignment == TOP_ALIGN and self.legendAlignment == TOP_ALIGN:
                    ver.append(Size.Scaled(1))
                    verLegendInd = 1
                    ver.append(Size.Fixed(self.legendWidth))
                    verCbInd = 2 + addGap(ver, self.legendGap)
                    ver.append(Size.Fixed(self.cbWidth))
                    addGap(ver, self.cbGap)
                elif self.cbAlignment == TOP_ALIGN and self.legendAlignment == BOTTOM_ALIGN:
                    verLegendInd = addGap(ver, self.legendGap)
                    ver.append(Size.Fixed(self.legendWidth))
                    ver.append(Size.Scaled(1))
                    ver.append(Size.Fixed(self.cbWidth))
                    verCbInd = verLegendInd + 2
                    addGap(ver, self.cbGap)
                elif self.cbAlignment == BOTTOM_ALIGN and self.legendAlignment == TOP_ALIGN:
                    verCbInd = addGap(ver, self.cbGap)
                    ver.append(Size.Fixed(self.cbWidth))
                    ver.append(Size.Scaled(1))
                    ver.append(Size.Fixed(self.legendWidth))
                    verLegendInd = verCbInd + 2
                    addGap(ver, self.legendGap)
            elif self.hasCb:
                if self.cbAlignment == TOP_ALIGN:
                    ver = [Size.Scaled(1), Size.Fixed(self.cbWidth)]
                    addGap(ver, self.cbGap)
                    verCbInd = 1
                else:
                    ver = []
                    verCbInd = addGap(ver, self.cbGap)
                    ver.append(Size.Fixed(self.cbWidth))
            else:
                if self.legendAlignment == TOP_ALIGN:
                    ver = [Size.Scaled(1), Size.Fixed(self.legendWidth)]
                    addGap(ver, self.legendGap)
                    verLegendInd = 1
                else:
                    ver = []
                    verLegendInd = addGap(ver, self.legendGap)
                    ver.append(Size.Fixed(self.legendWidth))

        return ver, hor, verCbInd, horCbInd, verLegendInd, horLegendInd

    def displayLabels(self):
        self.mainAx.text(0.5,0.5, 'Aux', ha='center', va='center')
        if self.orientation == VER_ORIENTATION:
            rot = 'vertical'
        else:
            rot = 'horizontal'
        if self.hasCb:
            self.cbAx.text(0.5,0.5, 'Colorbar', ha='center', va='center', rotation=rot)
        if self.hasLegend:
            self.legendAx.text(0.5,0.5, 'Legend', ha='center', va='center', rotation=rot)

def addGap(sizeList, gap):
    if gap is not None and gap > 0:
        sizeList.append(Size.Fixed(gap))
        return 1

    return 0