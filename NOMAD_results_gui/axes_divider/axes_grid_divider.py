""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from mpl_toolkits.axes_grid1.axes_divider import Divider

from NOMAD_results_gui.axes_divider import TOP_LOC, BOTTOM_LOC, LEFT_LOC, RIGHT_LOC
from NOMAD_results_gui.axes_divider.divider_grid import DividerGrid, createAxes
import mpl_toolkits.axes_grid1.axes_size as Size


class AxesGridDivider(DividerGrid):
    '''
    The grid which holds the different divider groups and on which the divider groups'
    location, span and variable factors are based.
    '''
    def __init__(self, verFactors, horFactors, auxAxConfig={}):
        super(AxesGridDivider, self).__init__(verFactors, horFactors)

        self.ver = [Size.Scaled(verFactor) for verFactor in verFactors]
        self.hor = [Size.Scaled(horFactor) for horFactor in horFactors]

        self.groups = []
        self.auxAx = None
        self.auxAxInfo = {}
        self.verAdd = 0
        self.horAdd = 0

        self.addAuxAx(auxAxConfig)

    def addGroups(self, groups):
        if not isinstance(groups, list):
            groups = [groups]

        self.groups += groups
        for group in groups:
            self.checkIfLocIsValid(group)

    def addAuxAx(self, auxAxConfig):
        if len(auxAxConfig) == 0:
            return

        if auxAxConfig['locStr'] == TOP_LOC:
            self.auxAxInfo = {'ny':len(self.ver), 'nx':0, 'ny1':len(self.ver) + 1, 'nx1':len(self.hor)}
            self.ver += [Size.Fixed(auxAxConfig['width'])]
        elif auxAxConfig['locStr'] == BOTTOM_LOC:
            self.auxAxInfo = {'ny':0, 'nx':0, 'ny1':1, 'nx1':len(self.hor)}
            self.ver = [Size.Fixed(auxAxConfig['width'])] + self.ver
            self.verAdd = 1
        elif auxAxConfig['locStr'] == LEFT_LOC:
            self.auxAxInfo = {'ny':0, 'nx':0, 'ny1':len(self.ver), 'nx1':1}
            self.horAdd = 1
            self.hor = [Size.Fixed(auxAxConfig['width'])] + self.hor
        elif auxAxConfig['locStr'] == RIGHT_LOC:
            self.auxAxInfo = {'ny':0, 'nx':len(self.hor), 'ny1':len(self.ver), 'nx1':len(self.hor) + 1}
            self.hor += [Size.Fixed(auxAxConfig['width'])]

    def createAndLocateAxes(self, fig, existingGroups=False):
        rect = (0,0,1,1)
        divider = Divider(fig, rect, self.hor, self.ver, aspect=False)
        for group in self.groups:
            axLabel = '{}_{}'.format(group.loc[0], group.loc[1])
            mainAx = createAxes(fig, axLabel, axIsVis=False)
            group.mainAx = mainAx

            kwargs = {'nx':group.loc[1] + self.horAdd,
                      'ny':group.loc[0] + self.verAdd}

            if group.span[1] > 1:
                kwargs['nx1'] = group.loc[1] + group.span[1] + self.horAdd

            if group.span[0] > 1:
                kwargs['ny1'] = group.loc[0] + group.span[0] + self.verAdd

            mainAx.set_axes_locator(divider.new_locator(**kwargs))
            if existingGroups:
                group.updateDividerMainAx(mainAx)
            else:
                group.createAndLocateAxes(fig, mainAx)

        if len(self.auxAxInfo):
            self.auxAx = createAxes(fig, 'auxAx', axIsVis=False)
            kwargs = {'nx':self.auxAxInfo['nx'],
                      'ny':self.auxAxInfo['ny'],
                      'nx1':self.auxAxInfo['nx1'],
                      'ny1':self.auxAxInfo['ny1'],}
            self.auxAx.set_axes_locator(divider.new_locator(**kwargs))

    def getDataAx(self, ID):
        for group in self.groups:
            dataAxes = group.getDataAx(ID)
            if dataAxes is not None:
                return dataAxes

    def setVerTextLabels(self, mplConfig={}, **kwargs):
        # Assumes the label text has been defined in the xml config
        for divGroup in self.groups:
            divGroup.setVerTextLabel(groupCall=True, mplConfig=mplConfig, **kwargs)

    def setHorTextLabels(self, mplConfig={}, **kwargs):
        # Assumes the label text has been defined in the xml config
        for divGroup in self.groups:
            divGroup.setHorTextLabel(groupCall=True, mplConfig=mplConfig, **kwargs)

    def setVerTextLabel(self, ID, label=None, mplConfig={}, **kwargs):
        divGroup = self.getAxDividerGroup(ID)
        divGroup.setVerTextLabel(label=label, mplConfig=mplConfig, **kwargs)

    def setHorTextLabel(self, ID, label=None, mplConfig={}, **kwargs):
        divGroup = self.getAxDividerGroup(ID)
        divGroup.setHorTextLabel(label=label, mplConfig=mplConfig, **kwargs)

    def hasVtext(self, ID):
        divGroup = self.getAxDividerGroup(ID)
        return divGroup.hasVtext()

    def hasHtext(self, ID):
        divGroup = self.getAxDividerGroup(ID)
        return divGroup.hasHtext()

    def setTitleLabels(self, mplConfig={}, **kwargs):
        # Assumes the label text has been defined in the xml config
        for divGroup in self.groups:
            divGroup.setTitleLabel(groupCall=True, mplConfig=mplConfig, **kwargs)

    def setTitleLabel(self, ID, label=None, mplConfig={}, **kwargs):
        divGroup = self.getAxDividerGroup(ID)
        divGroup.setTitleLabel(label=label, mplConfig=mplConfig, **kwargs)

    def getAxDividerGroup(self, ID):
        for group in self.groups:
            dataAxes = group.getDataAx(ID)
            if dataAxes is not None:
                return group

def adaptFactors(fig, horFactors, verFactors, groups, auxAxConfig={}):
        divider = AxesGridDivider(verFactors, horFactors, auxAxConfig)
        divider.addGroups(groups)
        divider.createAndLocateAxes(fig, existingGroups=True)

        # Create the new divider
        # Add the given groups
        # createAndLocateAxes but without creating the underlying groups (these are presumed to already have been created
        # For the groups -> Update the _fig attribute with the new mainAx entry
        # Delete the old axesGridDivider -> Necessary?? Probably not
        # return the new grid
        return divider
