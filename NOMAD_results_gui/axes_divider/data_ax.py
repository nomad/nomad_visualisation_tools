""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

class DataAx():

    def __init__(self, ID, loc=None, span=[1,1], ax=None, shareDataAx=None, shareX=False, shareY=False, mlpAxConfig={}, valueDict=None):
        self.ID = ID

        self.loc = loc # [row, col]
        self.span = span # [row, col]

        self.ax = ax
        self.cbAx = None
        self.legendAx = None
        self.shareDataAx = shareDataAx
        self.shareX = getShareBool(shareX)
        self.shareY = getShareBool(shareY)
        self.containsLegend = False
        self.legendLabels = []
        self.mlpAxConfig = mlpAxConfig

        if valueDict is not None:
            self.setValues(valueDict)

        if self.shareDataAx is not None:
            if self.shareDataAx.shareDataAx is not None:
                raise AttributeError('The shareAxConfig cannot contain a shareAxConfig itself')
        if (self.shareX or self.shareY) and self.shareDataAx is None:
            raise AttributeError('The ax config must contain a shareAxConfig if either shareX or shareY is True')

    def setValues(self, valueDict):
        for key, value in valueDict.items():
            if not hasattr(self, key):
                raise AttributeError('The ax config has no attribute "{}".'.format(key))
            else:
                setattr(self, key, value)

        if self.shareDataAx is None and (self.shareX is True or self.shareY is True):
            raise AttributeError('The ax config must contain a shareDataAx if either shareX or shareY is True')

def getShareBool(shareBool):
    if shareBool is None:
        return False

    return shareBool