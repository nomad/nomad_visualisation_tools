""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import numpy as np
import mpl_toolkits.axes_grid1.axes_size as Size

class DividerGrid():

    def __init__(self, verFactors, horFactors):
        self.verFactors = verFactors
        self.horFactors = horFactors
        self.occ = np.zeros((len(verFactors), len(horFactors)), dtype=bool)

    def checkIfLocIsValid(self, child):
        self.occ = _checkIfLocIsValid(self.occ, child.loc, child.span)

def _checkIfLocIsValid(parentOcc, childLoc, childSpan):
    assert(parentOcc[childLoc[0], childLoc[1]] == False), 'Location of the child is already occupied!'
    parentOcc[childLoc[0], childLoc[1]] = True

    for ii in range(1, childSpan[0]):
        assert(parentOcc[childLoc[0]+ii, childLoc[1]] == False), 'Vertical span of the child is already occupied!'
        parentOcc[childLoc[0]+ii, childLoc[1]] = True
        for jj in range(1, childSpan[1]):
            assert(parentOcc[childLoc[0], childLoc[1]+jj] == False), 'Horizontal span of the child is already occupied!'
            parentOcc[childLoc[0], childLoc[1]+jj] = True

    return parentOcc


# ===================================================================================================
# ===================================================================================================

def addGapToSizeList(sizeList, typeList, gap, gapType):
    if gap is not None and gap > 0:
        sizeList.append(Size.Fixed(gap))
        typeList.append(gapType)

    return sizeList, typeList


def createAxes(fig, axLabel, axConfig={}, axIsVis=True):
    ax = fig.add_axes((0,0,1,1), axLabel, **axConfig)
    ax.set_label(axLabel)
    if not axIsVis:
        ax.set_axis_off()

    return ax

def getDataAxLabel(groupLoc, axLoc, axSpan):
    return 'groupLoc:({},{})\naxLoc:({},{})\nspan:({},{})'.format(groupLoc[0], groupLoc[1], axLoc[0], axLoc[1],
                                      axSpan[0], axSpan[1])