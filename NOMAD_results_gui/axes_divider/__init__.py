""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

# Package wide constants
LEFT_LOC = 'left'
RIGHT_LOC = 'right'
BOTTOM_LOC = 'bottom'
TOP_LOC = 'top'
LOCATIONS = (LEFT_LOC, RIGHT_LOC, BOTTOM_LOC, TOP_LOC)

VER_ORIENTATION = 'vertical'
HOR_ORIENTATION = 'horizontal'
ORIENTATIONS = (VER_ORIENTATION, HOR_ORIENTATION)

LEFT_ALIGN = 'left'
RIGHT_ALIGN = 'right'
BOTTOM_ALIGN = 'bottom'
TOP_ALIGN = 'top'
ALIGNMENTS = (LEFT_ALIGN, RIGHT_ALIGN, BOTTOM_ALIGN, TOP_ALIGN)

LEGEND_TYPE = 'legend'
CB_TYPE = 'colorbar'

AUX_TYPE_2_CONFIG_NM = {
    CB_TYPE: 'colorBarConfig',
    LEGEND_TYPE: 'legendConfig',
    }

LEFT_GAP = 'leftGap'
RIGHT_GAP = 'rightGap'
H_GAP = 'hGap'
BOTTOM_GAP = 'bottomGap'
TOP_GAP = 'topGap'
V_GAP = 'vGap'
TITLE_GAP = 'titleGap'
H_TEXT_GAP = 'hTextGap'
V_TEXT_GAP = 'vTextGap'

GAPS = (LEFT_GAP, RIGHT_GAP, H_GAP, BOTTOM_GAP, TOP_GAP, V_GAP, TITLE_GAP, H_TEXT_GAP, V_TEXT_GAP)

LOC_2_GAP = {
    LEFT_LOC:LEFT_GAP,
    RIGHT_LOC:RIGHT_GAP,
    BOTTOM_LOC:BOTTOM_GAP,
    TOP_LOC:TOP_GAP
    }

AUX_GAP = 'gap'
WIDTH_OR_HEIGHT = 'widthOrHeight'

AUX_2_GAPS = {LEGEND_TYPE:{}, CB_TYPE:{}}
AUX_GAPS = []

for key, entryDict in AUX_2_GAPS.items():
    for gapId in [AUX_GAP, LEFT_GAP, RIGHT_GAP, BOTTOM_GAP, TOP_GAP]:
        auxGapID = '{}_{}'.format(key, gapId)
        AUX_2_GAPS[key][gapId] = auxGapID
        AUX_GAPS.append(auxGapID)
