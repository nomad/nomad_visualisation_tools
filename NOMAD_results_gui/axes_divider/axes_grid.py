""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from copy import deepcopy

from mpl_toolkits.axes_grid1.axes_divider import Divider

from NOMAD_results_gui.axes_divider import LEFT_LOC, RIGHT_LOC, BOTTOM_LOC, TOP_LOC, CB_TYPE, \
    LEGEND_TYPE, LOCATIONS, ORIENTATIONS, VER_ORIENTATION, LEFT_ALIGN, TOP_ALIGN, \
    ALIGNMENTS, RIGHT_ALIGN, HOR_ORIENTATION, BOTTOM_ALIGN, BOTTOM_GAP, \
    H_TEXT_GAP, WIDTH_OR_HEIGHT, AUX_2_GAPS, V_GAP, AUX_GAP, TOP_GAP, TITLE_GAP, \
    LEFT_GAP, V_TEXT_GAP, H_GAP, RIGHT_GAP, LOC_2_GAP, GAPS
from NOMAD_results_gui.axes_divider.aux_ax import AuxAx
from NOMAD_results_gui.axes_divider.divider_grid import createAxes, _checkIfLocIsValid, \
    getDataAxLabel, addGapToSizeList, DividerGrid
from NOMAD_results_gui.axes_divider.mpl_divider import DividerInDivider
import mpl_toolkits.axes_grid1.axes_size as Size


class AxesGrid(DividerGrid):
    '''
    Every group contains a a single horizontal and vertical size list. The only exception is the
    placement of a colorbar in a variable sized axes.
    '''
    def __init__(self, verFactors, horFactors, loc=[0,0], span=[1,1], valueDict=None, **kwargs):
        super(AxesGrid, self).__init__(verFactors, horFactors)

        self.loc = loc # [row, col]
        self.span = span # [row, col]

        self.dataAxes = None
        self.hTextAx = None
        self.vTextAx = None
        self.titleAx = None
        self.colorBarAxes = {}
        self.legendAxes = {}
        self.auxAxes = []

        self.leftGap = None
        self.rightGap = None
        self.hGap = None
        self.topGap = None
        self.bottomGap = None
        self.vGap = None

        self.titleGap = None
        self.hTextGap = None
        self.vTextGap = None

        self.vTextString = None
        self.hTextString = None
        self.titleString = None

        self.titleText = None

        self.showAllYticks = None

        # Either a dictionary with the fields (all, locStr, width, gap)
        # or a list of dictionaries with the fields (loc, span, width, gap, orientation, alignment)
        # whereby loc and span are within the given factors
        # or a list of dictionaries with the fields (locStr, width, gap)
        # whereby locStr is either left, right, top or bottom
        self.colorBarConfig = None
        self.cbForAllAxes = False
        self.cbLocOccupied = {LEFT_LOC:False, RIGHT_LOC:False,
                              BOTTOM_LOC:False, TOP_LOC:False}

        self.legendConfig = None
        self.legendForAllAxes = False
        self.legendLocOccupied = {LEFT_LOC:False, RIGHT_LOC:False,
                              BOTTOM_LOC:False, TOP_LOC:False}

        if valueDict is not None:
            setClassValues(self, valueDict, 'groupConfig')

        setClassValues(self, kwargs, 'groupConfig')

    def addDataAxes(self, dataAxes):
        if isinstance(dataAxes, (list, tuple)) and len(dataAxes) == 0:
            return
        if self.dataAxes is None:
            self.dataAxes = {}
        if not isinstance(dataAxes, (list, tuple)):
            dataAxes = [dataAxes]

        for dataAx in dataAxes:
            self.checkIfLocIsValid(dataAx)
            if dataAx.ID in self.dataAxes:
                raise Exception('A data ax with the ID "{}" has already been added!'.format(dataAx.ID))
            self.dataAxes[dataAx.ID] = dataAx

    def createAndLocateAxes(self, fig, parentAxes=None):
        #self.checkCbConfig()
        self.colorBarConfig, self.cbForAllAxes = checkAuxAxConfig(self.colorBarConfig, self.cbLocOccupied, self, CB_TYPE)
        self.legendConfig, self.legendForAllAxes = checkAuxAxConfig(self.legendConfig, self.legendLocOccupied, self, LEGEND_TYPE)
        self.createSizeLists()
        self.createAxes(fig)
        self.setAxLocations(fig, parentAxes)

    def createAxes(self, fig):
        reLabel, dataAxesOrdered  = self.getCreationOrder()
        for dataAx in reLabel:
            dataAx.ax.set_label(getDataAxLabel(self.loc, dataAx.loc, dataAx.span))

        for dataAx in dataAxesOrdered:
            axConfig = deepcopy(dataAx.mlpAxConfig)
            if dataAx.shareX:
                axConfig['sharex'] = dataAx.shareDataAx.ax
            if dataAx.shareY:
                axConfig['sharey'] = dataAx.shareDataAx.ax
            axLabel = getDataAxLabel(self.loc, dataAx.loc, dataAx.span)
            ax = createAxes(fig, axLabel, axConfig)
            dataAx.ax = ax
            if dataAx.containsLegend:
                dataAx.legendAx = ax
            if self.cbForAllAxes:
                axLabel += ' - CB'
                cbAx = createAxes(fig, axLabel)
                dataAx.cbAx = cbAx
            if self.legendForAllAxes:
                axLabel += ' - legend'
                legendAx = createAxes(fig, axLabel, axIsVis=self.legendConfig['showAx'])
                dataAx.legendAx = legendAx

        for auxAx in self.auxAxes:
            auxAx.create(fig, self.loc)

        if self.hTextAx is None and self.hasHtext():
            axLabel = '{}_{}_hText'.format(self.loc[0], self.loc[1])
            self.hTextAx = createAxes(fig, axLabel, axIsVis=False)
        if self.vTextAx is None and self.hasVtext():
            axLabel = '{}_{}_vText'.format(self.loc[0], self.loc[1])
            self.vTextAx = createAxes(fig, axLabel, axIsVis=False)
        if self.titleAx is None and self.hasTitle():
            axLabel = '{}_{}_title'.format(self.loc[0], self.loc[1])
            self.titleAx = createAxes(fig, axLabel, axIsVis=False)
        for loc in LOCATIONS:
            if self.cbLocOccupied[loc] and not self.cbForAllAxes:
                axLabel = '{}_{}_{}_CB'.format(self.loc[0], self.loc[1], loc)
                self.colorBarAxes[loc] = createAxes(fig, axLabel)
                self.addCbToDataAx(loc)
            if self.legendLocOccupied[loc] and not self.legendForAllAxes:
                axLabel = '{}_{}_{}_Legend'.format(self.loc[0], self.loc[1], loc)
                self.legendAxes[loc] = createAxes(fig, axLabel, axIsVis=self.legendConfig[loc]['showAx'])
                self.addLegendToDataAx(loc)

    def addCbToDataAx(self, loc):
        if 'axID' in self.colorBarConfig[loc]:
            dataAx = self.getDataAx(self.colorBarConfig[loc]['axID'])
            if dataAx is not None:
                dataAx.cbAx = self.colorBarAxes[loc]
        else:
            for dataAx in self.dataAxes.values():
                dataAx.cbAx = self.colorBarAxes[loc]

    def addLegendToDataAx(self, loc):
        dataAx = self.getDataAx(self.legendConfig[loc]['axID'])
        if dataAx is not None:
            dataAx.legendAx = self.legendAxes[loc]
            dataAx.legendLabels = self.legendConfig[loc]['labels']

    def getCreationOrder(self):
        # If the axConfig contains an already existing axes, assume it is either already
        # sharing its axis with another existing axes or is not sharing any axis
        dataAxesOrdered = []
        addLater = []
        reLabel = []

        axCreateCnt = 0
        for dataAx in self.dataAxes.values():
            if dataAx.ax is not None:
                reLabel.append(dataAx)
                continue

            if dataAx.shareDataAx is not None and (dataAx.shareDataAx.ax is None or dataAx.shareDataAx not in dataAxesOrdered):
                addLater.append(dataAx)
            else:
                dataAxesOrdered.append(dataAx)

            axCreateCnt += 1

        dataAxesOrdered += addLater
        return reLabel, dataAxesOrdered

    def setAxLocations(self, fig, parentAxes=None):
        rect =  (0,0,1,1)
        if parentAxes is not None:
            divider = DividerInDivider(fig, parentAxes, rect, self.hor, self.ver, aspect=False) #
        else:
            divider = Divider(fig, rect, self.hor, self.ver, aspect=False)

        # ----------------------------------------- TEXTS -----------------------------------------
        if self.hTextAx is not None:
            ny = 0 + self.hasBottomGap() + self.cbOffset(BOTTOM_LOC) + self.legendOffset(BOTTOM_LOC)
            nx, nx1 = self.getHorAxesSpan()
            self.hTextAx.set_axes_locator(divider.new_locator(nx=nx, nx1=nx1, ny=ny))

        if self.vTextAx is not None:
            ny, ny1 = self.getVerAxesSpan()
            nx = 0 + self.hasLeftGap() + self.cbOffset(LEFT_LOC) + self.legendOffset(LEFT_LOC)
            self.vTextAx.set_axes_locator(divider.new_locator(nx=nx, ny=ny, ny1=ny1))

        if self.titleAx is not None:
            ny = len(self.ver) -  (1 + self.hasTopGap())
            nx, nx1 = self.getHorAxesSpan() # Possibly center based on parent instead of the data axes
            self.titleAx.set_axes_locator(divider.new_locator(nx=nx, nx1=nx1, ny=ny))

        # --------------------------------------- COLORBARS ---------------------------------------
        if self.hasCb(LEFT_LOC):
            nx = 0 + self.hasLeftGap() + self.legendOffset(LEFT_LOC)
            ny, ny1 = self.getVerAxesSpan()
            self.colorBarAxes[LEFT_LOC].set_axes_locator(divider.new_locator(nx=nx, ny=ny, ny1=ny1))

        if self.hasCb(RIGHT_LOC):
            nx = len(self.hor) - (1 + self.hasRightGap() + self.legendOffset(RIGHT_LOC))
            ny, ny1 = self.getVerAxesSpan()
            self.colorBarAxes[RIGHT_LOC].set_axes_locator(divider.new_locator(nx=nx, ny=ny, ny1=ny1))

        if self.hasCb(BOTTOM_LOC):
            ny = 0 + self.hasBottomGap() + self.legendOffset(BOTTOM_LOC)
            nx, nx1 = self.getHorAxesSpan()
            self.colorBarAxes[BOTTOM_LOC].set_axes_locator(divider.new_locator(nx=nx, nx1=nx1, ny=ny))

        if self.hasCb(TOP_LOC):
            ny = len(self.ver) - (1 + self.hasTopGap() + self.hasTitle() + self.legendOffset(TOP_LOC))
            nx, nx1 = self.getHorAxesSpan()
            self.colorBarAxes[TOP_LOC].set_axes_locator(divider.new_locator(nx=nx, nx1=nx1, ny=ny))

        # ---------------------------------------- LEGENDS -----------------------------------------
        if self.hasLegend(LEFT_LOC):
            nx = 0 + self.hasLeftGap()
            ny, ny1 = self.getVerAxesSpan()
            self.legendAxes[LEFT_LOC].set_axes_locator(divider.new_locator(nx=nx, ny=ny, ny1=ny1))

        if self.hasLegend(RIGHT_LOC):
            nx = len(self.hor) - (1 + self.hasRightGap())
            ny, ny1 = self.getVerAxesSpan()
            self.legendAxes[RIGHT_LOC].set_axes_locator(divider.new_locator(nx=nx, ny=ny, ny1=ny1))

        if self.hasLegend(BOTTOM_LOC):
            ny = 0 + self.hasBottomGap()
            nx, nx1 = self.getHorAxesSpan()
            self.legendAxes[BOTTOM_LOC].set_axes_locator(divider.new_locator(nx=nx, nx1=nx1, ny=ny))

        if self.hasLegend(TOP_LOC):
            ny = len(self.ver) - (1 + self.hasTopGap() + self.hasTitle())
            nx, nx1 = self.getHorAxesSpan()
            self.legendAxes[TOP_LOC].set_axes_locator(divider.new_locator(nx=nx, nx1=nx1, ny=ny))

        # --------------------------------------- DATA AXES ----------------------------------------
        for dataAx in self.dataAxes.values():
            kwargs = {'nx':self.getHorInd(dataAx.loc[1]),
                      'ny':self.getVerInd(dataAx.loc[0])}

            if dataAx.span[1] > 1:
                kwargs['nx1'] = self.getHorInd(dataAx.loc[1] + dataAx.span[1]) - self.hasHGap()

            if dataAx.span[0] > 1:
                kwargs['ny1'] = self.getVerInd(dataAx.loc[0] + dataAx.span[0]) - self.hasVGap()

            dataAx.ax.set_axes_locator(divider.new_locator(**kwargs))
            self.setCbAxLocationForAll(dataAx, divider, kwargs)
            self.setLegendAxLocationForAll(dataAx, divider, kwargs)

        for auxAx in self.auxAxes:
            kwargs = {'nx':self.getHorInd(auxAx.loc[1]),
                  'ny':self.getVerInd(auxAx.loc[0])}

            if auxAx.span[1] > 1:
                kwargs['nx1'] = self.getHorInd(auxAx.loc[1] + auxAx.span[1]) - self.hasHGap()

            if auxAx.span[0] > 1:
                kwargs['ny1'] = self.getVerInd(auxAx.loc[0] + auxAx.span[0]) - self.hasVGap()

            auxAx.mainAx.set_axes_locator(divider.new_locator(**kwargs))

        self.divider = divider

    def setCbAxLocationForAll(self, dataAx, divider, kwargs):
        if not self.cbForAllAxes:
            return

        gap = self.colorBarConfig['gap']
        addFact = 1 + (gap is not None and gap > 0)

        if self.cbLocOccupied[LEFT_LOC]:
            kwargs['nx'] -= addFact
            if 'nx1' in kwargs:
                kwargs.pop('nx1')
        elif self.cbLocOccupied[RIGHT_LOC]:
            if 'nx1' in kwargs:
                kwargs['nx'] = kwargs['nx1'] + addFact
                kwargs.pop('nx1')
            else:
                kwargs['nx'] += addFact
        elif self.cbLocOccupied[BOTTOM_LOC]:
            kwargs['ny'] -= addFact
            if 'ny1' in kwargs:
                kwargs.pop('ny1')
        elif self.cbLocOccupied[TOP_LOC]:
            if 'ny1' in kwargs:
                kwargs['ny'] = kwargs['ny1'] + addFact
                kwargs.pop('ny1')
            else:
                kwargs['ny'] += addFact
        dataAx.cbAx.set_axes_locator(divider.new_locator(**kwargs))

    def setLegendAxLocationForAll(self, dataAx, divider, kwargs):
        if not self.legendForAllAxes:
            return
        gap = self.legendConfig['gap']
        addFact = 1 + (gap is not None and gap > 0)

        if self.legendLocOccupied[LEFT_LOC]:
            kwargs['nx'] -= addFact + self.getCbAddFact(LEFT_LOC)
            if 'nx1' in kwargs:
                kwargs.pop('nx1')
        elif self.legendLocOccupied[RIGHT_LOC]:
            if 'nx1' in kwargs:
                kwargs['nx'] = kwargs['nx1'] + addFact  + self.getCbAddFact(RIGHT_LOC)
                kwargs.pop('nx1')
            else:
                kwargs['nx'] += addFact + self.getCbAddFact(RIGHT_LOC)
        elif self.legendLocOccupied[BOTTOM_LOC]:
            kwargs['ny'] -= addFact + self.getCbAddFact(BOTTOM_LOC)
            if 'ny1' in kwargs:
                kwargs.pop('ny1')
        elif self.legendLocOccupied[TOP_LOC]:
            if 'ny1' in kwargs:
                kwargs['ny'] = kwargs['ny1'] + addFact  + self.getCbAddFact(TOP_LOC)
                kwargs.pop('ny1')
            else:
                kwargs['ny'] += addFact + self.getCbAddFact(TOP_LOC)
        dataAx.legendAx.set_axes_locator(divider.new_locator(**kwargs))

    def getCbAddFact(self, loc):
        if not self.cbForAllAxes:
            return 0

        cbGap = self.colorBarConfig['gap']
        cbGapAddFact = (cbGap is not None and cbGap > 0)

        if self.cbLocOccupied[loc]:
            return cbGapAddFact
        else:
            return 0

    def getVerInd(self, verLoc):
        return 0 + self.hasBottomGap() + self.hasHtext() + \
            self.legendOffset(BOTTOM_LOC) + self.cbOffset(BOTTOM_LOC) + \
            verLoc*(1 + self.hasVGap()) + \
            self.verCbFactor(verLoc) + self.verLegendFactor(verLoc)

    def getHorInd(self, horLoc):
        return 0 + self.hasLeftGap() + self.hasVtext() + \
            self.legendOffset(LEFT_LOC) + self.cbOffset(LEFT_LOC) + \
            horLoc*(1 + self.hasHGap()) + \
            self.horCbFactor(horLoc) + self.horLegendFactor(horLoc)

    def getVerAxesSpan(self):
        ny = 0 + self.hasBottomGap() + self.hasHtext() + \
                self.cbOffset(BOTTOM_LOC) + self.legendOffset(BOTTOM_LOC) + \
                self.legendForAllAxes*self.legendLocOccupied[BOTTOM_LOC] + \
                self.cbForAllAxes*self.cbLocOccupied[BOTTOM_LOC]
        ny1 = len(self.ver) - (self.hasTopGap() + self.hasTitle() + \
                               self.cbOffset(TOP_LOC) + self.legendOffset(TOP_LOC) + \
                               self.legendForAllAxes*self.legendLocOccupied[TOP_LOC] + \
                               self.cbForAllAxes*self.cbLocOccupied[TOP_LOC])
        return ny, ny1

    def getHorAxesSpan(self):
        nx = 0 + self.hasLeftGap() + self.hasVtext() + \
                self.cbOffset(LEFT_LOC) + self.legendOffset(LEFT_LOC) +\
                self.legendForAllAxes*self.legendLocOccupied[LEFT_LOC] + \
                self.cbForAllAxes*self.cbLocOccupied[LEFT_LOC]
        nx1 = len(self.hor) - (self.hasRightGap() + \
                               self.cbOffset(RIGHT_LOC) + self.legendOffset(RIGHT_LOC) + \
                               self.legendForAllAxes*self.legendLocOccupied[RIGHT_LOC] + \
                               self.cbForAllAxes*self.cbLocOccupied[RIGHT_LOC])
        return nx, nx1

    def hasLeftGap(self):
        return self.hasGap('leftGap')

    def hasRightGap(self):
        return self.hasGap('rightGap')

    def hasHGap(self):
        return self.hasGap('hGap')

    def hasBottomGap(self):
        return self.hasGap('bottomGap')

    def hasTopGap(self):
        return self.hasGap('topGap')

    def hasVGap(self):
        return self.hasGap('vGap')

    def hasHtext(self, ID=None): #@UnusedVariable
        return self.hasGap('hTextGap')

    def hasVtext(self, ID=None): #@UnusedVariable
        return self.hasGap('vTextGap')

    def hasTitle(self):
        return self.hasGap('titleGap')

    def hasTitleText(self):
        return self.titleText is not None

    def hasGap(self, gapType):
        gap = getattr(self, gapType)
        return gap is not None and gap > 0

    def hasCb(self, loc=None):
        if loc is None:
            # This assumes that the cbAx should be found in an auxAx
            for auxAx in self.auxAxes:
                if auxAx.cbAx is not None:
                    return True
            return False
        else:
            return self.cbLocOccupied[loc] and not self.cbForAllAxes

    def hasLegend(self, loc):
        return self.legendLocOccupied[loc] and not self.legendForAllAxes

    def hasVtextStr(self):
        return self.vTextString is not None

    def hasHtextStr(self):
        return self.hTextString is not None

    def getCbAx(self, loc=None):
        if loc is None:
            # This assumes that the cbAx should be found in an auxAx
            for auxAx in self.auxAxes:
                if auxAx.cbAx is not None:
                    return auxAx.cbAx

            if len(self.colorBarAxes):
                return self.colorBarAxes[list(self.colorBarAxes.keys())[0]]

            return False
        else:
            return self.colorBarAxes[loc]

    def cbOffset(self, loc):
        if not self.hasCb(loc):
            return 0
        gap = self.colorBarConfig[loc]['gap']
        return self.hasCb(loc) + (gap is not None and gap > 0)

    def legendOffset(self, loc):
        if not self.hasLegend(loc):
            return 0
        gap = self.legendConfig[loc]['gap']
        return self.hasLegend(loc) + (gap is not None and gap > 0)

    def horCbFactor(self, horLoc):
        if not self.cbForAllAxes:
            return 0

        gap = self.colorBarConfig['gap']
        multiplyFact = 1 + (gap is not None and gap > 0)

        if self.cbLocOccupied[LEFT_LOC]:
            return multiplyFact*(horLoc + 1)
        if self.cbLocOccupied[RIGHT_LOC]:
            return multiplyFact*horLoc

        return 0

    def horLegendFactor(self, horLoc):
        if not self.legendForAllAxes:
            return 0

        gap = self.legendConfig['gap']
        multiplyFact = 1 + (gap is not None and gap > 0)

        if self.legendLocOccupied[LEFT_LOC]:
            return multiplyFact*(horLoc + 1)
        if self.legendLocOccupied[RIGHT_LOC]:
            return multiplyFact*horLoc

        return 0

    def verCbFactor(self, verLoc):
        if not self.cbForAllAxes:
            return 0

        gap = self.colorBarConfig['gap']
        multiplyFact = 1 + (gap is not None and gap > 0)

        if self.cbLocOccupied[BOTTOM_LOC]:
            return multiplyFact*(verLoc + 1)
        if self.cbLocOccupied[TOP_LOC]:
            return multiplyFact*verLoc

        return 0

    def verLegendFactor(self, verLoc):
        if not self.legendForAllAxes:
            return 0

        gap = self.legendConfig['gap']
        multiplyFact = 1 + (gap is not None and gap > 0)

        if self.legendLocOccupied[BOTTOM_LOC]:
            return multiplyFact*(verLoc + 1)
        if self.legendLocOccupied[TOP_LOC]:
            return multiplyFact*verLoc

        return 0

    def createSizeLists(self):
        ver = []
        hor = []
        verTypes = []
        horTypes = []

        ver, verTypes = addGapToSizeList(ver, verTypes, self.bottomGap, BOTTOM_GAP)

        addAuxAxToBottom(ver, verTypes, LEGEND_TYPE, self.legendConfig, self.legendLocOccupied,
                         self.legendForAllAxes)
        addAuxAxToBottom(ver, verTypes, CB_TYPE, self.colorBarConfig, self.cbLocOccupied,
                         self.cbForAllAxes)

        ver, verTypes = addGapToSizeList(ver, verTypes, self.hTextGap, H_TEXT_GAP)

        for ii in range(len(self.verFactors)):
            addAuxAxToBottomOfAx(ver, verTypes, LEGEND_TYPE, self.legendConfig, self.legendLocOccupied, self.legendForAllAxes)
            addAuxAxToBottomOfAx(ver, verTypes, CB_TYPE, self.colorBarConfig, self.cbLocOccupied, self.cbForAllAxes)
            ver.append(Size.Scaled(self.verFactors[ii]))
            verTypes.append(WIDTH_OR_HEIGHT)
            addAuxAxToTopOfAx(ver, verTypes, CB_TYPE, self.colorBarConfig, self.cbLocOccupied, self.cbForAllAxes)
            addAuxAxToTopOfAx(ver, verTypes, LEGEND_TYPE, self.legendConfig, self.legendLocOccupied, self.legendForAllAxes)

            if ii < len(self.verFactors) - 1:
                ver, verTypes = addGapToSizeList(ver, verTypes, self.vGap, V_GAP)

        addAuxAxToTop(ver, verTypes, CB_TYPE, self.colorBarConfig, self.cbLocOccupied, self.cbForAllAxes)
        addAuxAxToTop(ver, verTypes, LEGEND_TYPE, self.legendConfig, self.legendLocOccupied, self.legendForAllAxes)
        ver, verTypes = addGapToSizeList(ver, verTypes, self.titleGap, TITLE_GAP)
        ver, verTypes = addGapToSizeList(ver, verTypes, self.topGap, TOP_GAP)

        hor, horTypes = addGapToSizeList(hor, horTypes, self.leftGap, LEFT_GAP)
        addAuxAxToLeft(hor, horTypes, CB_TYPE, self.legendConfig, self.legendLocOccupied, self.legendForAllAxes)
        addAuxAxToLeft(hor, horTypes, LEGEND_TYPE, self.colorBarConfig, self.cbLocOccupied, self.cbForAllAxes)

        hor, horTypes = addGapToSizeList(hor, horTypes, self.vTextGap, V_TEXT_GAP)

        for ii in range(len(self.horFactors)):
            addAuxAxToLeftOfAx(hor, horTypes, LEGEND_TYPE, self.legendConfig, self.legendLocOccupied, self.legendForAllAxes)
            addAuxAxToLeftOfAx(hor, horTypes, CB_TYPE, self.colorBarConfig, self.cbLocOccupied, self.cbForAllAxes)
            hor.append(Size.Scaled(self.horFactors[ii]))
            horTypes.append(WIDTH_OR_HEIGHT)
            addAuxAxToRightOfAx(hor, horTypes, CB_TYPE, self.colorBarConfig, self.cbLocOccupied, self.cbForAllAxes)
            addAuxAxToRightOfAx(hor, horTypes, LEGEND_TYPE, self.legendConfig, self.legendLocOccupied, self.legendForAllAxes)

            if ii < len(self.horFactors) - 1:
                hor, horTypes = addGapToSizeList(hor, horTypes, self.hGap, H_GAP)

        addAuxAxToRight(hor, horTypes, CB_TYPE, self.colorBarConfig, self.cbLocOccupied, self.cbForAllAxes)
        addAuxAxToRight(hor, horTypes, LEGEND_TYPE, self.legendConfig, self.legendLocOccupied, self.legendForAllAxes)

        hor, horTypes = addGapToSizeList(hor, horTypes, self.rightGap, RIGHT_GAP)

        self.ver = ver
        self.hor = hor
        self.verTypes = verTypes
        self.horTypes = horTypes

    def checkAuxAxConfigLoc(self, auxAxConfig, auxType):
        assert(auxAxConfig['orientation'] in ORIENTATIONS), 'Orientation value {} is not known'.format(auxAxConfig['orientation'])
        if 'alignment' not in auxAxConfig or auxAxConfig['alignment'] is None:
            if auxAxConfig['orientation'] == VER_ORIENTATION:
                auxAxConfig['alignment'] = LEFT_ALIGN
            else:
                auxAxConfig['alignment'] = TOP_ALIGN
        else:
            assert(auxAxConfig['alignment'] in ALIGNMENTS), 'Alignment value {} is not known'.format(auxAxConfig['alignment'])
        if auxAxConfig['orientation'] == VER_ORIENTATION:
            if auxAxConfig['alignment'] not in (LEFT_ALIGN, RIGHT_ALIGN):
                raise Exception('With a vertical orientation the alignment must be left or right, not "{}"'.format(auxAxConfig['alignment']))
        if auxAxConfig['orientation'] == HOR_ORIENTATION:
            if auxAxConfig['alignment'] not in (BOTTOM_ALIGN, TOP_ALIGN):
                raise Exception('With a horizontal orientation the alignment must be top or bottom, not "{}"'.format(auxAxConfig['alignment']))

        for auxAx in self.auxAxes:
            if auxAx.isOfConfig(auxAxConfig['loc'], auxAxConfig['span'], auxAxConfig['orientation'], auxType):
                auxAx.addType( auxAxConfig['gap'], auxAxConfig['width'],
                    auxAxConfig['alignment'], auxType)
                return

        self.occ = _checkIfLocIsValid(self.occ, auxAxConfig['loc'], auxAxConfig['span'])
        self.auxAxes.append(AuxAx(auxAxConfig['loc'], auxAxConfig['span'], auxAxConfig['gap'], auxAxConfig['width'],
                    auxAxConfig['orientation'], auxAxConfig['alignment'], auxType))

    def getDataAx(self, ID=None):
        if ID is not None:
            return self.dataAxes.get(ID)
        else:
            return list(self.dataAxes.values())[0]

    def displayLabelInAxes(self, axesOn=True):
        for dataAx in self.dataAxes.values():
            xLim = dataAx.ax.get_xlim()
            yLim = dataAx.ax.get_ylim()
            dataAx.ax.text((xLim[1] - xLim[0])/2.,(yLim[1] - yLim[0])/2, dataAx.ax.get_label(), ha='center', va='center')
            if dataAx.cbAx is not None:
                showAxAuxAx(dataAx.cbAx, self.cbLocOccupied, CB_TYPE)
            if dataAx.legendAx is not None:
                showAxAuxAx(dataAx.legendAx, self.legendLocOccupied, LEGEND_TYPE)
        if self.hTextAx is not None:
            if axesOn:
                self.hTextAx.set_axis_on()
            self.hTextAx.text(0.5,0.5, 'Hor. label', ha='center', va='center')
        if self.vTextAx is not None:
            if axesOn:
                self.vTextAx.set_axis_on()
            self.vTextAx.text(0.5,0.5, 'Var. label', ha='center', va='center', rotation='vertical')
        if self.titleAx is not None:
            if axesOn:
                self.titleAx.set_axis_on()
            self.titleAx.text(0.5,0.5, 'Title', ha='center', va='center')
        showAuxAxLabels(self.colorBarAxes, 'colorbar')
        showAuxAxLabels(self.legendAxes, 'legend')
        for auxAx in self.auxAxes:
            auxAx.displayLabels()

    def setVerTextLabel(self, groupCall=False, label=None, mplConfig={}, x=0.00, y=0.50, va='center', ha='left', rotation='vertical'):
        if not self.hasVtext():
            if groupCall:
                return
            else:
                raise Exception('The DividerGroup at ({}, {}) has no vText!'.format(self.loc[0], self.loc[1]))
        if label is None and self.vTextString is None:
            raise Exception('No label given for vText of the DividerGroup at ({}, {})!'.format(self.loc[0], self.loc[1]))
        if label is None:
            label = self.vTextString

        self.vTextAx.text(x, y, label, va=va, ha=ha, rotation=rotation,**mplConfig)

    def setHorTextLabel(self, groupCall=False, label=None, mplConfig={}, x=0.50, y=0.00, ha='center', va='bottom', rotation='horizontal'):
        if not self.hasHtext():
            if groupCall:
                return
            else:
                raise Exception('The DividerGroup at ({}, {}) has no hText!'.format(self.loc[0], self.loc[1]))
        if label is None and self.hTextString is None:
            raise Exception('No label given for the hText of the DividerGroup at ({}, {})!'.format(self.loc[0], self.loc[1]))
        if label is None:
            label = self.hTextString

        self.hTextAx.text(x, y, label, va=va, ha=ha, rotation=rotation,**mplConfig)

    def setVerTextLabels(self, mplConfig={}, **kwargs):
        # Assumes the label text has been defined in the xml config
        self.setVerTextLabel(groupCall=True, mplConfig=mplConfig, **kwargs)

    def setHorTextLabels(self, mplConfig={}, **kwargs):
        # Assumes the label text has been defined in the xml config
        self.setHorTextLabel(groupCall=True, mplConfig=mplConfig, **kwargs)

    def setTitleLabel(self, groupCall=False, label=None, mplConfig={}, x=0.50, y=1.00, ha='center', va='top', rotation='horizontal'):
        if not self.hasTitle():
            if groupCall:
                return
            else:
                raise Exception('The DividerGroup at ({}, {}) has no titleAx!'.format(self.loc[0], self.loc[1]))
        if label is None and self.titleString is None:
            raise Exception('No label given for the titleAx of the DividerGroup at ({}, {})!'.format(self.loc[0], self.loc[1]))
        if label is None:
            label = self.titleString

        self.titleText = self.titleAx.text(x, y, label, va=va, ha=ha, rotation=rotation,**mplConfig)

    def removeTitleLabel(self):
        if self.titleText is not None:
            self.titleText.remove()
            self.titleText = None
            self.titleString = None

    def updateTitleLabel(self, label):
        if self.titleText is not None:
            self.titleText.set_text(label)
            self.titleAx.draw_artist(self.titleText)

    def setTitleLabels(self, mplConfig={}, **kwargs):
        # Assumes the label text has been defined in the xml config
        self.setTitleLabel(groupCall=True, mplConfig=mplConfig, **kwargs)

    def adaptGapSizes(self, gaps2adapt):
        horSizes = self.divider.get_horizontal()
        verSizes = self.divider.get_vertical()
        for gapID, gapSize in gaps2adapt.items():
            self.adaptGapSizeInList(horSizes, self.horTypes, gapID, gapSize)
            self.adaptGapSizeInList(verSizes, self.verTypes, gapID, gapSize)

        self.divider.set_horizontal(horSizes)
        self.divider.set_vertical(verSizes)

    def adaptGapSizeInList(self, sizesList, typesList, gapID, gapSize):
        for ii in range(len(sizesList)):
            if typesList[ii] == gapID:
                sizesList[ii] = Size.Fixed(gapSize)

        return sizesList

    def getGaps(self):
        gaps = {}
        for gapID in GAPS:
            if getattr(self, gapID) is not None:
                gaps[gapID] = getattr(self, gapID)

        # Add gaps for the aux axes
        addAuxGap(gaps, self.colorBarConfig, CB_TYPE)
        return gaps

    def updateDividerMainAx(self, mainAx):
        if not isinstance(self.divider, DividerInDivider):
            raise Exception('The divider must be a "DividerInDivider"!')
        self.divider._fig.axes = mainAx

def addAuxGap(gaps, auxConfig, auxType):
    if auxConfig is None:
        return
    for LOC in LOCATIONS:
        if LOC in auxConfig:
            gaps[AUX_2_GAPS[auxType][LOC_2_GAP[LOC]]] = auxConfig[LOC]['gap']
        if 'gap' in auxConfig:
            gaps[AUX_2_GAPS[auxType][AUX_GAP]] = auxConfig['gap']

def showAxAuxAx(ax, locOccupied, typeStr):
    if locOccupied[LEFT_LOC] or locOccupied[RIGHT_LOC]:
        rot = 'vertical'
    else:
        rot = 'horizontal'
    ax.text(0.5,0.5, typeStr, ha='center', va='center', rotation=rot)

def showAuxAxLabels(auxAxes, typeStr):
    for loc, auxAx in auxAxes.items():
        if loc in (LEFT_LOC, RIGHT_LOC):
            rot = 'vertical'
        else:
            rot = 'horizontal'
        auxAx.text(0.5,0.5, typeStr, ha='center', va='center', rotation=rot)

def addAuxAxToBottom(ver, verTypes, auxType, auxAxConfig, auxAxLocOccupied, forAllAxes):
    if auxAxLocOccupied[BOTTOM_LOC] and not forAllAxes:
        ver.append(Size.Fixed(auxAxConfig[BOTTOM_LOC]['width']))
        verTypes.append(WIDTH_OR_HEIGHT)
        gap = auxAxConfig[BOTTOM_LOC]['gap']
        if gap is not None and gap > 0:
            ver.append(Size.Fixed(gap))
            verTypes.append(AUX_2_GAPS[auxType][BOTTOM_GAP])

def addAuxAxToBottomOfAx(ver, verTypes, auxType, auxAxConfig, auxAxLocOccupied, forAllAxes):
    if forAllAxes and auxAxLocOccupied[BOTTOM_LOC]:
        ver.append(Size.Fixed(auxAxConfig['width']))
        verTypes.append(WIDTH_OR_HEIGHT)
        gap = auxAxConfig['gap']
        if gap is not None and gap > 0:
            ver.append(Size.Fixed(gap))
            verTypes.append(AUX_2_GAPS[auxType][AUX_GAP])

def addAuxAxToTop(ver, verTypes, auxType, auxAxConfig, auxAxLocOccupied, forAllAxes):
    if auxAxLocOccupied[TOP_LOC] and not forAllAxes:
        gap = auxAxConfig[TOP_LOC]['gap']
        if gap is not None and gap > 0:
            ver.append(Size.Fixed(gap))
            verTypes.append(AUX_2_GAPS[auxType][AUX_GAP])
        ver.append(Size.Fixed(auxAxConfig[TOP_LOC]['width']))
        verTypes.append(WIDTH_OR_HEIGHT)

def addAuxAxToTopOfAx(ver, verTypes, auxType, auxAxConfig, auxAxLocOccupied, forAllAxes):
    if forAllAxes and auxAxLocOccupied[TOP_LOC]:
        gap = auxAxConfig['gap']
        if gap is not None and gap > 0:
            ver.append(Size.Fixed(gap))
            verTypes.append(AUX_2_GAPS[auxType][TOP_GAP])
        ver.append(Size.Fixed(auxAxConfig['width']))
        verTypes.append(WIDTH_OR_HEIGHT)

def addAuxAxToLeft(hor, horTypes, auxType, auxAxConfig, auxAxLocOccupied, forAllAxes):
    if auxAxLocOccupied[LEFT_LOC] and not forAllAxes:
        hor.append(Size.Fixed(auxAxConfig[LEFT_LOC]['width']))
        horTypes.append(WIDTH_OR_HEIGHT)
        gap = auxAxConfig[LEFT_LOC]['gap']
        if gap is not None and gap > 0:
            hor.append(Size.Fixed(gap))
            horTypes.append(AUX_2_GAPS[auxType][LEFT_GAP])

def addAuxAxToLeftOfAx(hor, horTypes, auxType, auxAxConfig, auxAxLocOccupied, forAllAxes):
    if forAllAxes and auxAxLocOccupied[LEFT_LOC]:
        hor.append(Size.Fixed(auxAxConfig['width']))
        horTypes.append(WIDTH_OR_HEIGHT)
        gap = auxAxConfig['gap']
        if gap is not None and gap > 0:
            hor.append(Size.Fixed(gap))
            horTypes.append(AUX_2_GAPS[auxType][AUX_GAP])

def addAuxAxToRight(hor, horTypes, auxType, auxAxConfig, auxAxLocOccupied, forAllAxes):
    if auxAxLocOccupied[RIGHT_LOC] and not forAllAxes:
        gap = auxAxConfig[RIGHT_LOC]['gap']
        if gap is not None and gap > 0:
            hor.append(Size.Fixed(gap))
            horTypes.append(AUX_2_GAPS[auxType][AUX_GAP])
        hor.append(Size.Fixed(auxAxConfig[RIGHT_LOC]['width']))
        horTypes.append(WIDTH_OR_HEIGHT)

def addAuxAxToRightOfAx(hor, horTypes, auxType, auxAxConfig, auxAxLocOccupied, forAllAxes):
    if forAllAxes and auxAxLocOccupied[RIGHT_LOC]:
        gap = auxAxConfig['gap']
        if gap is not None and gap > 0:
            hor.append(Size.Fixed(gap))
            horTypes.append(AUX_2_GAPS[auxType][RIGHT_GAP])
        hor.append(Size.Fixed(auxAxConfig['width']))
        horTypes.append(WIDTH_OR_HEIGHT)

def setClassValues(classInstance, valueDict, className):
        for key, value in valueDict.items():
            if not hasattr(classInstance, key):
                raise AttributeError('The {} has no attribute "{}".'.format(className, key))
            else:
                setattr(classInstance, key, value)

def checkAuxAxConfig(auxAxConfig, auxAxOccupied, divider, auxType):
    axForAllAxes = False
    if auxAxConfig is None:
        return auxAxConfig, axForAllAxes

    if isinstance(auxAxConfig, dict):
        if 'loc' in auxAxConfig:
            divider.checkAuxAxConfigLoc(auxAxConfig, auxType)
        elif 'all' in auxAxConfig and auxAxConfig['all']:
            axForAllAxes = True
            checkCbConfigLocStr(auxAxConfig, auxAxOccupied)
        else:
            checkCbConfigLocStr(auxAxConfig, auxAxOccupied)
            auxAxConfig = {auxAxConfig['locStr']:auxAxConfig}
    elif isinstance(auxAxConfig, list):
            if 'loc' in auxAxConfig[0]:
                for ii in range(len(auxAxConfig)):
                    divider.checkAuxAxConfigLoc(auxAxConfig[ii], auxType)
            elif 'locStr' in auxAxConfig[0]:
                auxAxConfigNew = {}
                for ii in range(len(auxAxConfig)):
                    checkCbConfigLocStr(auxAxConfig[ii], auxAxOccupied)
                    auxAxConfigNew[auxAxConfig[ii]['locStr']] = auxAxConfig[ii]
                auxAxConfig = auxAxConfigNew
            else:
                raise Exception('Unknown {} config.'.format(auxType))

    return auxAxConfig, axForAllAxes


def checkCbConfigLocStr(auxAxConfig, auxAxOccupied):
    assert(auxAxConfig['locStr'] in LOCATIONS), 'Location value {} is not known'.format(auxAxConfig['locStr'])
    assert(auxAxOccupied[auxAxConfig['locStr']] == False), 'The location {} is already occupied'.format(auxAxConfig['locStr'])
    auxAxOccupied[auxAxConfig['locStr']] = True