""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from pathlib import Path
import logging.config
from logging.handlers import RotatingFileHandler

class MakeRotatingFileHandler(RotatingFileHandler):
    def __init__(self, filename, *args, **kwargs):
        filenamePath = Path(filename).resolve()
        filenamePath.parent.mkdir(parents=True, exist_ok=True)
        super().__init__(filename, *args, **kwargs)
        
logging_config_file = Path(__file__).parent.joinpath('logging.ini')
logging.config.fileConfig(logging_config_file, disable_existing_loggers=True)

try:
    import NOMAD
except:
    nomadLocationFile = Path(__file__).parent.joinpath('nomad_location.txt')
    with open(nomadLocationFile) as f:
        nomadLocation = Path(f.readline())
    if not nomadLocation.is_dir():
        raise Exception(f'The path in "nomad_location.txt" is not a valid path! "{nomadLocation}"') 
    
    if not nomadLocation.joinpath('nomad_model.py').is_file():
        raise Exception(f'The path in "nomad_location.txt" is not a valid path! The file nomad_model.py was not found in this folder! "{nomadLocation}"') 
    import sys
    sys.path.append(str(nomadLocation))
    import NOMAD    
