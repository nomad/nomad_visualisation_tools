""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from math import inf
from pathlib import Path

from matplotlib.lines import Line2D
from matplotlib.patches import Polygon, Circle, Ellipse
from matplotlib.widgets import Button
import wx
import numpy as np

from NOMAD.input_manager import createWalkLevels, createSources
from NOMAD.output_manager import getWalkableAreaCoords
from NOMAD.xml_scenario_input import parseInfraXml
from NOMAD.activities import LineDestination, PolygonDestination, \
    MultiPolygonDestination, PointDestination, MultiPointInRectangleDestination
from NOMAD.demand_manager import LineSource, RectangleSource
from NOMAD.obstacles import LineObstacle, PolygonObstacle, CircleObstacle, \
    EllipseObstacle
import matplotlib as mpl
import matplotlib.pyplot as plt
import traceback

DEST_COLOR = 'b'
SOURCE_COLOR = 'm'
DEST_LINE_WIDTH = 2
DEST_CIRCLE_RADIUS = 0.05

class InfraVisualizer():

    def __init__(self, infraXmlFlNm=None, guiBackend=None, localDir=""):
        if guiBackend is not None:
            mpl.use(guiBackend)

        self.localDir = localDir

        self.fig = plt.figure('Infra vis - ')
        self.ax = self.fig.add_axes((0.06, 0.2, 0.9, 0.75))

        self.loadButtonAx = self.fig.add_axes((0.05, 0.025, 0.12, 0.04))
        self.reloadButtonAx = self.fig.add_axes((0.20, 0.025, 0.12, 0.04))

        loadButton = Button(self.loadButtonAx, 'Load')
        loadButton.on_clicked(self.onLoad)
        reloadButton = Button(self.reloadButtonAx, 'Reload')
        reloadButton.on_clicked(self.onReload)

        if infraXmlFlNm is not None:
            self.load(infraXmlFlNm)

        plt.show()

    def onLoad(self, event):
        with wx.FileDialog(None, "Open scenario file", defaultDir=str(self.localDir), wildcard="scenario files (*.xml)|*.xml",
                       style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            scenDataFlNm = fileDialog.GetPath()
            self.load(scenDataFlNm)

    def load(self, infraXmlFlNm):
        try:
            self.ax.clear()

            infraXmlFlNm = Path(infraXmlFlNm)
            self.infraXmlFlNm = infraXmlFlNm

            walkLevelInputs = parseInfraXml(infraXmlFlNm)
            try:
                walkLevels, _, _ = createWalkLevels(walkLevelInputs)
            except ValueError:
                walkLevels, _ = createWalkLevels(walkLevelInputs)
            _, sourcesPerWalkLevel = createSources(walkLevelInputs, walkLevels)
            for walkLevel, sourcesList in sourcesPerWalkLevel.items():
                walkLevel.addSources(sourcesList)

            minX, minY, maxX, maxY = drawGeometry(self, walkLevels, getLims=True)

            self.fig.canvas.manager.set_window_title('Infra vis - {}'.format(infraXmlFlNm.name))

            self.ax.set_xlim((minX, maxX))
            self.ax.set_ylim((minY, maxY))
            self.ax.set_aspect('equal')
        except:
            traceback.print_exc()
            self.infraXmlFlNm = None
            self.ax.clear()
            self.ax.text(0.5, 0.5, "An error occurred!", ha="center", va="center")

        self.fig.canvas.draw_idle()

    def onReload(self, event):
        if self.infraXmlFlNm is not None:
            self.load(self.infraXmlFlNm)

def drawGeometry(visualizer, walkLevels, getLims=False):
    minX = inf
    maxX = -inf
    minY = inf
    maxY = -inf
    if isinstance(walkLevels, dict):
        walkLevels = walkLevels.values()
    for walkLevel in walkLevels:
        for walkableArea in walkLevel.walkableAreas:
            walkableAreaCoords = getWalkableAreaCoords(walkableArea)
            for coords in walkableAreaCoords:
                pol = Polygon(coords, zorder=1, closed=True, fill=True,facecolor='whitesmoke', edgecolor='k')
                visualizer.ax.add_patch(pol)
                if getLims:
                    minX, minY, maxX, maxY = updateLims(minX, minY, maxX, maxY, walkableArea.geometry)
        for obstacle in walkLevel.obstacles:
            if getLims:
                minX, minY, maxX, maxY = updateLims(minX, minY, maxX, maxY, obstacle.geometry)
            if isinstance(obstacle, PolygonObstacle) or is_type(obstacle, 'polygon'):
                drawObj = Polygon(obstacle.coords, zorder=3, closed=True, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')
            elif isinstance(obstacle, LineObstacle) or is_type(obstacle, 'line'):
                drawObj = Line2D(obstacle.coords[0], obstacle.coords[1], zorder=3, color='k')
            elif isinstance(obstacle, CircleObstacle) or is_type(obstacle, 'circle'):
                drawObj = Circle(obstacle.centerCoord, radius=obstacle.radius, zorder=3, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')
            elif isinstance(obstacle, EllipseObstacle) or is_type(obstacle, 'ellipse'):
                drawObj = Ellipse(obstacle.centerCoord,obstacle.semiAxesValues[0]*2,obstacle.semiAxesValues[1]*2,
                                    obstacle.rotation, zorder=3, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')

            if isinstance(obstacle, LineObstacle) or is_type(obstacle, 'line'):
                visualizer.ax.add_line(drawObj)
            else:
                visualizer.ax.add_patch(drawObj)

        for destination in walkLevel.destinations:
            if getLims:   
                minX, minY, maxX, maxY = updateLims(minX, minY, maxX, maxY, destination.geometry)
            destID = type(destination).__name__
            drawObj = getDestinationObj(destination)
            if isinstance(destination, LineDestination) or is_type(destination, 'line'):
                visualizer.ax.add_line(drawObj)
            elif isinstance(destination, MultiPolygonDestination) or is_type(destination, 'multiPolygon'):
                visualizer.destinationHandles[destID] = []
                for drawObjPart in drawObj:
                    visualizer.ax.add_patch(drawObjPart)
                    if hasattr(visualizer, 'destinationHandles'):
                        visualizer.destinationHandles[destID].append(drawObjPart)
            else:
                visualizer.ax.add_patch(drawObj)
            if hasattr(visualizer, 'destinationHandles'): 
                visualizer.destinationHandles[destID] = drawObj

        for source in walkLevel.sources:
            if getLims:   
                minX, minY, maxX, maxY = updateLims(minX, minY, maxX, maxY, source.geometry)
            if isinstance(source, RectangleSource) or is_type(destination, 'polygon'):
                drawObj = Polygon(source.coords, zorder=5, closed=True, fill=True, alpha=0.6, facecolor=SOURCE_COLOR, edgecolor=SOURCE_COLOR)
                visualizer.ax.add_patch(drawObj)
            elif isinstance(source, LineSource) or is_type(destination, 'line'):
                drawObj = Line2D(source.coords[0], source.coords[1], zorder=5, linestyle='--', color=SOURCE_COLOR)
                visualizer.ax.add_line(drawObj)

        return minX, minY, maxX, maxY

def is_type(geom_object, type_str):
    if hasattr(geom_object, 'type'):
        return geom_object.type == type_str
    return False

def getDestinationObj(destination):
    hatch = 'x'
    color = DEST_COLOR

    if isinstance(destination,  (PolygonDestination, MultiPointInRectangleDestination)) or is_type(destination, 'polygon'):
        return Polygon(destination.coords, zorder=4, closed=True, fill=False, alpha=0.6, hatch=hatch, edgecolor=color, linewidth=DEST_LINE_WIDTH)
    elif isinstance(destination, MultiPolygonDestination) or is_type(destination, 'multiPolygon'):
        return [Polygon(coords, zorder=4, closed=True, fill=False, alpha=0.6, hatch=hatch, edgecolor=color, linewidth=DEST_LINE_WIDTH) for coords in  destination.coords]
    elif isinstance(destination, LineDestination) or is_type(destination, 'line'):
        return Line2D(destination.coords[0], destination.coords[1], zorder=4, color=color, linewidth=DEST_LINE_WIDTH)
    elif isinstance(destination, PointDestination) or is_type(destination, 'point'):
        return Circle(destination.coords, radius=DEST_CIRCLE_RADIUS*DEST_LINE_WIDTH, zorder=4, fill=True, color=color)
    else:
        raise Exception(f'{destination.type}')
    
def updateLims(minX, minY, maxX, maxY, geom):
    minX = min(minX, geom.bounds[0])
    maxX = max(maxX, geom.bounds[2])
    minY = min(minY, geom.bounds[1])
    maxY = max(maxY, geom.bounds[3])

    return minX, minY, maxX, maxY

def visualize(infraXmlFlNm=None, guiBackend='WxAgg', localDir=""):
    InfraVisualizer(infraXmlFlNm, guiBackend, localDir)

if __name__ == '__main__':
    visualize()