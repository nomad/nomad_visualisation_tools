""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import argparse
import glob
import os
from pathlib import Path
import shutil
import subprocess
import sys

from NOMAD_results_gui.results_gui import results_gui_main
from NOMAD_results_gui.infra_visualisation import visualize as visualize_infra
from NOMAD_results_gui.lrcm_visualisation import visualize as visualize_lrcm
from NOMAD import LOCAL_DIR

RESULTS_ACTION_NM = 'results'
INFRA_ACTION_NM = 'infra'
LRCM_ACTION_NM = 'lrcm'

RESULTS_ACTION_ALIAS = 'r'
INFRA_ACTION_ALIAS = 'i'
LRCM_ACTION_ALIAS = 'l'


def run():
    action_fcn, input_args, input_kwargs = parse_input_args()
    action_fcn(*input_args, **input_kwargs)
  

def parse_input_args(*args):
    main_parser = argparse.ArgumentParser(description='NOMAD gui arguments')
    subparsers = main_parser.add_subparsers(dest='tool')
    
    parser_run = subparsers.add_parser(RESULTS_ACTION_NM, help='Start the NOMAD visualisation tools', aliases=[RESULTS_ACTION_ALIAS])
    parser_visualize = subparsers.add_parser(INFRA_ACTION_NM, help='Start the NOMAD infrastructure visualisation tool', aliases=[INFRA_ACTION_ALIAS])
    parser_visualize = subparsers.add_parser(LRCM_ACTION_NM, help='Start the NOMAD local route choice visualisation tool', aliases=[LRCM_ACTION_ALIAS])
        
    if len(args):
        res = main_parser.parse_args(args)
    else:
        res = main_parser.parse_args()
    
    if res.tool in [RESULTS_ACTION_NM, RESULTS_ACTION_ALIAS]:
        action_fcn = results_gui_main.main
        args_out = (LOCAL_DIR,)
        kwargs_out = {}
    elif res.tool in [INFRA_ACTION_NM, INFRA_ACTION_ALIAS]:
        action_fcn = visualize_infra
        args_out = ()
        kwargs_out = {'guiBackend':'WxAgg', 'localDir':LOCAL_DIR}
    elif res.tool in [LRCM_ACTION_NM, LRCM_ACTION_ALIAS]:
        action_fcn = visualize_lrcm
        args_out = ()
        kwargs_out = {'guiBackend':'WxAgg', 'localDir':LOCAL_DIR}
    else:
        raise Exception("Unkown tool!")

            
    return action_fcn, args_out, kwargs_out

if __name__ == "__main__":
    run()