""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging
import os
import subprocess
import xml.etree.ElementTree as ET
from xml.dom import minidom


from NOMAD_results_gui.xml_config_base.xml2config import readXml as readXmlGen

def openXmlFileWithDefApplication(xmlFlNm):
    if os.name == 'nt':
        subprocess.run(['start', str(xmlFlNm)], check=True, shell=True)
    else:
        subprocess.run(['open', str(xmlFlNm)], check=True)

def checkValidity(config, configBase):
    for key, el in config.items():
        if key not in configBase:
            raise Exception('The key "{}" is not known!'.format(key))

        if isinstance(el, dict):
            checkValidity(config[key], configBase[key])

class XmlConfig():
    '''
    classdocs
    '''

    log = logging.getLogger(__name__)

    def __init__(self, resultsGui):
        self.resultsGui = resultsGui
        self.xmlFlNm = self.resultsGui.appPath.joinpath(self.getFile())

    def onUpdateXml(self, event):
        self.createConfigXml(self.getConfigDict(), self.xmlFlNm, self.addConfigDict2xml)
        openXmlFileWithDefApplication(self.xmlFlNm)

    def onUpdateGui(self, event):
        try:
            config = self.readXml(self.xmlFlNm, [self.readConfigFromXml,])
        except Exception as err:
            self.log.error(str(err.args[0]), exc_info=True)
            return

        isValid = self.checkConfigValidity(config)
        if not isValid:
            return

        self.sendPubMsg(config)

    def checkConfigValidity(self, config):
        configBase = self.getConfigDict()
        config = self.getConfigFromDict(config)
        try:
            checkValidity(config, configBase)
        except Exception as err:
            self.log.error('The new config contains and unknown key!')
            self.log.error(str(err.args[0]), exc_info=True)
            return False
        try:
            checkValidity(configBase, config)
        except Exception as err:
            self.log.error('The new config is missing a key!')
            self.log.error(str(err.args[0]), exc_info=True)
            return False

        return True

    @staticmethod
    def getConfigFromDict(config):
        raise Exception('Method should be implemented!')


    def getConfigDict(self):
        raise Exception('Method should be implemented!')


    @staticmethod
    def createConfigXml(configDict, xmlFlNm, addConfigDict2xmlFcn, encoding='utf-8'):
        rootEL = ET.Element('dividerConfig')
        rootEL.set('version', '2.0')

        addConfigDict2xmlFcn(rootEL, configDict)

        roughString = ET.tostring(rootEL, encoding)
        reparsed = minidom.parseString(roughString)
        encodedString =  reparsed.toprettyxml(indent='    ', newl='\n', encoding=encoding)
        xmlString = encodedString.decode(encoding)

        with open(xmlFlNm, 'w') as xmlFile:
            xmlFile.write(xmlString)

    @staticmethod
    def addConfigDict2xml(rootEL, configDict):
        raise Exception('Method should be implemented!')

    @staticmethod
    def readConfigFromXml(config, collection):
        raise Exception('Method should be implemented!')


    @staticmethod
    def readXml(xmlFlNm, elements):
        fields = []
        contents = {'fields':fields, 'elements':elements}
        config = readXmlGen(xmlFlNm, contents)
        return config

    @staticmethod
    def getFile():
        raise Exception('Method should be implemented!')

    @staticmethod
    def sendPubMsg(config):
        raise Exception('Method should be implemented!')
