""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import errno
import logging
from pathlib import Path

import wx

from NOMAD_results_gui.results_gui.constants import getConstant, APP_PATH


ERROR_INVALID_NAME = 123

class RecordDialog(wx.Dialog):
    '''
    classdocs
    '''
    log = logging.getLogger(__name__)

    def __init__(self, parent, defRecSpeed=1, defFilename='', **kwargs):
        wx.Dialog.__init__(self, parent, title='Setup recording', **kwargs)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)

        self.lastRecSpeed = defRecSpeed
        self.lastFilename = defFilename

        title = wx.StaticText(self, label='Choose a filename and a recording speed')
        title.SetFont(wx.Font(12, wx.DECORATIVE, wx.NORMAL, wx.NORMAL))
        self.sizer.Add(title, 0, wx.ALIGN_CENTER|wx.ALL, 15)

        self.createContentPanel()
        self.createButtonPanel()

        self.Fit()

    def createContentPanel(self):
        font = wx.Font(10, wx.DECORATIVE, wx.NORMAL, wx.NORMAL)

        sizer = wx.GridBagSizer(10,5)
        self.Sizer.Add(sizer, 0, wx.ALL, 10)

        filenameLabel = wx.StaticText(self, label='Filename:')
        filenameLabel.SetFont(font)
        sizer.Add(filenameLabel, (0,0), flag=wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)

        self.filenameText = wx.TextCtrl(self, style=wx.TE_PROCESS_ENTER, value=self.lastFilename)
        self.filenameText.SetMinSize((280, -1))
        sizer.Add(self.filenameText, (0,1), flag=wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        self.filenameText.Bind(wx.EVT_TEXT_ENTER, self.onFilenameChange)

        fileDialogButton = wx.BitmapButton(self, bitmap=wx.ArtProvider.GetBitmap(wx.ART_FILE_OPEN, size=(20,20)))
        sizer.Add(fileDialogButton, (0,2), flag=wx.ALIGN_CENTER)
        fileDialogButton.Bind(wx.EVT_BUTTON, self.onSelectFile)

        recSpeedLabel = wx.StaticText(self, label='Recording speed:')
        recSpeedLabel.SetFont(font)
        sizer.Add(recSpeedLabel, (1,0), flag=wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        self.recSpeedText = wx.TextCtrl(self, style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT, value=str(self.lastRecSpeed))
        self.recSpeedText.SetMaxSize((50, -1))
        sizer.Add(self.recSpeedText, (1,1), flag=wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        self.recSpeedText.Bind(wx.EVT_TEXT_ENTER, self.onRecSpeedChange)

    def createButtonPanel(self):
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.Sizer.Add(sizer, 0, wx.ALL|wx.ALIGN_RIGHT, 10)

        okButton = wx.Button(self, label='Start recording')
        cancelButton = wx.Button(self, wx.ID_CANCEL, label='Cancel')

        okButton.SetMinSize((100,25))
        cancelButton.SetMinSize((60,25))

        sizer.Add(okButton)
        sizer.Add(cancelButton, 0, wx.LEFT, 10)

        okButton.Bind(wx.EVT_BUTTON, self.onOk)

    def filenameCheck(self):
        valuePath = Path(self.filenameText.GetValue().strip())
        pd = valuePath.parent
        if not pd.is_dir():
            errMsg = 'The directory "{}" does not exist!'.format(pd)
            return False, None, errMsg
        try:
            valuePath.lstat()
        except FileNotFoundError:
            pass
        except OSError as exc:
            errMsgExpl = ''
            if exc.errno in {errno.ENAMETOOLONG, errno.ERANGE}:
                errMsgExpl = 'The filename is to long.'

            errMsg = 'The filename "{}" is not valid!'.format(valuePath.name)
            if len(errMsgExpl):
                errMsg = '{}\n{}'.format(errMsg, errMsgExpl)
            return False, None, errMsg

        return True, valuePath, None

    def filenameChange(self):
        isOk, valuePath, errMsg = self.filenameCheck()
        if not isOk:
            wx.MessageDialog(self, errMsg, 'Error', style=wx.OK|wx.ICON_ERROR).ShowModal()
            self.filenameText.SetValue(self.lastFilename)
        else:
            self.lastFilename = valuePath

        return isOk

    def onFilenameChange(self, event):
        self.filenameChange()

    def recSpeedCheck(self):
        recSpeedValueStr = self.recSpeedText.GetValue().strip()
        try:
            recSpeedValue = float(recSpeedValueStr)
        except ValueError:
            errMsg = 'The value "{}" is not a float!'.format(recSpeedValueStr)
            return False, None, errMsg

        if recSpeedValue <= 0:
            errMsg = 'The value should be larger than 0!'
            return False, None, errMsg

        return True, recSpeedValue, None

    def recSpeedChange(self):
        isOk, recSpeedValue, errMsg = self.recSpeedCheck()
        if not isOk:
            wx.MessageDialog(self, errMsg, 'Error', style=wx.OK|wx.ICON_ERROR).ShowModal()
            self.recSpeedText.SetValue(str(self.lastRecSpeed))
        else:
            self.lastRecSpeed = recSpeedValue

        return isOk

    def onRecSpeedChange(self, event):
        self.recSpeedChange()

    def onSelectFile(self, event):
        filename = aksForFile()
        if filename is None:
            return

        self.lastFilename = filename
        self.filenameText.SetValue(str(filename))

    def onOk(self, event):
        if not self.filenameChange() or not self.recSpeedChange():
            return

        self.SetReturnCode(wx.ID_OK)
        self.Hide()

    def getFilename(self):
        return self.lastFilename

    def getRecSpeed(self):
        return self.lastRecSpeed

def aksForFile(pd=None):
    wildcard = "MP4 (MPEG-4 Part 14) (*.mp4)|*.mp4|" \
        "AVI (Audio Video Interleaved) (*.avi)|*.avi|" \
        "FLV (Flash Video) (*.flv)|*.flv|" \
        "QuickTime / MOV (*.mov)|*.mov|" \
        "All video files (*.mp4;.avi*;.flv*;.mov*)|*.mp4;.avi*;.flv*;.mov*"
    if pd is None:
        pd = getConstant(APP_PATH)
    else:
        pd = Path(pd)


    with wx.FileDialog(None, "Choose a file", str(pd), wildcard=wildcard,
                       style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as fileDialog:

        if fileDialog.ShowModal() == wx.ID_CANCEL:
            return     # the user changed their mind

        filePath = Path(fileDialog.GetPath())
        return filePath