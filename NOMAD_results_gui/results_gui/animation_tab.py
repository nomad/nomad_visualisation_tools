""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from collections import namedtuple
import copy
from dataclasses import dataclass
import errno
import inspect
import logging
from math import floor, ceil
import subprocess

from matplotlib.animation import FFMpegWriter
from matplotlib.lines import Line2D
from matplotlib.patches import Polygon, Circle, Ellipse
from pubsub import pub
import wx

from NOMAD_results_gui.axes_divider import RIGHT_GAP, TOP_GAP, BOTTOM_GAP, V_GAP, \
    TITLE_GAP, LEFT_GAP
from NOMAD_results_gui.axes_divider.axes_grid import AxesGrid
from NOMAD_results_gui.results_gui.constants import getConstant, IMAGE_DIR_PATH, MOVIE_RECORD_MODE
from NOMAD_results_gui.results_gui.cust_wx_elements import Panel, TimeSlider, \
    EVT_TIMESLIDER_SCROLL_EVENT, getTimeLabel
from NOMAD_results_gui.results_gui.data_tab import DataTab
from NOMAD_results_gui.results_gui.main_draw_axes import MainDrawAxes
from NOMAD_results_gui.results_gui.matplotlib_canvas_panel import MatplotLibCanvasPanel
from NOMAD_results_gui.results_gui.record_dialog import RecordDialog
import numpy as np


PLAY_ID = 'play'
PAUSE_ID = 'pause'
STOP_ID = 'stop'
CHANGED_TIME_ID = 'changedTime'

DENS_TYPE = 'density'
COUNTS_TYPE = 'counts'

ZOOM_SCROLL_ID = 'zoom_scroll'

TIME_UPDATED_MSG_ID = 'timeUpdated'

class AnimationTab(DataTab):
    '''classdocs'''

    def __init__(self, ID, parentFrame, dataMenu, *args, **kwargs):
        '''Constructor'''
        DataTab.__init__(self, ID, parentFrame, dataMenu, self.HORIZONTAL_SPLIT, self.CONTENT_CONTROL, *args, **kwargs)

        self.stepCounts = {}
        self.timeSteps = {}
        self.dataSet2updatePerStep = []
        self.glob2locRatios = {}
        self.timeStep = None
        self.stepCount = None

        self.contentPanel.checkStates = self.controlPanel.getCheckStates()

        pub.subscribe(self.onUpdateAnim, self.controlPanel.UPDATE_ANIM)
        pub.subscribe(self.onStartRecording, self.controlPanel.START_RECORDING)
        pub.subscribe(self.onStopRecording, self.controlPanel.STOP_RECORDING)

    def initControlPanel(self, *args, **kwargs):
        return AnimationControlPanel(self.parentFrame, self.ID, *args, **kwargs)

    def initContentPanel(self, *args, **kwargs):
        return AnimationContentPanel(self.parentFrame, self.ID, *args, **kwargs)

    def preAddDataSets(self):
        if self.controlPanel.timer is not None:
            self.controlPanel.onStop(None)

    def preRemoveDataSets(self):
        if self.controlPanel.timer is not None:
            self.controlPanel.onStop(None)

    def preReloadDataSets(self):
        if self.controlPanel.timer is not None:
            self.controlPanel.onStop(None)

    def postAddDataSets(self):
        # Redraw the canvas
        self.updatePanels()

    def postRemoveDataSets(self):
        # Redraw the canvas
        if len(self.dataSetIds) == 0:
            self.controlPanel.clean()
            self.contentPanel.clean()
            self.contentPanel.setDataSets(self.dataSetIds, self.dataMenu)
            self.contentPanel.canvas.draw()
            return

        self.updatePanels()

    def postReloadDataSets(self):
        # Redraw the canvas
        self.updatePanels()

    def updatePanels(self):
        self.setTimeInfo()
        self.controlPanel.update(self.stepCount, self.timeStep)
        self.contentPanel.setDataSets(self.dataSetIds, self.dataMenu)

        self.onUpdateAnim(0, self.controlPanel.getCheckStates(),
                          forceFullUpdate=True)
        self.contentPanel.onForceCanvasRedraw(self.controlPanel.getCheckStates())

    def addDataSet(self, ID):
        simResults = self.dataMenu.dataSets[ID]
        self.stepCounts[ID] = simResults.stepCount
        self.timeSteps[ID] = simResults.timeStep

    def removeDataSet(self, ID):
        self.stepCounts.pop(ID)
        self.timeSteps.pop(ID)

    def reloadDataSet(self, ID):
        self.removeDataSet(ID)
        self.addDataSet(ID)

    def getTitles(self):
        return self.contentPanel.getTitles()

    def updateTitles(self, titles):
        self.contentPanel.updateTitles(titles)

    def setTimeInfo(self):

        def getIDsInStep(timeInd):
            IDsInStep = []
            for ID, loc2globFactor in glob2locRatios.items():
                if timeInd % loc2globFactor == 0:
                    IDsInStep.append(ID)

            return IDsInStep

        timeSteps = list(self.timeSteps.values())
        stepCounts = list(self.stepCounts.values())
        if len(self.dataSetIds) == 1:
            self.timeStep = timeSteps[0]
            self.stepCount = stepCounts[0]
            self.dataSet2updatePerStep = [self.dataSetIds.copy() for _ in range(self.stepCount)]
            glob2locRatios = {ID:1 for ID in self.dataSetIds}
        else:
            allEqual = np.all(np.array(timeSteps) == timeSteps[0])
            if allEqual:
                self.timeStep = timeSteps[0]
                self.stepCount = np.max(stepCounts)
                glob2locRatios = {ID:1 for ID in self.dataSetIds}
            else:
                timeSteps = (1. / np.array(timeSteps)).astype(dtype=int)
                maxInd = np.argmax(timeSteps)
                maxStep = timeSteps[maxInd]
                remSteps = np.delete(timeSteps, maxInd)
                gcd = numpy_gcd(np.array(maxStep), remSteps)
                minInd = np.argmin(gcd)
                timeStep = float(gcd[minInd]) / float(maxStep * remSteps[minInd])

                maxStepCnt = 0
                glob2locRatios = {}
                for ID in self.dataSetIds:
                    simResults = self.dataMenu.dataSets[ID]
                    glob2locRatio = int(simResults.timeStep / timeStep)
                    maxStepCnt = np.maximum(maxStepCnt, glob2locRatio*(simResults.stepCount - 1) + 1)
                    glob2locRatios[ID] = glob2locRatio

                self.timeStep = timeStep
                self.stepCount = maxStepCnt

            self.dataSet2updatePerStep = [getIDsInStep(ii) for ii in range(self.stepCount)]

        self.glob2locRatios = glob2locRatios

    def onUpdateAnim(self, timeInd, checkStates, forceFullUpdate=False):
        if forceFullUpdate:
            dataIds = self.dataSetIds
        else:
            dataIds = self.dataSet2updatePerStep[timeInd]

        dynamicElsPerID = {}
        updateElsPerID = {}
        timeIndPerID = {}

        for ID in dataIds:
            localTimeInd = floor(timeInd/self.glob2locRatios[ID])
            timeIndPerID[ID] = localTimeInd

            dynamicEls = {'toAdd':self.dataMenu.dataSets[ID].animationDynamicElements[localTimeInd].toAdd,
                          'toRemove':self.dataMenu.dataSets[ID].animationDynamicElements[localTimeInd].toRemove}

            if forceFullUpdate:
                dynamicEls['toAdd'] += self.dataMenu.dataSets[ID].animationDynamicElements[localTimeInd].drawn

            if localTimeInd >= self.dataMenu.dataSets[ID].stepCount:
                timeIndPerID[ID] = self.dataMenu.dataSets[ID].stepCount - 1

            updateEls = self.dataMenu.dataSets[ID].animationUpdateElements[localTimeInd]

            dynamicElsPerID[ID] = dynamicEls
            updateElsPerID[ID] = updateEls
            if localTimeInd >= self.dataMenu.dataSets[ID].stepCount:
                timeIndPerID[ID] = self.dataMenu.dataSets[ID].stepCount - 1
            else:
                timeIndPerID[ID] = localTimeInd

        self.contentPanel.onUpdateAnim(timeIndPerID, self.timeSteps,
                                       dynamicElsPerID, updateElsPerID, checkStates, forceFullUpdate)

        pub.sendMessage(TIME_UPDATED_MSG_ID, timeInSeconds=timeInd*self.timeStep)

    def onStartRecording(self, filename, recSpeed):
        self.contentPanel.startRecord(filename, recSpeed, self.timeStep)

    def onStopRecording(self):
        self.contentPanel.endRecord()

    def clean(self):
        if self.controlPanel.timer is not None:
            self.controlPanel.onStop(None)
        self.contentPanel.clean()

        self.stepCounts = {}
        self.timeSteps = {}
        self.dataSet2updatePerStep = []
        self.glob2locRatios = {}
        self.timeStep = None
        self.stepCount = None

    def onDelete(self):
        pub.unsubscribe(self.onUpdateAnim, self.controlPanel.UPDATE_ANIM)
        pub.unsubscribe(self.onStartRecording, self.controlPanel.START_RECORDING)
        pub.unsubscribe(self.onStopRecording, self.controlPanel.STOP_RECORDING)

    def onAfterZoomOrPan(self):
        self.controlPanel.pauseForEvent()
        self.onUpdateAnim(self.controlPanel.timeInd, self.controlPanel.getCheckStates(), forceFullUpdate=True)
        self.controlPanel.continueAfterEvent()

    @staticmethod
    def getType():
        return 'animation'

    @staticmethod
    def getLabel():
        return 'Animation'

    @staticmethod
    def getShortLabel():
        return 'Animation'


FORCE_CANVAS_REDRAW_MSG_ID = 'UpdateCellLabels'
CheckStates = namedtuple('CheckStates', ['showTimeInd', 'showAx', 'showTitle',
                                         'showTitlesLeft', 'showTimeAtTop'])

class AnimationControlPanel(Panel):

    log = logging.getLogger(__name__)

    UPDATE_ANIM = 'updateAnim'
    START_RECORDING = 'startRecording'
    STOP_RECORDING = 'stopRecording'

    def __init__(self, parentFrame, ID, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parentFrame = parentFrame
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)
        self.addButtonPanel(sizer)
        self.addSliderPanel(sizer)
        self.isPaused = None
        self.isPlaying = None
        self.stepCount = None
        self.timeInd = None
        self.timeStep = None
        self.timer = None
        self.isRecording = False
        self.recSpeed = None
        self.recFilename = None
        self.pausedByEvent = False

        self.UPDATE_ANIM += '_' + ID

        self.START_RECORDING += '_' + ID
        self.STOP_RECORDING += '_' + ID

        self.checkFFmpegStatus()

    def checkFFmpegStatus(self):
        self.hasFFmpeg = False
        try:
            subprocess.run('ffmpeg', stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except (FileNotFoundError, OSError) as err:
            if (isinstance(err, OSError) and err.errno == errno.ENOENT) or isinstance(err, FileNotFoundError):
                return
            else:
                raise err

        self.hasFFmpeg = True

    def addButtonPanel(self, sizer):
        buttonPanel = Panel(self)
        pnlSizer = wx.BoxSizer(wx.HORIZONTAL)
        buttonPanel.SetSizer(pnlSizer)

        # ----------------------- Play/Pause and Stop -----------------------
        imageDirPath = getConstant(IMAGE_DIR_PATH)

        self.playBitmap = getScaledBitmapImage(str(imageDirPath.joinpath('icon-play.png')))
        self.pauseBitmap = getScaledBitmapImage(str(imageDirPath.joinpath('icon-pause.png')))
        self.stopBitmap = getScaledBitmapImage(str(imageDirPath.joinpath('icon-stop.png')))

        self.playPause = wx.BitmapButton(buttonPanel, -1, self.playBitmap)
        self.stop = wx.BitmapButton(buttonPanel, -1, self.stopBitmap)

        self.Bind(wx.EVT_BUTTON, self.onTogglePlayPause, self.playPause)
        self.Bind(wx.EVT_BUTTON, self.onStop, self.stop)

        pnlSizer.Add(self.playPause, 0, wx.LEFT)
        pnlSizer.Add(self.stop, 0, wx.LEFT, 4)

        self.record = wx.BitmapToggleButton(buttonPanel, label=getScaledBitmapImage(str(imageDirPath.joinpath('record.png')), 16))
        self.record.SetMinSize((32, 32))

        self.Bind(wx.EVT_TOGGLEBUTTON, self.onRecordToggle, self.record)

        pnlSizer.Add(self.record, 0, wx.LEFT | wx.ALIGN_CENTER, 10)

        # ----------------------- Speed -----------------------
        self.curSpeed = 1
        self.speedTxt = wx.TextCtrl(buttonPanel, style=wx.TE_PROCESS_ENTER, value='{}'.format(self.curSpeed))
        self.speedTxt.SetMinSize((40, 25))

        labelFont = wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        speedLabel = wx.StaticText(buttonPanel, -1, 'Sim. speed:', style=wx.ALIGN_RIGHT)
        speedLabel.SetFont(labelFont)

        self.speedTxt.Bind(wx.EVT_TEXT_ENTER, self.onChangeSpeed)
        self.speedTxt.Bind(wx.EVT_KILL_FOCUS, self.onChangeSpeed)

        pnlSizer.Add(speedLabel, 0, wx.LEFT | wx.ALIGN_CENTER, 15)
        pnlSizer.Add(self.speedTxt, 0, wx.LEFT | wx.ALIGN_CENTER, 2)

        # ----------------------- visibility check boxes -----------------------
        checkBoxes = (
            # fieldNm, checkFldNm, label, default, cbFcn
            ('showTimeInd', 'timeIndCheck', 'Show time index', True, None),
            ('showTitle', 'showTitleCheck', 'Show title', True, (pub.sendMessage, (FORCE_CANVAS_REDRAW_MSG_ID,), {'checkStates':self.getCheckStates})),
            ('showAx', 'axCheck', 'Show ax', True, (pub.sendMessage, (FORCE_CANVAS_REDRAW_MSG_ID,), {'checkStates':self.getCheckStates})),
            ('showTitlesLeft', 'titlesLeftCheck', 'Title on the left', False, (pub.sendMessage, (FORCE_CANVAS_REDRAW_MSG_ID,), {'checkStates':self.getCheckStates})),
            ('showTimeAtTop', 'showTimeAtTopCheck', 'Show time at top', False, (pub.sendMessage, (FORCE_CANVAS_REDRAW_MSG_ID,), {'checkStates':self.getCheckStates})),
            )

        rowCnt = 2
        colCnt = int(ceil(len(checkBoxes)/rowCnt))

        self.checkBoxInfo = {}

        checkSizer = wx.FlexGridSizer(rowCnt, colCnt, 5, 5)
        self.checkBoxes = []
        for checkBoxInfo in checkBoxes:
            setattr(self, checkBoxInfo[0], checkBoxInfo[3])
            checkBox = wx.CheckBox(buttonPanel, label=checkBoxInfo[2])
            checkBox.SetValue(checkBoxInfo[3])
            checkBox.Bind(wx.EVT_CHECKBOX, self.onCheckBoxChange)
            setattr(self, checkBoxInfo[1], checkBox)
            checkSizer.Add(checkBox, 0, wx.ALIGN_LEFT)

            self.checkBoxInfo[checkBox] = {'field':checkBoxInfo[0], 'afterFcn':None,
                                           'afterFcnArgs':None}
            if checkBoxInfo[4] is not None:
                self.checkBoxInfo[checkBox]['afterFcn'] = checkBoxInfo[4][0]
                self.checkBoxInfo[checkBox]['afterFcnArgs'] = checkBoxInfo[4][1]
                self.checkBoxInfo[checkBox]['afterFcnKwargs'] = checkBoxInfo[4][2]

            self.checkBoxes.append(checkBox)

        pnlSizer.Add(checkSizer, 0, wx.LEFT | wx.ALIGN_CENTER, 10)

        # ----------------------- Finalize -----------------------
        self.buttonPanel = buttonPanel
        sizer.Add(buttonPanel, 0, wx.ALL | wx.EXPAND, 5)

        self.playPause.Enable(False)
        self.stop.Enable(False)
        self.speedTxt.Enable(False)
        self.record.Enable(False)
        for checkBox in self.checkBoxes:
            checkBox.Enable(False)

    def addSliderPanel(self, sizer):
        sliderPanel = Panel(self)
        pnlSizer = wx.BoxSizer(wx.VERTICAL)
        sliderPanel.SetSizer(pnlSizer)

        self.timeSlider = TimeSlider(sliderPanel)
        pnlSizer.Add(self.timeSlider, 1, wx.EXPAND)

        self.timeSlider.Bind(EVT_TIMESLIDER_SCROLL_EVENT, self.onScrollChanged)

        self.sliderPanel = sliderPanel
        sizer.Add(sliderPanel, 1, wx.ALL | wx.EXPAND, 5)

        self.timeSlider.Enable(False)

    def update(self, stepCount, timeStep):
        self.stepCount = stepCount
        self.timeStep = timeStep

        self.timeSlider.setTime(self.stepCount, self.timeStep)
        self.timeSlider.SetValue(0)

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.onRedrawTimer, self.timer)
        self.isPlaying = False
        self.isPaused = False
        self.timeInd = 0

        self.playPause.Enable(True)
        self.stop.Enable(True)
        self.speedTxt.Enable(True)
        self.timeSlider.Enable(True)
        self.record.Enable(True)
        for checkBox in self.checkBoxes:
            checkBox.Enable(True)

    def clean(self):
        self.timeSlider.setTime(100, 1)
        self.timeSlider.SetValue(0)

        self.isPaused = None
        self.isPlaying = None
        self.stepCount = None
        self.timeInd = None
        self.timeStep = None
        self.timer = None
        self.isRecording = False
        self.recSpeed = None
        self.recFilename = None
        self.pausedByEvent = False

        self.playPause.Enable(False)
        self.stop.Enable(False)
        self.speedTxt.Enable(False)
        self.timeSlider.Enable(False)
        self.record.Enable(False)
        for checkBox in self.checkBoxes:
            checkBox.Enable(False)

    def setSimResults(self, simResults):
        self.timeSlider.setTime(simResults.stepCount, simResults.timeStep)

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.onRedrawTimer, self.timer)
        self.isPlaying = False
        self.isPaused = False
        self.stepCount = simResults.stepCount
        self.timeInd = 0
        self.timeStep = simResults.timeStep

        self.playPause.Enable(True)
        self.stop.Enable(True)
        self.speedTxt.Enable(True)
        self.timeSlider.Enable(True)
        self.record.Enable(True)

    def onScrollChanged(self, event):
        self.pauseForEvent()

        self.timeInd = self.timeSlider.GetValue()
        pub.sendMessage( self.UPDATE_ANIM, timeInd=self.timeInd, checkStates=self.getCheckStates(),
                        forceFullUpdate=True)

        self.continueAfterEvent()

    def onTogglePlayPause(self, event):
        if self.isPaused or not(self.isPlaying):
            self.log.debug('Starting simulation')
            bitmap = self.pauseBitmap
            self.isPaused = False
            self.isPlaying = True
            self.playPause.SetBitmap(bitmap)
            if not self.isRecording and self.record.GetValue():
                self.startRecording()

            self.timer.Start(milliseconds=int(self.timeStep * 1000 / self.curSpeed))
        else:
            self.log.debug('Pausing simulation')
            bitmap = self.playBitmap
            self.isPaused = True
            self.isPlaying = False
            self.timer.Stop()
            self.playPause.SetBitmap(bitmap)

    def onStop(self, event):
        self.log.debug('Stopping simulation')
        if self.isRecording:
            self.stopRecording()
        self.playPause.SetBitmap(self.playBitmap)
        self.isPaused = False
        self.isPlaying = False
        self.timer.Stop()
        self.timeInd = 0
        self.timeSlider.SetValue(self.timeInd)
        pub.sendMessage( self.UPDATE_ANIM, timeInd=self.timeInd, checkStates=self.getCheckStates())

    def onRedrawTimer(self, event):
        self.timeInd = self.timeInd + 1
        if self.timeInd >= self.stepCount:
            self.onStop(None)
            return

        self.timeSlider.SetValue(self.timeInd)
        pub.sendMessage( self.UPDATE_ANIM, timeInd=self.timeInd, checkStates=self.getCheckStates())

    def onChangeSpeed(self, event):
        try:
            value = float(self.speedTxt.GetValue())
        except Exception as err:
            self.speedTxt.SetValue(str(self.curSpeed))
            dialog = wx.MessageDialog(self, str(err), 'Warning', wx.OK | wx.ICON_WARNING)
            dialog.ShowModal()
            dialog.Destroy()
            event.Skip()
            return

        if value <= 0:
            self.speedTxt.SetValue(str(self.curSpeed))
            dialog = wx.MessageDialog(self, 'Speed must be greater than 0', 'Warning', wx.OK | wx.ICON_WARNING)
            dialog.ShowModal()
            dialog.Destroy()
            event.Skip()
            return

        self.pauseForEvent()

        self.curSpeed = value
        self.continueAfterEvent(self.afterChangeCurSpeed)

        event.Skip()

    def afterChangeCurSpeed(self):
        if not self.pausedByEvent:
            pub.sendMessage( self.UPDATE_ANIM, timeInd=self.timeInd, checkStates=self.getCheckStates())

    def onRecordToggle(self, event):
        self.pauseForEvent()
        self.recordMode = self.parentFrame.getRecordMode()
        
        if self.recordMode == MOVIE_RECORD_MODE and not self.hasFFmpeg:
            warningStr = 'Cannot record because ffmpeg.exe cannot be found'
            dialog = wx.MessageDialog(self, warningStr, 'Cannot record', wx.OK | wx.ICON_WARNING)
            dialog.ShowModal()
            dialog.Destroy()
            self.record.SetValue(False)
            self.continueAfterEvent()
            return

        if self.record.GetValue():
            self.setupRecording()
            self.continueAfterEvent(self.startRecording)
            return
        elif self.isRecording:
            self.stopRecording()
            self.recSpeed = None
            self.recFilename = None
        else:
            self.recSpeed = None
            self.recFilename = None

        self.continueAfterEvent()

    def setupRecording(self):
        # Ask for filename and recSpeed
        dlg = RecordDialog(self, self.recordMode)
        retCode = dlg.ShowModal()
        # self.log.debug('{}'.format(retCode))
        if retCode == wx.ID_CANCEL:
            self.log.debug('Cancel recording')
            self.record.SetValue(False)
            return

        self.recFilename = dlg.getFilename()        
        self.recSpeed = dlg.getRecSpeed()
        self.log.debug('Recording is set-up')
        dlg.Destroy()

    def startRecording(self):
        self.log.debug('Start recording')
        self.isRecording = True
        self.saveSpeed = self.curSpeed
        self.curSpeed = 1e12
        self.speedTxt.SetValue('inf')
        self.speedTxt.Enable(False)
        pub.sendMessage(self.START_RECORDING, filename=self.recFilename, recSpeed=self.recSpeed)

    def stopRecording(self):
        self.log.debug('Stop recording')
        self.pauseForEvent()
        pub.sendMessage(self.STOP_RECORDING)
        self.curSpeed = self.saveSpeed
        self.saveSpeed = None
        self.record.SetValue(False)
        self.speedTxt.SetValue(str(self.curSpeed))
        self.speedTxt.Enable(True)
        self.isRecording = False

        msg = 'The recording was saved to {} at {} record speed.'.format(self.recFilename, self.recSpeed)
        self.recSpeed = None
        self.recFilename = None
        wx.CallAfter(self.showRecordingSavedDialog, msg)
        self.continueAfterEvent()

    def showRecordingSavedDialog(self, msg):
        wx.MessageDialog(wx.GetTopLevelParent(self), msg, 'Recording saved').ShowModal()

    def onCheckBoxChange(self, event):
        self.pauseForEvent()

        fldNm = self.checkBoxInfo[event.EventObject]['field']
        setattr(self, fldNm, event.EventObject.GetValue())

        pub.sendMessage(self.UPDATE_ANIM, timeInd=self.timeInd, checkStates=self.getCheckStates(),
                        forceFullUpdate=True)

        if self.checkBoxInfo[event.EventObject]['afterFcn'] is not None:
            args = []
            for arg in self.checkBoxInfo[event.EventObject]['afterFcnArgs']:
                if inspect.ismethod(arg):
                    arg = arg()
                args.append(arg)

            kwargs = {}
            for key, value in self.checkBoxInfo[event.EventObject]['afterFcnKwargs'].items():
                if inspect.ismethod(value):
                        value = value()
                kwargs[key] = value

            self.checkBoxInfo[event.EventObject]['afterFcn'](*args,
                                                             **kwargs)

        self.continueAfterEvent()

    def getCheckStates(self):
        return CheckStates(self.showTimeInd, self.showAx, self.showTitle,
                           self.showTitlesLeft, self.showTimeAtTop)

    def pauseForEvent(self, extraFnc2execute=None, *args):
        self.pausedByEvent = False
        if self.isPlaying:
            self.onTogglePlayPause(None)
            self.pausedByEvent = True
            if extraFnc2execute is not None:
                extraFnc2execute(*args)

    def continueAfterEvent(self, extraFnc2execute=None, *args):
        if self.pausedByEvent:
            if extraFnc2execute is not None:
                extraFnc2execute(*args)
            self.pausedByEvent = False
            self.onTogglePlayPause(None)
        elif extraFnc2execute == self.startRecording:
            extraFnc2execute(*args)
            self.onTogglePlayPause(None)


def getScaledBitmapImage(filename, scale=24):
    image = wx.Image(filename)
    image = image.Scale(scale, scale)
    return wx.Bitmap(image)


UPDATE_RESIZE_TEXT_MSG_ID = 'UpdateResizeText'

class AnimationContentPanel(MatplotLibCanvasPanel):

    log = logging.getLogger(__name__)

    BASE_GAP_CONFIG = 'base'
    AXISLESS_GAP_CONFIG = 'axisless'
    TITLELESS_GAP_CONFIG = 'titleless'
    AXIS_AND_TITLELESS_GAP_CONFIG = 'axisAndtitleless'

    GAPS_CONFIG = {
        BASE_GAP_CONFIG:copy.deepcopy(MatplotLibCanvasPanel.DEF_GAPS),
        }

    GAPS_CONFIG[BASE_GAP_CONFIG][RIGHT_GAP] = 0.2
    GAPS_CONFIG[BASE_GAP_CONFIG][TOP_GAP] = 0.4
    GAPS_CONFIG[BASE_GAP_CONFIG][BOTTOM_GAP] = 0.5
    GAPS_CONFIG[BASE_GAP_CONFIG][V_GAP] = 0.9
    GAPS_CONFIG[BASE_GAP_CONFIG][TITLE_GAP] = 0.01

    GAPS_CONFIG[AXISLESS_GAP_CONFIG] = copy.deepcopy(GAPS_CONFIG[BASE_GAP_CONFIG])
    GAPS_CONFIG[TITLELESS_GAP_CONFIG] = copy.deepcopy(GAPS_CONFIG[BASE_GAP_CONFIG])
    GAPS_CONFIG[AXIS_AND_TITLELESS_GAP_CONFIG] = copy.deepcopy(GAPS_CONFIG[BASE_GAP_CONFIG])

    GAPS_CONFIG[AXISLESS_GAP_CONFIG][LEFT_GAP] = 0.2
    GAPS_CONFIG[AXISLESS_GAP_CONFIG][RIGHT_GAP] = 0.6
    GAPS_CONFIG[AXISLESS_GAP_CONFIG][TOP_GAP] = 0.3
    GAPS_CONFIG[AXISLESS_GAP_CONFIG][BOTTOM_GAP] = 0.1
    GAPS_CONFIG[AXISLESS_GAP_CONFIG][V_GAP] = 0.4

    GAPS_CONFIG[TITLELESS_GAP_CONFIG][TOP_GAP] = 0.1
    GAPS_CONFIG[TITLELESS_GAP_CONFIG][V_GAP] = 0.45

    GAPS_CONFIG[AXIS_AND_TITLELESS_GAP_CONFIG][LEFT_GAP] = 0.2
    GAPS_CONFIG[AXIS_AND_TITLELESS_GAP_CONFIG][RIGHT_GAP] = 0.6
    GAPS_CONFIG[AXIS_AND_TITLELESS_GAP_CONFIG][TOP_GAP] = 0.1
    GAPS_CONFIG[AXIS_AND_TITLELESS_GAP_CONFIG][BOTTOM_GAP] = 0.1
    GAPS_CONFIG[AXIS_AND_TITLELESS_GAP_CONFIG][V_GAP] = 0.2

    def __init__(self, *args, **kwargs):
        '''Constructor'''
        super().__init__(*args, **kwargs)
        self.isRecording = False
        self.lastStateArgs = []
        self.curGapConfig = None
        self.checkStates = None

        self.textFlags = [self.canvas.TOP_LOC, self.canvas.BOTTOM_LOC]

        pub.subscribe(self.onUpdateResizeText, UPDATE_RESIZE_TEXT_MSG_ID)
        pub.subscribe(self.onForceCanvasRedraw, FORCE_CANVAS_REDRAW_MSG_ID)

    def createAxes(self, dataSetIds=[]):
        super().createAxes(dataSetIds=dataSetIds)
        for dataAx in self.axesGrid.dataAxes.values():
            dataAx.setAfterAxLimChangeCallbacks(self.afterXLimChange, self.afterYLimChange)

    def setDataSets(self, dataSetIds, dataMenu):
        self.clean()
        self.createAxes(dataSetIds)
        if len(dataSetIds):
            self.drawStaticElements(dataMenu, len(dataSetIds) > 1)
        else:
            self.finalizePlot()

    def drawStaticElements(self, dataMenu, showLabels):
        dataLims = {axID:None for axID in self.ID2axInd.values()}
        gapFactors = dataLims.copy()

        for dataID, axID in self.ID2axInd.items():
            dataAx = self.axesGrid.dataAxes[axID]
            staticElements = dataMenu.dataSets[dataID].animationStaticElements
            if showLabels:
                label = dataMenu.dataSets[dataID].label
            else:
                label = None

            dataAx.drawStaticElements(staticElements, label)

            dataLims[axID] = dataMenu.dataSets[dataID].animationMinMax
            gapFactors[axID] = {'x':0.05, 'y':0.05}

        self.canvas.setBlitState(True, self.afterResize)
        self.canvas.setAxLimChangeFcn(self.afterResize)

        self.finalizePlot(dataLims=dataLims, gapFactors=gapFactors, aspect='equal')
        self.canvas.draw()

    def onUpdateResizeText(self, dataAx, texts, doRemove):
        if doRemove:
            self.canvas.removeTextResize(texts)
        else:
            resizeText = None
            resizeTextCoords = [-np.inf, -np.inf]
            for text in texts:
                textPos = text.get_position()
                if textPos[1] > resizeTextCoords[1]:
                    resizeText = text
            self.canvas.addTextResize(dataAx, resizeText)

    def getDividerGroup(self, dataSetIds=[]):
        axesGridConfig = copy.deepcopy(self.DEF_AXES_GRID_CONFIG)
        self.curGapConfig = self.BASE_GAP_CONFIG
        for gapID, gapSize in self.GAPS_CONFIG[self.BASE_GAP_CONFIG].items():
            axesGridConfig[gapID] = gapSize

        axCnt = np.maximum(1, len(dataSetIds))

        verFactors = [1 for _ in range(axCnt)]
        axesGrid = AxesGrid(verFactors, [1], valueDict=axesGridConfig)
        dataAxes = [AnimationDrawAxes('{}_{}'.format(self.tabID, ii), [ii, 0]) for ii in range(axCnt)]
        self.ID2axInd = {dataSetIds[ii]:dataAxes[ii].ID for ii in range(len(dataSetIds))}
        axesGrid.addDataAxes(dataAxes)
        return axesGrid

    def startRecord(self, filename, recSpeed, timeStep):
        metadata = dict(title='networkMovie', artist='resultsGui',
                        comment='')

        fps = 1/timeStep*recSpeed

        self.writer = FFMpegWriter(fps=fps, metadata=metadata)
        self.writer.setup(self.figure, str(filename), self.figure.get_dpi())
        self.log.debug(self.writer.bin_path())
        self.switchIsRecording(True)

    def endRecord(self):
        self.writer.finish()
        self.writer = None
        self.switchIsRecording(False)

    def onUpdateAnim(self, timeInd, timeStep, dynamicElsPerID, updateElsPerID, checkStates, forceFullUpdate):
        self.canvas.restore_region()
        for dataID, axID in self.ID2axInd.items():
            self.axesGrid.dataAxes[axID].updateElements(timeInd[dataID], timeStep[dataID],
                                                        dynamicElsPerID[dataID], updateElsPerID[dataID],
                                                        checkStates, forceFullUpdate)

        self.axesGrid.updateTitleLabel(getTimeLabel(timeInd[dataID]*timeStep[dataID]))

        self.canvas.blit(self.figure.bbox)
        self.lastStateArgs = [timeInd, timeStep, dynamicElsPerID, updateElsPerID, checkStates, forceFullUpdate]

        if self.isRecording:
            self.writer.grab_frame()

    def clean(self):
        super().clean()
        self.lastStateArgs = []

    def onForceCanvasRedraw(self, checkStates):
        gaps2adapt = {}
        if (checkStates.showTitle and checkStates.showAx) and (not self.checkStates.showTitle or not self.checkStates.showAx):
            gaps2adapt = self.GAPS_CONFIG[self.BASE_GAP_CONFIG]
        elif (checkStates.showTitle and not checkStates.showAx):
            gaps2adapt = self.GAPS_CONFIG[self.AXISLESS_GAP_CONFIG]
        elif (not checkStates.showTitle and checkStates.showAx):
            gaps2adapt = self.GAPS_CONFIG[self.TITLELESS_GAP_CONFIG]
        elif (not checkStates.showTitle and not checkStates.showAx) and (self.checkStates.showTitle or self.checkStates.showAx):
            gaps2adapt = self.GAPS_CONFIG[self.AXIS_AND_TITLELESS_GAP_CONFIG]

        if len(gaps2adapt):
            self.axesGrid.adaptGapSizes(gaps2adapt)

        if checkStates.showTimeAtTop and not self.axesGrid.hasTitleText():
            # Add title
            timeLabel = getTimeLabel(0)
            self.axesGrid.setTitleLabel(label=timeLabel, mplConfig={'size':12, 'animated':True})
        elif not checkStates.showTimeAtTop and self.axesGrid.hasTitleText():
            self.axesGrid.removeTitleLabel()

        self.canvas.draw(forceBlitReset=True)
        self.checkStates = checkStates

    def afterResize(self):
        if len(self.lastStateArgs):
            if len(self.lastStateArgs[2]) == len(self.ID2axInd):
                self.onUpdateAnim(*self.lastStateArgs)

    def afterXLimChange(self, navMode):
        if navMode != 'PAN':
            #self.log.debug('XLimChange')
            self.canvas.axLimChanged = True

    def afterYLimChange(self, navMode):
        if navMode != 'PAN':
            #self.log.debug('YLimChange')
            self.canvas.axLimChanged = True

    def home(self, axLabel):
        for dataAx in self.axesGrid.dataAxes.values():
            if axLabel is None or dataAx.ax.get_label() == axLabel:
                dataAx.isInHomeView = True
        super().home(axLabel)

    def switchIsRecording(self, isRecording):
        self.isRecording = isRecording
        self.canvas.isRecording = isRecording

    def preSaveFcn(self):
        self.ax2childs = {}
        for dataAx in self.axesGrid.dataAxes.values():
            self.ax2childs[dataAx] = dataAx.turnAnimatedOff()

    def postSaveFcn(self):
        for dataAx in self.axesGrid.dataAxes.values():
            dataAx.turnAnimatedOn(self.ax2childs[dataAx])

        self.ax2childs = {}

    def getTitles(self):
        titles = {}
        for dataID, axID in self.ID2axInd.items():
            titles[dataID] = self.axesGrid.dataAxes[axID].custLabelString
            if titles[dataID] is None:
                titles[dataID] = 'None'

        return titles

    def updateTitles(self, titles):
        for dataID, title in titles.items():
            axID = self.ID2axInd[dataID]
            self.axesGrid.dataAxes[axID].updateCustLabelString(title)

        self.onForceCanvasRedraw(self.checkStates)

TOP_TITLE_LOC = 'top'
LEFT_TITLE_LOC = 'left'

TITLE_LABEL_CONFIG = {
    TOP_TITLE_LOC:{True:{'x':0, 'y':1, 'horAlign':'left', 'verAlign':'bottom'},
             False:{'x':0.5, 'y':1, 'horAlign':'center', 'verAlign':'bottom'},
            },
    LEFT_TITLE_LOC:{True:{'x':0, 'y':1., 'horAlign':'right', 'verAlign':'top'},
              False:{'x':0, 'y':0.5, 'horAlign':'right', 'verAlign':'center'},
             }
    }

TIME_LABEL_CONFIG = {
    TOP_TITLE_LOC:{True:{'x':1, 'y':1, 'horAlign':'right', 'verAlign':'bottom'},
             False:{'x':0.5, 'y':1, 'horAlign':'center', 'verAlign':'bottom'},
            },
    LEFT_TITLE_LOC:{True:{'x':0, 'y':0., 'horAlign':'right', 'verAlign':'bottom'},
              False:{'x':0, 'y':0.5, 'horAlign':'right', 'verAlign':'center'},
             }
    }


class AnimationDrawAxes(MainDrawAxes):

    log = logging.getLogger(__name__)

    def __init__(self, ID, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, 'x [m]', 'y [m]', *args, **kwargs)
        self.dynamicArtists = {}
        self.timeText = None
        self.labelText = None
        self.showTitleLeftState = None
        self.showTimeIndState = None
        self.showTimeAtTopState = None
        self.showTitleState = None
        self.useCustomTitlesState = None
        self.textsPrp = []
        self.labelString = None
        self.custLabelString = None
        self.custLabelChanged = False

    def setAfterAxLimChangeCallbacks(self, afterXLimChangeFcn, afterYLimChangeFcn):
        self.afterXLimChangeFcn = afterXLimChangeFcn
        self.afterYLimChangeFcn = afterYLimChangeFcn

        self.ax.callbacks.connect('xlim_changed', self.onAfterXLimChange)
        self.ax.callbacks.connect('ylim_changed', self.onAfterYLimChange)

    def onAfterXLimChange(self, event):
        self.isInHomeView = False
        self.afterXLimChangeFcn(event._navigate_mode)

    def onAfterYLimChange(self, event):
        self.isInHomeView = False
        self.afterYLimChangeFcn(event._navigate_mode)

    def drawStaticElements(self, staticElements, label=None):
        self.labelString = label
        self.custLabelString = label

        self.ax.set_aspect('equal')
        for element in staticElements:
            self.drawElement(element)

    def updateElements(self, timeInd, timeStep, dynamicEls, updateEls, checkStates, forceFullUpdate):
        if forceFullUpdate:
            # Remove all dynamic elements
            for artist in self.dynamicArtists.values():
                if artist is not None:
                    artist.remove()

            self.dynamicArtists = {}

        for elementID in dynamicEls['toRemove']:
            artist = self.dynamicArtists[elementID]
            if artist is not None:
                artist.remove()

            self.dynamicArtists.pop(elementID)

        for element in dynamicEls['toAdd']:
            artist = self.drawElement(element)
            self.dynamicArtists[element.ID] = artist

        for element in updateEls:
            self.updateElement(element)

        self.checkTitleStatus(checkStates, timeInd, timeStep)
        self.checkAxisState(checkStates.showAx)

    def checkAxisState(self, showAx):
        if showAx and not self.ax.axison:
            self.ax.set_axis_on()
        elif not showAx and self.ax.axison:
            self.ax.set_axis_off()

    def checkTitleStatus(self, checkStates, timeInd, timeStep):
        if checkStates.showTitle != self.showTitleState or \
         checkStates.showTimeInd != self.showTimeIndState or \
         checkStates.showTitlesLeft != self.showTitleLeftState  or \
         checkStates.showTimeAtTop != self.showTimeAtTopState or \
         False != self.useCustomTitlesState or \
         (self.useCustomTitlesState and self.custLabelChanged):
            self.removeTitles()
            self.showTitleState = checkStates.showTitle
            self.showTimeIndState = checkStates.showTimeInd
            self.showTitleLeftState = checkStates.showTitlesLeft
            self.showTimeAtTopState = checkStates.showTimeAtTop
            self.useCustomTitlesState = False

            self.addTitles()

        self.custLabelChanged = False

        if self.timeText is not None:
            time = timeInd*timeStep
            timeLabel = getTimeLabel(time)

            if checkStates.showTimeInd and not checkStates.showTimeAtTop:
                timeLabel = '{} ({})'.format(timeLabel, timeInd)
            elif checkStates.showTimeInd and checkStates.showTimeAtTop:
                timeLabel = '({})'.format(timeInd)

            self.timeText.set_text(timeLabel)
            self.ax.draw_artist(self.timeText)

    def removeTitles(self):
        if self.labelText is not None:
            self.labelText.remove()
            self.labelText = None
        if self.timeText is not None:
            self.timeText.remove()
            self.timeText = None

    def addTitles(self):
        if not self.showTitleState:
            return

        if self.showTitleLeftState:
            titleLoc = LEFT_TITLE_LOC
        else:
            titleLoc = TOP_TITLE_LOC

        if self.labelString is None:
            showTitle = False
        else:
            showTitle = True

        if self.showTimeAtTopState and not self.showTimeIndState:
            showTime = False
        else:
            showTime = True

        if showTitle:
            titleConfig = TITLE_LABEL_CONFIG[titleLoc][showTime]

            if self.useCustomTitlesState:
                labelString = self.custLabelString
            else:
                labelString = self.labelString

            self.labelText = self.ax.text(titleConfig['x'], titleConfig['y'],
                                          labelString, size=12,
                                          horizontalalignment=titleConfig['horAlign'],
                                          verticalalignment=titleConfig['verAlign'],
                                          transform=self.ax.transAxes, animated=False)
            self.ax.draw_artist(self.labelText)

        if showTime:
            timeConfig = TIME_LABEL_CONFIG[titleLoc][showTitle]
            self.timeText = self.ax.text(timeConfig['x'], timeConfig['y'], '00:00.00',
                                         size=12, horizontalalignment=timeConfig['horAlign'],
                                         verticalalignment=timeConfig['verAlign'],
                                         transform=self.ax.transAxes, animated=True)

    def turnAnimatedOff(self):
        changed = []
        for child in self.ax.get_children():
            if child.get_animated():
                child.set_animated(False)
                changed.append(child)

        return changed

    def turnAnimatedOn(self, childs):
        for child in childs:
            child.set_animated(True)

# ========================================================================================================
    def updateElement(self, element):
        if isinstance(element, PedUpdateEl):
            self.updatePedestrian(element)

    def updatePedestrian(self, pedestrianEl):
        self.dynamicArtists[pedestrianEl.ID].set_center(pedestrianEl.centerCoord)
        if pedestrianEl.isConnected:
            self.dynamicArtists[pedestrianEl.ID].set_edgecolor('r')
        else:
            self.dynamicArtists[pedestrianEl.ID].set_edgecolor('k')
        self.ax.draw_artist(self.dynamicArtists[pedestrianEl.ID])

# ========================================================================================================
    def drawElement(self, element):
        if isinstance(element, ObstacleEl):
            return self.drawObstacle(element)
        if isinstance(element, DestinationEl):
            return self.drawDestination(element)
        if isinstance(element, SourceEl):
            return self.drawSource(element)
        elif isinstance(element, WalkableAreaEl):
            return self.drawWalkableArea(element)
        elif isinstance(element, PedDynamicEl):
            return self.drawPedestrian(element)

    def drawWalkableArea(self, walkableAreaEl):
        artist = Polygon(walkableAreaEl.coords, zorder=1, closed=True, fill=True,facecolor='whitesmoke', edgecolor='k')
        self.ax.add_patch(artist)
        return artist

    def drawObstacle(self, obstacleEl):
        kwargs = {
            'zorder':3,
            }

        lineKwargs = {
            'color':'k'
            }
        lineKwargs.update(kwargs)

        patchKwargs = {
            'fill':True,
            'alpha':0.6,
            'facecolor':'darkgray',
            'edgecolor':lineKwargs['color']
            }
        patchKwargs.update(kwargs)

        if isinstance(obstacleEl, PolygonObstacleEl):
            artist = Polygon(obstacleEl.coords, closed=True, **patchKwargs)
        elif isinstance(obstacleEl, LineObstacleEl):
            artist = Line2D(obstacleEl.coords[0], obstacleEl.coords[1], **lineKwargs)
        elif isinstance(obstacleEl, CircleObstacleEl):
            artist = Circle(obstacleEl.centerCoord, radius=obstacleEl.radius, **patchKwargs)
        elif isinstance(obstacleEl, EllipseObstacleEl):
            artist = Ellipse(obstacleEl.centerCoord, obstacleEl.semiAxesMajor, obstacleEl.semiAxesMinor, obstacleEl.rotation, **patchKwargs)

        if isinstance(obstacleEl, LineObstacleEl):
            self.ax.add_line(artist)
        else:
            self.ax.add_patch(artist)

        return artist

    def drawDestination(self, destinationEl):
        kwargs = {
            'zorder':4,
            }

        lineKwargs = {
            'color':'b',
            'linewidth':4
            }
        lineKwargs.update(kwargs)

        patchKwargs = {
            'fill':False,
            'alpha':0.8,
            'hatch':'x',
            'edgecolor':lineKwargs['color']
            }

        if isinstance(destinationEl, PolygonDestinationEl):
            artist = Polygon(destinationEl.coords, closed=True, **patchKwargs)
        elif isinstance(destinationEl, LineDestinationEl):
            artist = Line2D(destinationEl.coords[0], destinationEl.coords[1], **lineKwargs)
        elif isinstance(destinationEl, PointDestinationEl):
            artist = Circle(destinationEl.coords, radius=0.05, color=lineKwargs['color'], fill=True, **kwargs)

        if isinstance(destinationEl, LineDestinationEl):
            self.ax.add_line(artist)
        else:
            self.ax.add_patch(artist)

        return artist

    def drawSource(self, sourceEl):
        kwargs = {
            'zorder':5,
            }

        lineKwargs = {
            'color':'m',
            'linewidth':3,
            'linestyle':'--'
            }
        lineKwargs.update(kwargs)

        patchKwargs = {
            'fill':False,
            'alpha':0.8,
            'hatch':'*',
            'edgecolor':lineKwargs['color']
            }

        if isinstance(sourceEl, PolygonSourceEl):
            artist = Polygon(sourceEl.coords, closed=True, **patchKwargs)
        elif isinstance(sourceEl, LineSourceEl):
            artist = Line2D(sourceEl.coords[0], sourceEl.coords[1], **lineKwargs)

        if isinstance(sourceEl, LineSourceEl):
            self.ax.add_line(artist)
        else:
            self.ax.add_patch(artist)

        return artist

    def drawPedestrian(self, pedestrianEl):
        kwargs = {
            'edgecolor':'k',
            'linewidth':2,
            'zorder':7,
            'alpha':0.6,
            'animated':True
            }

        artist = Circle((np.NaN, np.NaN), radius=pedestrianEl.radius, facecolor=pedestrianEl.color, **kwargs)
        self.ax.add_patch(artist)
        return artist

# ========================================================================================================

    def addText(self, x, y, label, angle, vAlign='bottom', animated=False):
        if vAlign == 'top':
            label = '\n{} '.format(label)
        else:
            label = '{}\n '.format(label)

        return self.ax.text(x, y, label, rotation=angle, horizontalalignment='center',
                     verticalalignment=vAlign, size=12, linespacing=0.2, animated=animated,
                     clip_on=True, zorder=5)

    def clean(self):
        self.dynamicCells = {}
        self.timeText = None
        self.labelText = None
        self.textsPrp = []
        self.showTitleLeftState = None
        self.showTimeIndState = None
        self.showTimeAtTopState = None
        self.showTitleState = None
        self.useCustomTitlesState = None
        self.labelString = None
        self.custLabelString = None
        self.custLabelChanged = False

    def onScrollZoom(self, scale_factor, xdata, ydata):
        super().onScrollZoom(scale_factor, xdata, ydata, False)
        self.afterXLimChangeFcn(ZOOM_SCROLL_ID)

    def updateCustLabelString(self, newString):
        self.custLabelString = newString
        self.custLabelChanged = True

class RatioLine(Line2D):

    def __init__(self, orig, angle, length, width, *args, **kwargs):
        Line2D.__init__(self, *args, **kwargs)
        self.orig = orig
        self.angle = angle
        self.length = length
        self.width = width
        self.curRatio = None

    def updateRatio(self, streamRatio):
        if streamRatio == -1:
            self.set_visible(False)
            return

        self.set_visible(True)

        if streamRatio == self.curRatio:
            return

        height = streamRatio * self.width

        x = [self.orig[0] + np.sin(np.deg2rad(self.angle)) * height,
             self.orig[0] + np.sin(np.deg2rad(self.angle)) * height + np.sin(np.deg2rad(self.angle + 90)) * self.length]

        y = [self.orig[1] + np.cos(np.deg2rad(self.angle)) * height,
             self.orig[1] + np.cos(np.deg2rad(self.angle)) * height + np.cos(np.deg2rad(self.angle + 90)) * self.length]

        self.set_xdata(x)
        self.set_ydata(y)

# ========================================================================================================

@dataclass
class drawEl():
    ID: str
# --------------------------------
@dataclass
class PolygonEl(drawEl):
    coords: tuple

@dataclass
class LineEl(drawEl):
    coords: tuple

@dataclass
class CircleEl(drawEl):
    centerCoord: tuple
    radius: float

@dataclass
class EllipseEl(drawEl):
    centerCoord: tuple
    semiAxesMajor: float
    semiAxesMinor: float
    rotation: float
# --------------------------------

@dataclass
class WalkableAreaEl(PolygonEl): pass

# --------------------------------

@dataclass
class ObstacleEl(): pass
@dataclass
class PolygonObstacleEl(PolygonEl, ObstacleEl): pass
@dataclass
class LineObstacleEl(LineEl, ObstacleEl): pass
@dataclass
class CircleObstacleEl(CircleEl, ObstacleEl): pass
@dataclass
class EllipseObstacleEl(EllipseEl, ObstacleEl): pass

# --------------------------------
@dataclass
class DestinationEl(): pass
@dataclass
class PolygonDestinationEl(PolygonEl, DestinationEl): pass
@dataclass
class LineDestinationEl(LineEl, DestinationEl): pass
@dataclass
class PointDestinationEl(LineEl, DestinationEl): pass

# --------------------------------
@dataclass
class SourceEl(): pass
@dataclass
class PolygonSourceEl(PolygonEl, SourceEl): pass
@dataclass
class LineSourceEl(LineEl, SourceEl): pass

# --------------------------------

@dataclass
class PedDynamicEl(drawEl):
    radius: float
    color: str

@dataclass
class PedUpdateEl(drawEl):
    centerCoord: tuple
    isConnected: bool = False

# ========================================================================================================


def numpy_gcd(a, b):
    a, b = np.broadcast_arrays(a, b)
    a = a.copy()
    b = b.copy()
    pos = np.nonzero(b)[0]
    while len(pos) > 0:
        b2 = b[pos]
        a[pos], b[pos] = b2, a[pos] % b2
        pos = pos[b[pos] != 0]
    return a
