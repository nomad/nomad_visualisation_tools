""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import copy
import logging
from pathlib import Path

import matplotlib
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg
from matplotlib.figure import Figure
from pubsub import pub
import wx

from NOMAD_results_gui.axes_divider import LEFT_GAP, RIGHT_GAP, H_GAP, BOTTOM_GAP, TOP_GAP, V_GAP
from NOMAD_results_gui.axes_divider.axes_grid import AxesGrid
from NOMAD_results_gui.results_gui.cust_wx_elements import Panel
from NOMAD_results_gui.results_gui.main_draw_axes import MainDrawAxes
from NOMAD_results_gui.results_gui.navigation_tool_bar import ZOOM_ID, PAN_ID, HOME_ID, GRID_ID, \
    NavigationToolbar


matplotlib.use('WXAgg')

class MatplotLibCanvasPanel(Panel):
    '''classdocs'''

    DEF_GAPS = {
        LEFT_GAP:0.7,
        RIGHT_GAP:0.2,
        H_GAP:0.3,
        BOTTOM_GAP:0.6,
        TOP_GAP:0.2,
        V_GAP:0.3,
        }

    DEF_AXES_GRID_CONFIG = copy.deepcopy(DEF_GAPS)

    KEY_2_TOOLID = {
        'z':ZOOM_ID,
        'p':PAN_ID,
        'h':HOME_ID,
        'g':GRID_ID,
        }

    log = logging.getLogger(__name__)

    def __init__(self, parentFrame, tabID, *args, **kwargs):
        '''Constructor'''
        Panel.__init__(self, *args, **kwargs)
        self.tabID = tabID
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)
        self.scrollEventId = None
        self.ax2homeLims = {}
        self.ID2axInd = {}
        self.initMatplotlib()
        self.addToolbar(parentFrame)
        self.ax2GridState = {}
        self.timeLine = None

    def addToolbar(self, parentFrame):
        self.toolBar = NavigationToolbar(self, parentFrame, self.canvas)
        self.sizer.Add(self.toolBar.wxToolBar, 0, wx.EXPAND)
        self.sizer.Add(self.canvas, 1, wx.EXPAND)

    def initMatplotlib(self):
        self.figure = Figure(dpi=wx.GetDisplayPPI()[0])
        self.canvas = FigureCanvas(self.GetGrandParent(), self, -1, self.figure)
        self.canvas.SetWindowStyle(wx.WANTS_CHARS)

        self.createAxes()
        self.canvas.mpl_connect('figure_enter_event', self.onFigEnter)
        self.canvas.mpl_connect('figure_leave_event', self.onFigLeave)
        self.canvas.mpl_connect('key_press_event', self.onKeyPress)

    def resetToDefault(self):
        super().clean()
        self.createAxes()

    def clean(self):
        self.ax2homeLims = {}
        self.ID2axInd = {}
        self.ax2GridState = {}
        self.timeLine = None
        self.canvas.clean()

    def getDividerGroup(self):
        axesGrid = AxesGrid([1], [1], valueDict=self.DEF_AXES_GRID_CONFIG)
        axesGrid.addDataAxes([MainDrawAxes('{}_0'.format(self.tabID), '', '', [0, 0])])
        return axesGrid

    def createAxes(self, **kwargs):
        self.figure.clf()

        axesGrid = self.getDividerGroup(**kwargs)

        if len(axesGrid.dataAxes) == 0:
            return

        self.axesGrid = axesGrid
        self.axesGrid.createAndLocateAxes(self.figure)
        for dataAx in self.axesGrid.dataAxes.values():
            self.ax2homeLims[dataAx.ID] = (dataAx.ax.get_xlim(), dataAx.ax.get_ylim())

    def finalizePlot(self, showLegend=False, doRelim=True, dataLims={'x':(0,1), 'y':(0,1)}, gapFactors={'x':0, 'y':0}, aspect='auto', legendInfo=None):
        for axID, dataAx in self.axesGrid.dataAxes.items():
            if axID in dataLims:
                dataLims_ii = dataLims[axID]
            else:
                dataLims_ii = dataLims
            if axID in gapFactors:
                gapFactors_ii = gapFactors[axID]
            else:
                gapFactors_ii = gapFactors

            newLims = dataAx.finalizePlot(showLegend, doRelim, dataLims_ii, gapFactors_ii, aspect,
                                          gridOn=self.toolBar.gridOn, legendInfo=legendInfo)
            self.ax2homeLims[dataAx.ID] = (newLims['x'], newLims['y'])

    def saveFigure(self, bbox_inches=None):
        kwargs = {}
        if bbox_inches is not None:
            kwargs['bbox_inches'] = bbox_inches

        filename, fileFormat = self.getImageSaveFilename()
        if filename is None:
            return

        self.preSaveFcn()
        self.figure.savefig(filename, format=fileFormat, **kwargs)
        self.postSaveFcn()

    def saveAxes(self, axLabel):
        ax = None
        for dataAx in self.axesGrid.dataAxes.values():
            if axLabel is None or dataAx.ax.get_label() == axLabel:
                ax = dataAx.ax
                break

        if ax is None:
            # TO DO: Ask user to select one of the axes (by label if they do not have (unique) titles) in dialog
            # For now, return the first axes
            ax = self.axesGrid.getDataAx().ax
            self.log.info('Currently selects first data axes!')

        extent = ax.get_window_extent().transformed(self.figure.dpi_scale_trans.inverted())
        self.saveFigure(extent)

    def onFigEnter(self, event):
        self.scrollEventId = self.canvas.mpl_connect('scroll_event', self.onScrollZoom)

    def onFigLeave(self, event):
        if self.scrollEventId is not None:
            self.canvas.mpl_disconnect(self.scrollEventId)

    def home(self, axLabel):
        for dataAx in self.axesGrid.dataAxes.values():
            if axLabel is None or dataAx.ax.get_label() == axLabel:
                dataAx.ax.set_xlim(self.ax2homeLims[dataAx.ID][0], emit=False)
                dataAx.ax.set_ylim(self.ax2homeLims[dataAx.ID][1], emit=False)

        self.canvas.draw(forceBlitReset=True)

    def onScrollZoom(self, event):
        if event.inaxes is None:
            return

        base_scale = 1.2
        xdata = event.xdata  # get event x location
        ydata = event.ydata  # get event y location
        if event.button == 'up':
            # deal with zoom in
            scale_factor = 1 / base_scale
        elif event.button == 'down':
            # deal with zoom out
            scale_factor = base_scale
        else:
            # deal with something that should never happen
            scale_factor = 1

        for dataAx in self.axesGrid.dataAxes.values():
            if dataAx.ax == event.inaxes:
                dataAx.onScrollZoom(scale_factor, xdata, ydata)

        self.canvas.draw()

    def gridOnOff(self, axLabel, onOff):
        for dataAx in self.axesGrid.dataAxes.values():
            if axLabel is None or dataAx.ax.get_label() == axLabel:
                dataAx.ax.grid(onOff)
                self.ax2GridState[dataAx.ax.get_label()] = onOff

        self.canvas.draw()

    def onKeyPress(self, event):
        if event.key in self.KEY_2_TOOLID:
            mousePos = wx.GetMousePosition()
            self.toolBar.executeAction(self.KEY_2_TOOLID[event.key], self, mousePos)

    def getImageSaveFilename(self):
            filetypes, exts, filter_index = self.canvas._get_imagesave_wildcards()
            default_file = self.canvas.get_default_filename()
            dlg = wx.FileDialog(wx.TopLevelWindow(self), "Save to file", "", default_file,
                                filetypes,
                                wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
            dlg.SetFilterIndex(filter_index)
            if dlg.ShowModal() != wx.ID_OK:
                return None, None

            dirPath = Path(dlg.GetDirectory())
            filePath = Path(dlg.GetFilename())
            fileFormat = Path(exts[dlg.GetFilterIndex()])
            ext = fileFormat.suffix
            if ext.startswith('.'):
                ext = ext[1:]
            if ext in ('svg', 'pdf', 'ps', 'eps', 'png') and format != ext:
                fileFormat = ext

            return dirPath.joinpath(filePath), fileFormat

    def showTimeLine(self, timeValue):
        if self.timeLine is not None:
            return

        self.timeLine = self.axesGrid.getDataAx().ax.axvline(timeValue)
        self.log.info('Currently selects first data axes!')
        self.canvas.draw()
        self.log.debug('Added time line {}'.format(self.__class__))

    def updateTimeLine(self, timeValue):
        if self.timeLine is not None:
            self.timeLine.set_xdata([timeValue,timeValue])
            self.canvas.draw()

    def hideTimeLine(self):
        if self.timeLine is not None:
            self.log.debug('Removed time line {}'.format(self.__class__))
            try:
                self.timeLine.remove()
            except:
                pass
            self.canvas.draw()
            self.timeLine = None

    def preSaveFcn(self):
        pass

    def postSaveFcn(self):
        pass


wxEVT_FIGURECANVAS_RESIZE = wx.NewEventType()
EVT_FIGURECANVAS_RESIZE = wx.PyEventBinder(wxEVT_FIGURECANVAS_RESIZE, 1)
ON_ENTER_CANVAS = 'onEnterCanvas'

class FigureCanvas(FigureCanvasWxAgg):

    log = logging.getLogger(__name__)

    TOP_LOC = 'top'
    BOTTOM_LOC = 'bottom'
    LEFT_LOC = 'left'
    RIGHT_LOC = 'right'

    LOCS = (TOP_LOC, BOTTOM_LOC, LEFT_LOC, RIGHT_LOC)

    loc2fldNm = {
        TOP_LOC:'add2Top',
        BOTTOM_LOC:'add2Bottom',
        LEFT_LOC:'add2Left',
        RIGHT_LOC:'add2Right'
        }

    def __init__(self, tabParent, *args, **kwargs):
        FigureCanvasWxAgg.__init__(self, *args, **kwargs)
        self.tabParent = tabParent
        self.doTextResize = False
        self.doBlitUpdate = False
        self.afterResizeFcn = None
        self.doResizePaint = False
        self.blitBackground = None
        self.axLimChangeFcn = None
        self.axLimChanged = False
        self.isRecording = False

        self.dataAxes = []
        self.textPerAx = []
        self.flagsPerAx = []

        self.Bind(EVT_FIGURECANVAS_RESIZE, self.onResize)

    def clean(self):
        self.doTextResize = False
        self.doBlitUpdate = False
        self.afterResizeFcn = None
        self.doResizePaint = False
        self.blitBackground = None
        self.axLimChangeFcn = None
        self.axLimChanged = False

        self.dataAxes = []
        self.textPerAx = []
        self.flagsPerAx = []

    def setTextResize(self, dataAx, text, flags=[]):
        self.dataAxes.append(dataAx)
        self.textPerAx.append(text)
        if not isinstance(flags, (list, tuple)):
            flags = [flags, ]

        self.flagsPerAx.append(flags)
        self.doTextResize = True
        self.textDataHeight = 0

    def addTextResize(self, dataAx, text):
        ind = self.dataAxes.index(dataAx)
        self.textPerAx[ind] = text

    def removeTextResize(self, texts):
        for text in texts:
            if text in self.textPerAx:
                ind = self.textPerAx.index(text)
                self.textPerAx[ind] = None

    def _onEnter(self, event):
        FigureCanvasWxAgg._onEnter(self, event)
        pub.sendMessage(ON_ENTER_CANVAS, window=self.tabParent)
        self.SetFocus()
        event.Skip()

    def resetBlitBackground(self):
        #self.log.debug('Resetting blit background')
        self.blitBackground = self.copy_from_bbox(self.figure.bbox)

    def restore_region(self, bbox=None, xy=None):
        region = self.blitBackground
        if region is None:
            return
        return super().restore_region(region, bbox=bbox, xy=xy)

    def draw(self, drawDC=None, forceBlitReset=False, isInternalDrawCall=False):
        super().draw(drawDC)
        if not(isInternalDrawCall or self.axLimChanged or self.doResizePaint or forceBlitReset or self.isRecording) and self.blitBackground is not None:
            self.onResize(None)
            return

        if self.axLimChanged:
            self.axLimChanged = False
            if self.axLimChangeFcn is not None:
                self.axLimChangeFcn()
            if not(self.doResizePaint or forceBlitReset):
                self.draw(isInternalDrawCall=True)
                self.resetBlitBackground()
                if self.axLimChangeFcn is not None:
                    self.axLimChangeFcn()


        if self.doResizePaint or forceBlitReset:
            self.onResize(None)

    def _onSize(self, evt):
        super()._onSize(evt)
        # Deal with resize of the canvas after it has been initially resized
        if self.doTextResize or self.doBlitUpdate:
            self.doResizePaint = True

    def onResize(self, event):
        # Take care of the limits regarding the text
        if self.doTextResize:
            #self.log.debug('Resize')
            for ii in range(len(self.dataAxes)):
                if not(self.dataAxes[ii].isInHomeView):
                    continue
                margins = {}
                if self.textPerAx[ii] is None:
                    textHeight = 0
                else:
                    textHeight = self.getTextHeight(self.textPerAx[ii])
                for loc in self.flagsPerAx[ii]:
                    margins[self.loc2fldNm[loc]] = textHeight

                #self.log.debug('{}'.format(self.textDataHeight))
                self.dataAxes[ii].updateLims(**margins)

        self.doResizePaint = False
        self.draw(isInternalDrawCall=True)
        # Take care of the blit
        if self.doBlitUpdate:
            self.resetBlitBackground()
            self.afterResizeFcn()

    def getTextHeight(self, text):
        ax = text._axes
        renderer = ax.get_renderer_cache()
        textBox = text.get_window_extent(renderer)
        dataBox = textBox.transformed(ax.transData.inverted())
        return dataBox.y1 - dataBox.y0

    def setBlitState(self, blitState, afterResizeFcn):
        self.doBlitUpdate = blitState
        self.afterResizeFcn = afterResizeFcn
        assert(not(blitState) or (blitState and afterResizeFcn is not None))

    def setAxLimChangeFcn(self, axLimChangeFcn):
        self.axLimChangeFcn = axLimChangeFcn
