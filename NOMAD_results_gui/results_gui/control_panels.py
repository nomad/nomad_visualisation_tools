""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from pubsub import pub
import wx

import numpy as np
from NOMAD_results_gui.results_gui.cust_wx_elements import Panel


DEF_FIELD_FORMATS = ['{:.4f}', '{:.4f}']

class PlotControlPanel(Panel):
    '''
    classdocs
    '''

    def __init__(self, parent, setLabelsMsgID, cleanMsgID, fieldLabels=['x', 'y'], fieldFormats=DEF_FIELD_FORMATS):
        '''
        Constructor
        '''
        Panel.__init__(self, parent)
        self.parent = parent
        self.setLabelsMsgID = setLabelsMsgID
        self.cleanMsgID = cleanMsgID
        self.fieldLabels = fieldLabels
        self.fieldFormats = fieldFormats

        self.fields = []
        self.label2ind = {}

        self.timeLineCheckBox = None
        self.timeLineStatusMsgID = None

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)

        self.label = wx.StaticText(self, label = 'NA')
        self.sizer.Add(self.label, 0, wx.TOP|wx.ALIGN_LEFT, 10)
        self.sizer.Add((0,5),0)
        self.setFields()

        pub.subscribe(self.setLabels, setLabelsMsgID)
        pub.subscribe(self.clean, cleanMsgID)

    def clean(self):
        self.setLabels('NA', {label:'NA' for label in self.fieldLabels})

    def clearFields(self):
        for field in self.fields:
            field.Destroy()

        self.fields = []
        self.label2ind = {}

    def setFields(self):
        for ii in range(len(self.fieldLabels)):
            fieldLabel = self.fieldLabels[ii]
            field = wx.StaticText(self, label = '{} = NA'.format(fieldLabel))
            self.fields.append(field)
            self.sizer.Add(field, 0, wx.TOP|wx.ALIGN_LEFT, 5)
            self.label2ind[fieldLabel] = ii

    def updateConfig(self, fieldLabels, showTimeLineCheckBox, fieldFormats=DEF_FIELD_FORMATS, showTimeLineStatus=True, msgID=None):
        if self.timeLineCheckBox is not None:
            status = self.timeLineCheckBox.GetValue()
        else:
            status = None

        self.fieldLabels = fieldLabels
        self.fieldFormats = fieldFormats

        self.clearFields()

        if not showTimeLineCheckBox and self.timeLineCheckBox is not None:
            self.timeLineCheckBox.Destroy()
            self.timeLineCheckBox = None
            self.timeLineStatusMsgID = None

        self.setFields()

        if showTimeLineCheckBox and self.timeLineCheckBox is None:
            self.addShowTimeLineCheckBox(msgID, showTimeLineStatus)

        self.Layout()

        return status

    def addShowTimeLineCheckBox(self, msgID, status=True):
        self.timeLineCheckBox = wx.CheckBox(self, label='Show time line')
        self.timeLineCheckBox.SetValue(status)
        self.timeLineCheckBox.Bind(wx.EVT_CHECKBOX, self.onShowTimeLine)
        self.GetSizer().Add(self.timeLineCheckBox, 0, wx.TOP|wx.ALIGN_LEFT, 10)
        self.timeLineStatusMsgID = msgID
        self.onShowTimeLine(None)

    def setLabel(self, label, value):
        try:
            if np.isnan(value):
                value = 'NA'
        except:
            pass

        ind = self.label2ind[label]

        if not isinstance(value, str):

            value = self.fieldFormats[ind].format(value)

        self.fields[ind].SetLabel('{} = {}'.format(self.fieldLabels[ind], value))

    def setLabels(self, label, fieldValues):
        self.label.SetLabel(label)

        for label, value in fieldValues.items():
            self.setLabel(label, value)

        self.Refresh()
        self.parent.Layout()

    def onShowTimeLine(self, event):
        pub.sendMessage(self.timeLineStatusMsgID, showTimeLine=self.timeLineCheckBox.GetValue())

    def onDelete(self):
        pub.unsubscribe(self.setLabels, self.setLabelsMsgID)
        pub.unsubscribe(self.clean, self.cleanMsgID)

class MultiCollapsiblePanel(Panel):
    '''
    classdocs
    '''

    def __init__(self, parent):
        '''
        Constructor
        '''
        Panel.__init__(self, parent)
        self.parent = parent

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)
        self.Bind(wx.EVT_COLLAPSIBLEPANE_CHANGED, self.onCollapisblePaneChanged)
        self.Bind(wx.EVT_SIZE, self.onResize)

        self.ratios = []
        self.collapsiblePanes = []
        self.contentPanels = []
        self.setMargin()

    def setMargin(self):
        collapisblePane = wx.CollapsiblePane(self, style=wx.CP_NO_TLW_RESIZE)
        minContentHeight = 20
        collapisblePane.GetPane().SetMinSize((5,minContentHeight))
        collapisblePane.Expand()
        self.Layout()
        expandedHeight = collapisblePane.GetSize()[1]
        collapisblePane.Collapse()
        self.Layout()
        self.minHeight = collapisblePane.GetSize()[1]
        self.margin = expandedHeight - self.minHeight - minContentHeight
        collapisblePane.Destroy()

    def addPane(self, label, collapsed, contentPanelCreateFcn, *args, **kwargs):
        collapsiblePane = wx.CollapsiblePane(self, label=label, style=wx.CP_NO_TLW_RESIZE)
        contentPanel = contentPanelCreateFcn(collapsiblePane.GetPane(), *args, **kwargs)
        paneSizer = wx.BoxSizer(wx.VERTICAL)
        collapsiblePane.GetPane().SetSizer(paneSizer)
        paneSizer.Add(contentPanel, 0, wx.ALL, 3)
        paneSizer.Add((0,3), 0)

        self.sizer.Add(collapsiblePane, 0, wx.ALIGN_TOP)
        self.collapsiblePanes.append(collapsiblePane)
        self.contentPanels.append(contentPanel)

        collapsiblePane.Expand()

        contentPanelHeights = []
        for ii in range(len(self.collapsiblePanes)):
            collapsiblePane = self.collapsiblePanes[ii]
            contentPanelHeights.append(self.contentPanels[ii].GetBestVirtualSize()[1])

        totalContentPanelHeight = sum(contentPanelHeights)
        self.ratios = [contentPanelHeight/totalContentPanelHeight for contentPanelHeight in contentPanelHeights]
        if collapsed:
            collapsiblePane.Collapse()

        self.onCollapisblePaneChanged(None)

    def onResize(self, event):
        self.onCollapisblePaneChanged(None)

    def onCollapisblePaneChanged(self, event):
        paneCount = len(self.collapsiblePanes)

        ratioSum = sum([self.ratios[ii] for ii in range(paneCount) if self.collapsiblePanes[ii].IsExpanded()])
        expandedCount = sum([1 for pane in self.collapsiblePanes if pane.IsExpanded()])
        paneHeight = self.GetSize()[1] - self.minHeight*paneCount - self.margin*expandedCount

        for ii in range(paneCount):
            if self.collapsiblePanes[ii].IsExpanded():
                paneHeight_ii = max([paneHeight*self.ratios[ii]/ratioSum, 10])
                #self.collapsiblePanes[ii].GetPane().SetMinSize((self.GetSize()[0], paneHeight_ii))
                self.collapsiblePanes[ii].GetPane().SetMaxSize((self.GetSize()[0], paneHeight_ii))
                self.collapsiblePanes[ii].SetMinSize((self.GetSize()[0], -1))
            else:
                self.collapsiblePanes[ii].SetMinSize((self.GetSize()[0], self.minHeight))
        self.Layout()

    def clean(self):
        for pane in self.collapsiblePanes:
            pane.Destroy()
        self.ratios = []
        self.collapsiblePanes = []
        self.contentPanels = []

def addSelectDeselectButtons(parent, sizer, onSelectFcn, onDeselectFcn):
    horSizer = wx.BoxSizer(wx.HORIZONTAL)
    buttonSize = (76,30)
    selectAllButton = wx.Button(parent, label='Select all')
    deselectAllButton = wx.Button(parent, label='Deselect all')
    selectAllButton.SetMinSize(buttonSize)
    selectAllButton.Bind(wx.EVT_BUTTON, onSelectFcn)
    deselectAllButton.SetMinSize(buttonSize)
    deselectAllButton.Bind(wx.EVT_BUTTON, onDeselectFcn)

    horSizer.Add(selectAllButton, 0)
    horSizer.Add(deselectAllButton, 0, wx.LEFT, 5)
    sizer.Add(horSizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)