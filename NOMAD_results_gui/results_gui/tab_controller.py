""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging

from pubsub import pub
import wx

from NOMAD_results_gui.results_gui import data_menu, title_config_xml
from NOMAD_results_gui.results_gui.count_graph_tab import CountGraphTab
from NOMAD_results_gui.results_gui.density_graph_tab import DensityGraphTab
from NOMAD_results_gui.results_gui.travel_time_tab import TravelTimeTab
import wx.lib.agw.aui as aui
from NOMAD_results_gui.results_gui.animation_tab import AnimationTab


TAB_TYPE_2_TAB = {AnimationTab.getType():AnimationTab,
                  DensityGraphTab.getType():DensityGraphTab,
                  CountGraphTab.getType():CountGraphTab,
                  TravelTimeTab.getType():TravelTimeTab}
TAB_TYPES = (AnimationTab.getType(), DensityGraphTab.getType(),
             CountGraphTab.getType(), TravelTimeTab.getType())
TAB_TYPES = (AnimationTab.getType(), )

BASE_TABS = ((AnimationTab.getType(),),)
#BASE_TABS = ((NetworkTab.getType(),), (DensityGraphTab.getType(),))
#BASE_TABS = ((NetworkTab.getType(),), )

class TabController():
    '''classdocs'''

    log = logging.getLogger(__name__)

    def __init__(self, notebook, parentFrame):
        '''Constructor'''
        self.notebook = notebook
        self.parentFrame = parentFrame
        self.tabs = {}

        self.notebook.Bind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.onCloseTab)
        self.notebook.Bind(aui.EVT_AUINOTEBOOK_TAB_RIGHT_DOWN, self.onTabContext)
        self.notebook.Bind(aui.EVT_AUINOTEBOOK_BG_RIGHT_UP, self.onTabBarContext)
        self.notebook.Bind(wx.EVT_CONTEXT_MENU, self.onNotebookContext)

        isFirstGroup = True
        firstTab = None
        for tabGroup in BASE_TABS:
            isFirstInNewGroup = True
            for tabType in tabGroup:
                tab = self.addTab(tabType)
                if firstTab is None:
                    firstTab = tab
                if not isFirstGroup and isFirstInNewGroup:
                    idx = self.notebook.GetPageIndex(tab)
                    self.notebook.Split(idx, wx.RIGHT)

                isFirstInNewGroup = False
            isFirstGroup = False

        if BASE_TABS:
            self.notebook.SetSelection(self.notebook.GetPageIndex(firstTab))

        from NOMAD_results_gui.results_gui.divider_config_xml import UPDATE_GAPS_MSG_ID

        pub.subscribe(self.dataSetAddedListener, data_menu.DATA_ADDED_MSG_ID)
        pub.subscribe(self.dataSetRemovedListener, data_menu.DATA_REMOVED_MSG_ID)
        pub.subscribe(self.dataSetsReloadedListener, data_menu.DATA_RELOADED_MSG_ID)
        pub.subscribe(self.dataSetsPreReloadListener, data_menu.DATA_RELOADED_PRE_MSG_ID)
        pub.subscribe(self.onUpdateDividerGaps, UPDATE_GAPS_MSG_ID)
        pub.subscribe(self.onUpdateTitles, title_config_xml.UPDATE_TITLES_MSG_ID)

    def addTab(self, tabType, tabCtrl=None):
        tabNr = 0
        tabID = TAB_TYPE_2_TAB[tabType].createID(TAB_TYPE_2_TAB[tabType], tabNr)
        while tabID in self.tabs:
            tabNr += 1
            tabID = TAB_TYPE_2_TAB[tabType].createID(TAB_TYPE_2_TAB[tabType], tabNr)

        tab = TAB_TYPE_2_TAB[tabType](tabID, self.parentFrame, self.parentFrame.dataMenu, parent=self.notebook)

        if tabCtrl is not None:
            idx = self.notebook.GetPageIndex(tabCtrl._pages[-1].window) + 1
            self.notebook.InsertPage(idx, tab, tab.getShortLabel(), select=True)
        else:
            self.notebook.AddPage(tab, tab.getShortLabel())

        self.log.info('Added a {} tab'.format(tabType))
        self.tabs[tab.ID] = tab

        if self.parentFrame.simResults is not None:
            tab.setSimResults(self.parentFrame.simResults)

        return tab

    def addTabContextMenu(self, tabCtrl=None):
        contextMenu = wx.Menu('Add a tab')

        self.addContextMenuId2type = {}
        self.addContextMenuTabCtrl = tabCtrl
        ii = 1
        for tabType in TAB_TYPES:
            tabLabel = TAB_TYPE_2_TAB[tabType].getLabel()
            item = contextMenu.Append(wx.ID_ANY, '{}. {} tab'.format(ii, tabLabel), 'Add a {} tab'.format(tabLabel))
            contextMenu.Bind(wx.EVT_MENU, self.onAddTabContext, item)
            self.addContextMenuId2type[item.Id] = tabType
            #if tabType in self.tabs:
            #    item.Enable(False)
            ii += 1

        self.parentFrame.PopupMenu(contextMenu)

        contextMenu.Destroy()
        self.addContextMenuId2type = {}
        self.addContextMenuTabCtrl = None

    def dataSetAddedListener(self, addToAll):
        if addToAll:
            for tab in self.tabs.values():
                for ID in self.parentFrame.dataMenu.ids:
                    tab.onAddDataSet(ID=ID)

    def dataSetRemovedListener(self, ID):
        for tab in self.tabs.values():
            tab.onRemoveDataSet(ID=ID)

        self.parentFrame.Layout()

    def dataSetsReloadedListener(self, ids):
        for tab in self.tabs.values():
            tab.reloadDataSets(ids)

        self.parentFrame.Layout()

    def dataSetsPreReloadListener(self):
        for tab in self.tabs.values():
            tab.preReloadDataSets()

    def clean(self):
        for tab in self.tabs.values():
            tab.clean()
            tab.contentPanel.canvas.draw()

    def onCloseTab(self, event):
        tab = self.notebook.GetPage(event.Selection)
        tab.onDelete()

        self.tabs.pop(tab.ID)
        self.log.info('Closed a tab')

    def onAddTabContext(self, event):
        self.addTab(self.addContextMenuId2type[event.Id], self.addContextMenuTabCtrl)

    def onAddTab(self, event):
        self.addTab(self.addMenuId2type[event.Id])

    def onTabContext(self, event):
        pass

    def onTabBarContext(self, event):
        self.addTabContextMenu(event.EventObject)

    def onNotebookContext(self, event):
        self.addTabContextMenu()

    def getGapsPerTabType(self):
        gapsPerTabType = {}
        for tab in self.tabs.values():
            if tab.getType() in gapsPerTabType:
                continue

            gapsPerTabType[tab.getType()] = tab.getDividerGaps()

        return gapsPerTabType

    def onUpdateDividerGaps(self, gapsPerTabType):
        for tab in self.tabs.values():
            tab.updateDividerGaps(gapsPerTabType[tab.getType()])

    def getTitlesPerTab(self):
        titlesPerTab = {}
        for tabID, tab in self.tabs.items():
            if tab.getType() == AnimationTab.getType():
                titlesPerTab[tabID] = tab.getTitles()

        return titlesPerTab

    def onUpdateTitles(self, titlesPerTab):
        for tabID, tab in self.tabs.items():
            if tab.getType() == AnimationTab.getType():
                tab.updateTitles(titlesPerTab[tabID])
