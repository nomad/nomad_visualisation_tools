""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging

from matplotlib import ticker
from pubsub import pub
import wx

import numpy as np
from NOMAD_results_gui.results_gui.control_panels import addSelectDeselectButtons
from NOMAD_results_gui.results_gui.cust_wx_elements import CheckListBox
from NOMAD_results_gui.results_gui.data_tab import DataTab
from NOMAD_results_gui.results_gui.matplotlib_canvas_panel import MatplotLibCanvasPanel
from copy import deepcopy
from matplotlib.patches import Rectangle

class TravelTimeTab(DataTab):
    '''classdocs'''

    def __init__(self, ID, parentFrame, dataMenu, *args, **kwargs):
        '''Constructor'''
        DataTab.__init__(self, ID, parentFrame, dataMenu, canShowTimeLine=True, *args, **kwargs)
        self.IDsPlotted = []
        self.ID2scatterData = {}
        self.ID2histData = {}

        self.curPlotType = None

        self.timeLineShown = self.controlPanel.showTimeLineStatus

        pub.subscribe(self.onChangeDataDisplayed, self.getChangeDataMsgID())
        pub.subscribe(self.onChangePlotType, self.getChangePlotTypeMsgID())

        self.onChangePlotType(SCATTER_TYPE)

    def initControlPanel(self, *args, **kwargs):
        return TravelTimeControlPanel(self.getChangeDataMsgID(),
                                      self.getChangePlotTypeMsgID(),
                                      self.getSetLabelsMsgID(),
                                      self.getChangeTimeLineStatusMsgID(),
                                      self.getCleanMsgID(),
                                      *args, **kwargs)

    def initContentPanel(self, *args, **kwargs):
        return TravelTimeContentPanel(self, self.parentFrame, self.ID, *args, **kwargs)

    def addDataSet(self, ID):
        label = self.dataMenu.id2label[ID]
        self.controlPanel.addDataSet(ID, label)
        self.Layout()

        travelTimes = self.dataMenu.dataSets[ID].travelTimes
        if travelTimes is None:
            return

        departureTimeVsTT = travelTimes.versusDepartureTime
        scatterData = {'xData':departureTimeVsTT[:,0], 'yData':departureTimeVsTT[:,1], 'label':label}
        self.ID2scatterData[ID] = scatterData
        histData = {'data':travelTimes.total, 'label':label}
        self.ID2histData[ID] = histData

    def removeDataSet(self, ID):
        label = self.dataMenu.id2label[ID]
        self.controlPanel.removeDataSet(ID, label)

        # Remove any histogram or scatter from the content panel
        self.onChangeDataDisplayed(ID)

        self.ID2scatterData.pop(ID)
        self.ID2histData.pop(ID)
        self.Layout()

    def reloadDataSet(self, ID):
        if ID in self.dataSetIds:
            self.removeDataSet(ID)
            self.addDataSet(ID)

    def clean(self):
        self.controlPanel.clean()
        self.contentPanel.clean()
        self.IDsPlotted = []
        self.ID2scatterData = {}
        self.ID2histData = {}

    def onDelete(self):
        super(TravelTimeTab, self).onDelete()
        pub.unsubscribe(self.onChangeDataDisplayed, self.getChangeDataMsgID())
        pub.unsubscribe(self.onChangePlotType, self.getChangePlotTypeMsgID())
        self.controlPanel.onDelete()

    def onChangePlotType(self, plotType):
        if plotType == self.curPlotType:
            return

        self.curPlotType = plotType
        self.controlPanel.changePlotType(plotType)
        if plotType == SCATTER_TYPE:
            IDsandData = {ID:self.ID2scatterData[ID] for ID in self.IDsPlotted}
            self.contentPanel.createScatterPlot(IDsandData)
            self.onChangeTimeLineStatus(self.timeLineShown)
        elif plotType == HIST_TYPE:
            IDsandData = {ID:self.ID2histData[ID] for ID in self.IDsPlotted}
            self.contentPanel.createHistogramPlot(IDsandData)

    def onChangeDataDisplayed(self, dataID):
        if dataID in self.IDsPlotted:
            self.contentPanel.removeDataFromPlot(dataID)
            self.IDsPlotted.remove(dataID)
        else:
            self.IDsPlotted.append(dataID)
            if self.contentPanel.curType == SCATTER_TYPE:
                data2add = self.ID2scatterData[dataID]
                self.onChangeTimeLineStatus(self.timeLineShown)
            elif self.contentPanel.curType == HIST_TYPE:
                data2add = self.ID2histData[dataID]

            self.contentPanel.addDataToPlot(dataID, data2add)

    def onChangeTimeLineStatus(self, showTimeLine):
        self.timeLineShown = showTimeLine
        if self.contentPanel.curType == SCATTER_TYPE:
            super(TravelTimeTab, self).onChangeTimeLineStatus(showTimeLine)

    def onChangeTime(self, timeInSeconds):
        self.lastTimeValue = self.getTimeValue(timeInSeconds)
        if self.contentPanel.curType == SCATTER_TYPE:
            super(TravelTimeTab, self).onChangeTime(timeInSeconds)

    def getSetLabelsMsgID(self):
        return 'setXyValues_{}'.format(self.ID)

    def getChangeTimeLineStatusMsgID(self):
        return 'changeTimeLineStatus_{}'.format(self.ID)

    def getChangeDataMsgID(self):
        return 'changeData_{}'.format(self.ID)

    def getChangePlotTypeMsgID(self):
        return 'changePlotType_{}'.format(self.ID)

    def getCleanMsgID(self):
        return 'clean_{}_{}'.format(self.getType(), self.ID)

    def preAddDataSets(self):
        pass

    def preRemoveDataSets(self):
        pass

    def preReloadDataSets(self):
        pass

    def postAddDataSets(self):
        pass

    def postRemoveDataSets(self):
        pass

    def postReloadDataSets(self):
        pass

    def addTimeLine(self):
        pass

    def getTimeValue(self, timeInSeconds):
        return timeInSeconds

    @staticmethod
    def getType():
        return 'travel_time_graph'

    @staticmethod
    def getLabel():
        return 'Travel times'

    @staticmethod
    def getShortLabel():
        return 'Travel times'

SCATTER_TYPE = 'scatter'
HIST_TYPE = 'histogram'
TYPE_2_LABEL = {
    SCATTER_TYPE: 'Travel times vs. departure times',
    HIST_TYPE: 'Travel times histogram'
    }

LABEL_2_TYPE = {value:key for key, value in TYPE_2_LABEL.items()}

TYPE_2_FIELDS = {
    SCATTER_TYPE: ('x', 'y'),
    HIST_TYPE: ('bin', 'bin size')
    }

class TravelTimeControlPanel(wx.Panel):

    def __init__(self, onChangeDataMsgID, changePlotTypeMsgID, setLabelsMsgID, changeTimeLineStatusMsgID, cleanMsgID, *args, **kwargs):
        wx.Panel.__init__(self, *args, **kwargs)

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)
        self.sizer = sizer

        self.showTimeLineStatus = True

        self.onChangeDataMsgID = onChangeDataMsgID
        self.changePlotTypeMsgID = changePlotTypeMsgID
        self.changeTimeLineStatusMsgID = changeTimeLineStatusMsgID

        from results_gui.control_panels import PlotControlPanel
        self.plotControlPanel = PlotControlPanel(self, setLabelsMsgID, cleanMsgID)
        sizer.Add(self.plotControlPanel, 0, wx.LEFT|wx.RIGHT, 7)

        self.addStatsPanel()
        self.addDataSetPanel()

        if LABEL_2_TYPE[self.typeComboBox.GetStringSelection()] == SCATTER_TYPE:
            self.plotControlPanel.addShowTimeLineCheckBox(self.changeTimeLineStatusMsgID)

        self.Layout()
        self.SetMinSize((self.GetBestVirtualSize()[0] ,-1))

    def addStatsPanel(self):
        pass

    def addDataSetPanel(self):
        self.typeComboBox = wx.Choice(self, choices=list(TYPE_2_LABEL.values()))
        self.typeComboBox.Bind(wx.EVT_CHOICE, self.onChangePlotType)
        self.typeComboBox.Enable(True)
        self.typeComboBox.SetSelection(0)
        self.sizer.Add((0,10),0)
        self.sizer.Add(self.typeComboBox, 0, wx.ALL|wx.EXPAND, 5)

        addSelectDeselectButtons(self, self.sizer, None, None)

        self.dataSetCheckBoxList = CheckListBox([], self, style=wx.LB_MULTIPLE|wx.LB_HSCROLL|wx.LB_NEEDED_SB)
        self.Bind(wx.EVT_CHECKLISTBOX, self.onChangeDataDisplayed, self.dataSetCheckBoxList)
        self.sizer.Add(self.dataSetCheckBoxList, 1, wx.ALL|wx.EXPAND, 5)

    def onChangePlotType(self, event=None):
        plotType = LABEL_2_TYPE[self.typeComboBox.GetStringSelection()]
        pub.sendMessage(self.changePlotTypeMsgID, plotType=plotType)

    def changePlotType(self, plotType):
        fieldLabels = TYPE_2_FIELDS[plotType]
        showTimeLineCheckBox = plotType == SCATTER_TYPE
        showTimeLineStatus = self.plotControlPanel.updateConfig(fieldLabels, showTimeLineCheckBox,
                                                                showTimeLineStatus=self.showTimeLineStatus,
                                                                msgID=self.changeTimeLineStatusMsgID)
        if showTimeLineStatus is not None:
            self.showTimeLineStatus = showTimeLineStatus
        self.Layout()

    def addDataSet(self, ID, label):
        self.dataSetCheckBoxList.addItems((ID, label))

    def removeDataSet(self, ID, label):
        self.dataSetCheckBoxList.removeItem(label, ID)

    def clean(self):
        pass

    def onDelete(self):
        self.plotControlPanel.onDelete()

    def onChangeDataDisplayed(self, event):
        dataID = event.EventObject.IDs[event.GetInt()]
        pub.sendMessage(self.onChangeDataMsgID, dataID=dataID)

class TravelTimeContentPanel(MatplotLibCanvasPanel):

    log = logging.getLogger(__name__)
    GAP_FACTORS = {'x':[0.03,0.03], 'y':[0,0.05]}

    def __init__(self, tabParent, *args, **kwargs):
        '''Constructor'''
        MatplotLibCanvasPanel.__init__(self, *args, **kwargs)
        self.tabParent = tabParent

        self.curType = None
        self.legendInfo = {'handles':[], 'labels':[]}
        self.curDataLims = [[np.inf, -np.inf], [0, -np.inf]]

        self.ID2scatter = {}
        self.ID2histData = {}
        self.hist = None
        self.timeLine = None

        self.labels = {}
        self.clickData = None
        self.xBuf = None
        self.yBuf = None
        self.xNorm = None
        self.yNorm = None
        self.curSelectedHandle = None
        self.curIdx = None

        self.canvas.mpl_connect('button_press_event', self.onClick)

        #self.createScatterPlot()
        from results_gui.navigation_tool_bar import GRID_ID
        self.toolBar.executeAction(GRID_ID, self, None, self.axesGrid.getDataAx().ax.get_label())

    def createScatterPlot(self, IDsAndScatterData=None):
        self.curType = SCATTER_TYPE
        self.clean()
        self.axesGrid.getDataAx().xLabel = 'Departure time [s]'
        self.axesGrid.getDataAx().yLabel = 'Travel time [s]'
        if IDsAndScatterData is not None and len(IDsAndScatterData):
            for ID, scatterData in IDsAndScatterData.items():
                self.addDataToScatterPlot(ID, scatterData, False)
            self.endPlotting()

    def createHistogramPlot(self, IDsAndHistData=None):
        self.curType = HIST_TYPE
        self.clean()
        self.axesGrid.getDataAx().xLabel = 'Travel time [s]'
        self.axesGrid.getDataAx().yLabel = 'Count [-]'
        @ticker.FuncFormatter
        def major_formatter(x, pos): # @UnusedVariable
            return '%.4f' % x

        self.axesGrid.getDataAx().ax.xaxis.set_major_formatter(major_formatter)
        if IDsAndHistData is not None and len(IDsAndHistData):
            for ID, histData in IDsAndHistData.items():
                self.ID2histData[ID] = histData
            self.createHistogram()
            self.endPlotting()

    def addDataToPlot(self, ID, data2add):
        if self.curType == SCATTER_TYPE:
            self.addDataToScatterPlot(ID, data2add)
        elif self.curType == HIST_TYPE:
            self.addDataToHistogramPlot(ID, data2add)

    def removeDataFromPlot(self, ID):
        if self.curType == SCATTER_TYPE:
            self.removeDataFromScatterPlot(ID)
        elif self.curType == HIST_TYPE:
            self.removeDataFromHistogramPlot(ID)

    def addDataToScatterPlot(self, ID, scatterData, isLast=True):
        # Get marker
        # Get color
        scatter = self.axesGrid.getDataAx().ax.plot(scatterData['xData'],
                                                        scatterData['yData'],
                                                        label=scatterData['label'],
                                                        linestyle='none',
                                                        markersize=10,
                                                        marker='x',
                                                        zorder=1)
        scatter = scatter[0]
        self.add2Legend(scatter)
        self.setDataLim(scatter)

        self.ID2scatter[ID] = scatter

        self.addScatterDataToEnableSelection(scatterData['xData'], scatterData['yData'], scatterData['label'])

        if isLast:
            self.endPlotting()

    def addScatterDataToEnableSelection(self, xData, yData, label):
        if self.curSelectedHandle is None:
            self.curSelectedHandle = self.axesGrid.getDataAx().ax.plot([np.NaN], [np.NaN], marker='o',
                                                                       markeredgecolor='k', markeredgewidth=2,
                                                                       markersize=11, markerfacecolor='None',
                                                                       zorder=2)[0]

        if self.clickData is None:
            self.clickData = np.column_stack((xData, yData))
            self.xBuf = np.inf
            self.yBuf = np.inf
            startInd = 0
        else:
            startInd = len(self.clickData)
            self.clickData = np.append(self.clickData, np.column_stack((xData, yData)), axis=0)

        for ii in range(startInd, len(self.clickData)):
            self.labels[ii] = label

        uniqueCnt_y = len(np.unique(yData))
        uniqueCnt_x = len(np.unique(xData))
        yLength = np.nanmax(yData) - np.nanmin(yData)
        xLength = np.nanmax(xData) - np.nanmin(xData)

        if yLength == 0:
            yLength = 1
        if xLength == 0:
            xLength = 1

        ySpan = self.curDataLims[1][1] - self.curDataLims[1][0]
        if ySpan/yLength > 10:
            yLength = ySpan*0.1
        xSpan = self.curDataLims[0][1] - self.curDataLims[0][0]
        if xSpan/xLength > 10:
            xLength = xSpan*0.1

        self.xBuf = np.minimum(self.xBuf, xLength/uniqueCnt_x*5)
        self.yBuf = np.minimum(self.yBuf, yLength/uniqueCnt_y*5)

        self.xNorm = xLength
        self.yNorm = yLength

    def removeDataFromScatterPlot(self, ID):
        scatter = self.ID2scatter.pop(ID)
        scatter.remove()
        if len(self.ID2scatter) == 0:
            self.clean()
            return

        self.curDataLims = [[np.inf, -np.inf], [0, -np.inf]]
        self.legendInfo = {'handles':[], 'labels':[]}

        self.labels = {}
        self.clickData = None
        self.xBuf = None
        self.yBuf = None
        self.xNorm = None
        self.yNorm = None
        self.curSelectedHandle = None
        self.curIdx = None

        for scatter in self.ID2scatter.values():
            self.add2Legend(scatter)
            self.setDataLim(scatter)
            self.addScatterDataToEnableSelection(scatter.get_xdata(), scatter.get_ydata(), scatter.get_label())

        self.endPlotting()

    def add2Legend(self, scatter):
        self.legendInfo['handles'].append(scatter)
        self.legendInfo['labels'].append(scatter.get_label())

    def setDataLim(self, scatter):
        self.curDataLims[0][0] = np.minimum(self.curDataLims[0][0], np.nanmin(scatter.get_xdata()))
        self.curDataLims[1][0] = np.minimum(self.curDataLims[1][0], np.nanmin(scatter.get_ydata()))
        self.curDataLims[0][1] = np.maximum(self.curDataLims[0][1], np.nanmax(scatter.get_xdata()))
        self.curDataLims[1][1] = np.maximum(self.curDataLims[1][1], np.nanmax(scatter.get_ydata()))

    def addDataToHistogramPlot(self, ID, histData):
        self.ID2histData[ID] = histData
        if self.hist is not None:
            ID2histData = deepcopy(self.ID2histData)
            self.createHistogramPlot(ID2histData)
            return

        self.createHistogram()
        self.endPlotting()

    def removeDataFromHistogramPlot(self, ID):
        self.ID2histData.pop(ID)
        ID2histData = deepcopy(self.ID2histData)
        self.createHistogramPlot(ID2histData)

    def createHistogram(self):
        histDataList = []
        labels = []
        xMin = np.inf
        xMax = -np.inf

        for histData in self.ID2histData.values():
            histDataList.append(histData['data'].flatten())
            labels.append(histData['label'])
            xMin = np.minimum(xMin, np.min(histData['data']))
            xMax = np.maximum(xMax, np.max(histData['data']))

        kwargs = {'label':labels, 'zorder':1}
        if xMax - xMin < 1e-3:
            kwargs['bins'] = 1

        self.hist = self.axesGrid.getDataAx().ax.hist(histDataList, **kwargs)
        if len(self.ID2histData) == 1:
            self.legendInfo['handles'].append(self.hist[2][0])
            self.legendInfo['labels'].append(self.hist[2][0].get_label())
        else:
            for patches in self.hist[2]:
                self.legendInfo['handles'].append(patches[0])
                self.legendInfo['labels'].append(patches[0].get_label())

        patchList = self.hist[2]
        if isinstance(patchList[0], Rectangle):
            patchList = [patchList]

        for patches in patchList:
            for patch in patches:
                faceColor = patch.get_facecolor()
                patch.set_edgecolor([faceColor[ii]*0.5 for ii in range(3)])
                patch.set_linewidth(2)

        xStep = self.hist[1][1] - self.hist[1][0]

        yMax = -np.inf
        for yCoords in self.hist[0]:
            yMax = np.maximum(yMax, np.max(yCoords))

        self.curDataLims[0][0] = self.hist[1][0] - 2*xStep
        self.curDataLims[1][0] = 0
        self.curDataLims[0][1] = self.hist[1][-1] + 2*xStep
        self.curDataLims[1][1] = yMax

        self.addHistDataToEnableSelection()

    def addHistDataToEnableSelection(self):
        self.labels = []
        self.clickData = []
        self.xBuf = None
        self.yBuf = None
        self.xNorm = None
        self.yNorm = None
        if self.curSelectedHandle is None:
            self.curSelectedHandle = Rectangle((np.NaN, np.NaN), 1, 1, fill=False,
                                               edgecolor='k', linewidth=3, zorder=2)
            self.axesGrid.getDataAx().ax.add_artist(self.curSelectedHandle)

        patchList = self.hist[2]
        binSizes = self.hist[0]
        if isinstance(patchList[0], Rectangle):
            patchList = [patchList]
            binSizes = [binSizes]

        # clickData[ii] = [bin, bin size, xy, width, height]
        for ii in range(len(patchList)):
            patches = patchList[ii]
            label = patches[0].get_label()
            for jj in range(len(patches)):
                binX = self.hist[1][jj]
                binSize = binSizes[ii][jj]
                xy = patches[jj].get_xy()
                width = patches[jj].get_width()
                height = patches[jj].get_height()
                self.clickData.append((binX, binSize, xy, width, height))
                self.labels.append(label)


    def endPlotting(self):
        kwargs = {
            'gapFactors':self.GAP_FACTORS,
            'legendInfo':self.legendInfo
            }

        if self.curType == HIST_TYPE:
            kwargs['gapFactors']['x'] = (0,0)

        self.finalizePlot(True, True, {'x':self.curDataLims[0], 'y':self.curDataLims[1]},
                    **kwargs)

        self.canvas.draw()
        self.canvas._onSize(None)

    def clean(self):
        self.resetToDefault()

        self.legendInfo = {'handles':[], 'labels':[]}
        self.curDataLims = [[np.inf, -np.inf], [0, -np.inf]]

        self.ID2scatter = {}
        self.ID2histData = {}
        self.hist = None
        self.timeLine = None

        self.labels = {}
        self.clickData = None
        self.xBuf = None
        self.yBuf = None
        self.xNorm = None
        self.yNorm = None
        self.curSelectedHandle = None
        self.curIdx = None

        pub.sendMessage(self.tabParent.getCleanMsgID())

        self.finalizePlot()
        self.canvas.draw()


    def onClick(self, event):
        x, y = event.xdata, event.ydata
        if x is None or self.clickData is None:
            return

        if self.curType == SCATTER_TYPE:
            self.setPointToSelect(x,y)
        elif self.curType == HIST_TYPE:
            self.setRectangleToSelect(x, y)

    def setPointToSelect(self, x, y):
        idx = np.nanargmin((np.divide(self.clickData - (x,y), (self.xNorm, self.yNorm))**2).sum(axis = -1))
        xP, yP = self.clickData[idx]
        if np.abs(xP - x) < self.xBuf and np.abs(yP - y) < self.yBuf:
            self.setSelected(idx)
        elif self.curIdx is not None:
            idx = None
            self.setSelected(idx)

    def setRectangleToSelect(self, x, y):
        idx = None
        for ii in range(len(self.clickData)):
            if intersectsRegtangle(x, y, *self.clickData[ii][2:]):
                idx = ii
                break

        self.setSelected(idx)

    def setSelected(self, idx):
        if idx is None:
            if self.curType == SCATTER_TYPE:
                self.curSelectedHandle.set_data((np.NaN, np.NaN))
            elif self.curType == HIST_TYPE:
                self.curSelectedHandle.set_xy((np.NaN, np.NaN))
            pub.sendMessage(self.tabParent.getCleanMsgID())
        else:
            label = self.labels[idx]
            if self.curType == SCATTER_TYPE:
                fieldValues = tuple(self.clickData[idx])
                self.curSelectedHandle.set_data(*fieldValues)
            elif self.curType == HIST_TYPE:
                fieldValues = tuple(self.clickData[idx][0:2])
                self.curSelectedHandle.set_xy(tuple(self.clickData[idx][2]))
                self.curSelectedHandle.set_width(self.clickData[idx][3])
                self.curSelectedHandle.set_height(self.clickData[idx][4])
            fieldValues = {TYPE_2_FIELDS[self.curType][ii]:fieldValues[ii] for ii in range(len(TYPE_2_FIELDS[self.curType]))}
            pub.sendMessage(self.tabParent.getSetLabelsMsgID(), label=label, fieldValues=fieldValues)

        self.curIdx = idx
        self.canvas.draw()

    def selectNext(self):
        if self.curIdx is None or self.curIdx == len(self.clickData) - 1:
            return

        self.setSelected(self.curIdx + 1)

    def selectPrevious(self):
        if self.curIdx is None or self.curIdx == 0:
            return

        self.setSelected(self.curIdx - 1)

    def onKeyPress(self, event):
        MatplotLibCanvasPanel.onKeyPress(self, event)
        if event.key == 'left':
            self.selectPrevious()
        elif event.key == 'right':
            self.selectNext()

def intersectsRegtangle(x,y, xy, width, height):
    return x >= xy[0] and y >= xy[1] and x <= xy[0] + width and y <= xy[1] + height
