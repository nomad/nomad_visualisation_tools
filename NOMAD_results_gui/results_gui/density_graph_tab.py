""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import numpy as np
from NOMAD_results_gui.results_gui.line_graph_tab import LineGraphTab, Line, LineGraphControlPanel, \
    LineGraphContentPanel, ClickableLine


DSC_LINE_TYPE = 'dcs'

class DensityGraphTab(LineGraphTab):
    '''classdocs'''

    def __init__(self, ID, parentFrame, dataMenu, *args, **kwargs):
        '''Constructor'''
        LineGraphTab.__init__(self, ID, parentFrame, dataMenu, *args,
                                canShowTimeLine=True, **kwargs)

    def initControlPanel(self, *args, **kwargs):
        return DensityGraphControlPanel(self.getChangeLinesMsgID(),
                                     self.getSetLabelsMsgID(),
                                     self.getCleanMsgID(), *args, **kwargs)

    def initContentPanel(self, *args, **kwargs):
        return DensityGraphContentPanel(self, self.parentFrame, self.ID, *args, **kwargs)

    def setLines(self, dataID):
        lines = {}
        simResults = self.dataMenu.dataSets[dataID]
        for ID in simResults.densities:
            if ID in simResults.dcs.cell2dcs:
                continue
            lineID = (dataID, ID)
            label = '{}'.format(ID)
            legendLabel = '{}: {}'.format(simResults.shortLabel, ID)
            lines[lineID] = Line(lineID, ID, label, label, legendLabel, None, self.contentPanel.createLine, {})

        for dcsID in simResults.dcs.IDorderPerDcs:
            lineID = (dataID, dcsID)
            label = '{}'.format(dcsID)
            legendLabel = '{}: {}'.format(simResults.shortLabel, dcsID)
            lines[lineID] = Line(lineID, dcsID, label, label, legendLabel, DSC_LINE_TYPE,
                                 self.contentPanel.createDcsLineGroup, {})

        self.lines[dataID] = lines

    def onDelete(self):
        LineGraphTab.onDelete(self)

    @staticmethod
    def getType():
        return 'density_graph'

    @staticmethod
    def getLabel():
        return 'Density graphs'

    @staticmethod
    def getShortLabel():
        return 'Density'

class DensityGraphControlPanel(LineGraphControlPanel):

    def __init__(self, plotMsgID, setLabelsMsgID, *args, **kwargs):
        LineGraphControlPanel.__init__(self, plotMsgID, setLabelsMsgID, *args, **kwargs)

    def addLineSelectPanelContent(self, dataID, lines):
        dcsLines = {key:line for key, line in lines.items() if line.type == DSC_LINE_TYPE}
        self.lineSelectPanels[dataID].addPane("DCSs", False,
                                           self.createLineCheckListBox, dcsLines)

class DensityGraphContentPanel(LineGraphContentPanel):

    def __init__(self, *args, **kwargs):
        '''Constructor'''
        LineGraphContentPanel.__init__(self, *args, **kwargs)

    def createLine(self, simResults, line):
        timeVec = simResults.timeVec

        densOfCell = simResults.densities[line.dataID]
        xData = timeVec[densOfCell.timeInd[0]:densOfCell.timeInd[1]]
        yData = densOfCell.density
        if isinstance(yData, list):
            yData = np.array(yData)

        return ClickableLine(line.ID, line.dataID, xData, yData, line.legendLabel)

    def getXlabel(self):
        return 'Time [s]'

    def getYlabel(self):
        return 'Density [ped/m^2]'
