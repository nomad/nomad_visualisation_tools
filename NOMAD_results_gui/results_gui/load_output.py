""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from _collections import defaultdict
from collections import namedtuple
import itertools
import logging
from math import nan
import pickle
from typing import NamedTuple
from pathlib import Path

from matplotlib import colors
from pubsub import pub

from NOMAD.constants import PED_OUTSIDE_OF_SIM_STATE
from NOMAD.output_manager import BasicFileOutputManager, \
    ConnectivityFileOutputManager, SimulationFileOutputManager
import numpy as np
from NOMAD_results_gui.results_gui.animation_tab import PointDestinationEl


LOAD_PROGRESS = 'loadProgress'
BOUNDARY_SENT = 'sent'
BOUNDARY_RECEIVED = 'received'
BOUNDARY_CREATED = 'created'
BOUNDARY_QUEUE = 'queueLength'

BOUNDARY_FIELD_NMS = (BOUNDARY_SENT, BOUNDARY_RECEIVED, BOUNDARY_CREATED, BOUNDARY_QUEUE)
DensOfCell = namedtuple('DensOfCell', ['density', 'timeInd'])
CountInCell = namedtuple('CountInCell', ['count', 'countStreams', 'timeInd', 'demand', 'transfer', 'cumTransfer'])
BoundaryCountInfo = namedtuple('BoundaryCountInfo', BOUNDARY_FIELD_NMS)
Network = namedtuple('Network', ['drawEls', 'minMax', 'dcsDrawEls', 'cell2dcs'])
DcsData = namedtuple('DcsData', ['cell2dcs', 'IDorderPerDcs', 'maxCellCount'])
DcsParent = namedtuple('DcsParent', ['ID', 'index'])
XiData = namedtuple('XiData', ['timeInd', 'xi'])
Boundary = namedtuple('Boundary', ['x', 'y', 'angle', 'label'])
Dcs = namedtuple('Dcs', ['orig', 'length', 'width', 'angle', 'label'])
DynamicCell = namedtuple('DynamicCell', ['orig', 'length', 'width', 'angle',
                                         'label', 'cellID', 'center', 'A_B_dir'])
TravelTimes = namedtuple('TravelTimes', ['total', 'versusDepartureTime', 'departedCount', 'arrivedCount'])

DynamicElementList = NamedTuple('DynamicElementList', [('toAdd', list), ('toRemove', list), ('drawn', list)])

log = logging.getLogger(__name__)

class SimResults():
    def __init__(self, name, label, timeStep, stepCount, animationStaticElements, animationDynamicElements, animationUpdateElements, animationMinMax, shortLabel=None):

        self.name = name
        self.label = label
        if shortLabel is not None:
            self.shortLabel = shortLabel
        else:
            self.shortLabel = self.label

        self.timeStep = timeStep
        self.stepCount = stepCount
        self.timeVec = np.arange(0, self.timeStep*self.stepCount, self.timeStep)

        self.animationStaticElements = animationStaticElements
        self.animationDynamicElements = animationDynamicElements
        self.animationUpdateElements = animationUpdateElements
        self.animationMinMax = animationMinMax

def loadNomadData(scenDataFilePath):
    scenData = SimulationFileOutputManager.readScenarioDataFile(scenDataFilePath)
    trajDataFilePath = scenDataFilePath.parent.joinpath(scenData.trajFile)
    pub.sendMessage(LOAD_PROGRESS, progress=45, message='Loading trajectory data...')
    trajData, _ = SimulationFileOutputManager.readTrajactoryDataFile(trajDataFilePath)
    descDataFilePath = scenDataFilePath.parent.joinpath(scenData.descFile)
    pub.sendMessage(LOAD_PROGRESS, progress=75, message='Loading description data...')
    descData = SimulationFileOutputManager.readDescriptionDataFile(descDataFilePath)

    connDict = None
    if 'connFile' in scenData._fields:
        if Path(scenData.connFile).suffix != '.npz':
            scenData.connFile = f'{scenData.connFile}.npz'
        
        connFlNm = scenDataFilePath.parent.joinpath(scenData.connFile)
        connDict = ConnectivityFileOutputManager.readConnectionsDataFile(connFlNm)
        ID2IDtuples = defaultdict(list)
        for IDtuple in connDict['connData'].keys():
            ID2IDtuples[IDtuple[0]].append(IDtuple)
            ID2IDtuples[IDtuple[1]].append(IDtuple)
        connDict['ID2IDtuples'] = ID2IDtuples

    pub.sendMessage(LOAD_PROGRESS, progress=85, message='Processing data...')
    animationStaticElements = getNomadStaticElements(scenData)
    animationDynamicElements, animationUpdateElements = getNomadDynamicAndUpdateElements(descData, trajData, scenData.timeStep, scenData.endTimeInd, connDict)

    animationMinMax = {'x':(scenData.geometry.extent[0], scenData.geometry.extent[2]),
                       'y':(scenData.geometry.extent[1], scenData.geometry.extent[3])}

    simResults = SimResults(scenData.name, scenData.label, scenData.timeStep, scenData.endTimeInd,
                            animationStaticElements, animationDynamicElements, animationUpdateElements,
                            animationMinMax)

    return simResults

def getNomadStaticElements(scenData):
    from NOMAD_results_gui.results_gui.animation_tab import WalkableAreaEl, PolygonObstacleEl, LineObstacleEl, \
                                    CircleObstacleEl, EllipseObstacleEl, PolygonDestinationEl, \
                                    LineDestinationEl, PolygonSourceEl, LineSourceEl
    staticElements = []
    for walkLevel in scenData.geometry.walkLevels:
        for ID, walkableArea in walkLevel.walkableAreas._asdict().items():
            for coords in walkableArea:
                staticElements.append(WalkableAreaEl(ID, coords))

        for ID, obstacle in walkLevel.obstacles._asdict().items():
            if obstacle.type == 'polygon':
                staticElements.append(PolygonObstacleEl(ID, obstacle.coords))
            elif obstacle.type == 'line':
                staticElements.append(LineObstacleEl(ID, obstacle.coords))
            elif obstacle.type == 'circle':
                staticElements.append(CircleObstacleEl(ID, obstacle.centerCoord, obstacle.radius))
            elif obstacle.type == 'ellipse':
                staticElements.append(EllipseObstacleEl(ID, obstacle.centerCoord, obstacle.semiAxesValues[0]*2,
                                                        obstacle.semiAxesValues[1]*2, obstacle.rotation))

        for ID, destination in walkLevel.destinations._asdict().items():
            if destination.type == 'polygon':
                staticElements.append(PolygonDestinationEl(ID, destination.coords))
            elif destination.type == 'line':
                staticElements.append(LineDestinationEl(ID, destination.coords))
            elif destination.type == 'point':
                staticElements.append(PointDestinationEl(ID, destination.coords))
            elif destination.type == 'multiPolygon':
                for coords in  destination.coords:
                    staticElements.append(PolygonDestinationEl(ID, coords))
                
        for ID, source in walkLevel.sources._asdict().items():
            if source.type == 'polygon':
                staticElements.append(PolygonSourceEl(ID, source.coords))
            elif source.type == 'line':
                staticElements.append(LineSourceEl(ID, source.coords))

    return staticElements

def getNomadDynamicAndUpdateElements(descData, trajData, timeStep, stepCount, connDict):
    from NOMAD_results_gui.results_gui.animation_tab import PedDynamicEl, PedUpdateEl

    dynamicElements = {timeInd:DynamicElementList([],[],[]) for timeInd in range(stepCount + 1)}
    updateElements = {timeInd:[] for timeInd in range(stepCount + 1)}

    colorCycler = itertools.cycle(['r', 'b', 'y', 'c', 'g'])
    ped2groups = {}
    group2baseColor = {}
    for ID, pedInfo in descData.items():
        if pedInfo.groupID not in ped2groups:
            ped2groups[pedInfo.groupID] = []
            if pedInfo.groupID == 'staff':
                group2baseColor[pedInfo.groupID] = 'm'
            else:
                group2baseColor[pedInfo.groupID] = colorCycler.__next__()
        ped2groups[pedInfo.groupID].append(ID)

    processConn = connDict is not None 
    if processConn:
        pedsConn = getPedConnectedDicts(connDict, timeStep)

    for ID, pedInfo in descData.items():
        if processConn:
            pedConn = pedsConn[ID]
        log.debug(f'Processing ped {ID} ...')
        startTimeInd = getTimeInd(pedInfo.startTime,timeStep)
        if pedInfo.endTime is None:
            endTimeInd = stepCount
        else:
            endTimeInd = getTimeInd(pedInfo.endTime,timeStep) + 1

        pedDynamicEl = PedDynamicEl(ID, pedInfo.radius, getPedColor(pedInfo, ped2groups, group2baseColor))

        for timeInd in range(startTimeInd+1,endTimeInd+1):
            dynamicElements[timeInd].drawn.append(pedDynamicEl)

        dynamicElements[startTimeInd].toAdd.append(pedDynamicEl)
        dynamicElements[endTimeInd].toRemove.append(ID)

        pedTrajData = trajData[ID]
        if pedTrajData.size == 0:
            continue
        prevTimeInd = getTimeInd(pedTrajData['time'][0], timeStep)
        activityInd = 0
        for ii in range(len(pedTrajData['time'])):
            timeInd = getTimeInd(pedTrajData['time'][ii], timeStep)
            if timeInd - prevTimeInd > 1:
                pedIsOutsideOfSim = False
                for jj in range(activityInd, len(pedInfo.activityLog)):
                    activityStartInd = getTimeInd(pedInfo.activityLog[jj].startTime, timeStep)
                    if activityStartInd == prevTimeInd:                        
                        if pedInfo.activityLog[jj].pedState == PED_OUTSIDE_OF_SIM_STATE:
                            pedIsOutsideOfSim = True
                        activityInd = jj
                    if activityStartInd > prevTimeInd:
                        break
                    
                if pedIsOutsideOfSim:
                    centerCoord = (nan, nan)
                else:                    
                    centerCoord = (pedTrajData['pos_x'][ii-1], pedTrajData['pos_y'][ii-1])
                                                            
                for jj in range(prevTimeInd+1,timeInd):
                    if processConn:
                        isConnected = isPedConnected(pedConn, jj)
                    else:
                        isConnected = False                                              
                    
                    updateElements[jj].append(PedUpdateEl(ID, centerCoord, isConnected))
            
            if processConn:
                isConnected = isPedConnected(pedConn, timeInd)        
            else:
                isConnected = False 
            centerCoord = (pedTrajData['pos_x'][ii], pedTrajData['pos_y'][ii])
            updateElements[timeInd].append(PedUpdateEl(ID, centerCoord, isConnected))
            prevTimeInd = timeInd
        lastTimeInd = getTimeInd(pedTrajData['time'][-1], timeStep)
        if lastTimeInd + 1 < endTimeInd:
            centerCoord = (pedTrajData['pos_x'][-1], pedTrajData['pos_y'][-1])
            for jj in range(prevTimeInd+1,endTimeInd):          
                if processConn:
                    isConnected = isPedConnected(pedConn, jj)  
                else:
                    isConnected = False             
                updateElements[jj].append(PedUpdateEl(ID, centerCoord, isConnected))
            

    return dynamicElements, updateElements

def getPedConnectedDicts(connDict, timeStep):
    connData = connDict['connData']
    pedsConn = defaultdict(set)
    for ID, IDtuples in  connDict['ID2IDtuples'].items():
        for IDtuple in IDtuples:
            times = connData[IDtuple][:,0]
            timeIndices = np.round(times/timeStep)
            for timeInd in timeIndices:
                pedsConn[ID].add(int(timeInd))
    
    return pedsConn

def isPedConnected(pedConn, timeInd):
    return timeInd in pedConn

def getPedColor(pedInfo, ped2groups, group2baseColor):
    baseColorRgb = colors.to_rgba(group2baseColor[pedInfo.groupID])[:-1]
    ind = ped2groups[pedInfo.groupID].index(pedInfo.ID)
    f = 1 - ind*0.8/len(ped2groups[pedInfo.groupID])

    return (baseColorRgb[0]*f, baseColorRgb[1]*f, baseColorRgb[2]*f)

def getTimeInd(timeInSeconds, timeStep):
    return int(round(timeInSeconds/timeStep))

class SimResultsOld():

    def __init__(self, filePath):
        if filePath is None:
            return

        with open(filePath, 'rb') as f:
            self.resultsDict = pickle.load(f)

        self.name = self.resultsDict['name']
        self.label = self.resultsDict['label']
        if 'shortLabel' in self.resultsDict:
            self.shortLabel = self.resultsDict['shortLabel']
        else:
            self.shortLabel = self.label

        self.timeStep = self.resultsDict['timeInfo']['timeStep']
        self.stepCount = self.resultsDict['timeInfo']['stepCount']
        self.timeVec = np.arange(0, self.timeStep*self.stepCount, self.timeStep)

        pub.sendMessage(LOAD_PROGRESS, progress=45, message='Processing network data...')
        self.createDrawableNetwork()
        self.createStreamRatios()
        pub.sendMessage(LOAD_PROGRESS, progress=60, message='Processing dynamic cells...')
        self.createCell2TimeInd()
        pub.sendMessage(LOAD_PROGRESS, progress=70, message='Processing density data...')
        self.createDensityData()
        pub.sendMessage(LOAD_PROGRESS, progress=80, message='Processing count data...')
        self.createCountData()
        self.createBoundaryCountData()
        self.createDcsData()
        pub.sendMessage(LOAD_PROGRESS, progress=90, message='Processing travel time data...')
        self.createTravelTimeData()
        pub.sendMessage(LOAD_PROGRESS, progress=95, message='Processing xi data...')
        self.createXiData()

        #self.resultsDict = None

    def createDrawableNetwork(self):
        converter = Connection2coordinatesCoverter(self.resultsDict['networkLayout'])
        drawEls, minMax, dcsDrawEls = converter.convert()
        cell2dcs = {}

        for dcsID, connections in self.resultsDict['networkLayout']['connections']['dcs'].items():
            for _, IDorder in connections.items():
                for ID in IDorder:
                    cell2dcs[ID] = dcsID

        self.network = Network(drawEls, minMax, dcsDrawEls, cell2dcs)

    def createStreamRatios(self):
        self.streamRatios = {}
        if 'streamRatios' not in self.resultsDict:
            return

        for cellID, ratioData in self.resultsDict['streamRatios']['dynamicCells'].items():
            ratioCnt = len(ratioData['ratios'])
            self.streamRatios[cellID] = {ii+ratioData['startTimeInd']:ratioData['ratios'][ii] for ii in range(ratioCnt)}

    def createCell2TimeInd(self):
        # Only for dynamic cells
        cell2TimeInd = {}
        for dcsInfo in self.resultsDict['networkLayout']['dynamicCellSpaces'].values():
            for timeInd in dcsInfo['cellsPerTimeInd']:
                for cellID in cell2TimeInd:
                    if cellID not in dcsInfo['cellsPerTimeInd'][timeInd] and cell2TimeInd[cellID][1] is None:
                        cell2TimeInd[cellID][1] = timeInd

                for cellID in dcsInfo['cellsPerTimeInd'][timeInd]:
                    if cellID not in cell2TimeInd:
                        cell2TimeInd[cellID] = [timeInd, None]

            for cellID in cell2TimeInd:
                if cell2TimeInd[cellID][1] is None:
                    cell2TimeInd[cellID][1] = self.resultsDict['timeInfo']['stepCount']

        self.cell2TimeInd = cell2TimeInd

    def createDensityData(self):
        stepCount = self.resultsDict['timeInfo']['stepCount']
        densPerCell = {}
        for ID in self.resultsDict['densities']:
            if ID not in self.resultsDict['networkLayout']['fixedCells']:
                densPerCell[ID] = DensOfCell(self.resultsDict['densities'][ID], self.cell2TimeInd[ID])
            else:
                densPerCell[ID] = DensOfCell(self.resultsDict['densities'][ID], [0, stepCount])

        self.densities = densPerCell

        self.maxJamDensity = 0
        for _, fd in self.resultsDict['fds'].items():
            if fd.jamDensity > self.maxJamDensity:
                self.maxJamDensity = fd.jamDensity

    def createCountData(self):
        self.counts = {}
        self.maxCount = 0

        stepCount = self.resultsDict['timeInfo']['stepCount']


        for ID, cellData in self.resultsDict['cells'].items():
            if ID not in self.resultsDict['networkLayout']['fixedCells']:
                pedCounts = np.array([cellData['pedCount'][timeInd] for timeInd in cellData['pedCount']])
                self.maxCount = np.maximum(self.maxCount, np.max(pedCounts))
                pedCountsStreams = getCountsInStreams(cellData, self.cell2TimeInd[ID][1] - self.cell2TimeInd[ID][0] + 1)
                demand = getDemandCountData(cellData)
                transfer = getTransferCountData(cellData)
                cumTransfer = getCumTransferCountData(cellData, transferData=transfer)
                self.counts[ID] = CountInCell(pedCounts, pedCountsStreams, self.cell2TimeInd[ID],
                                                demand, transfer, cumTransfer)
            else:
                pedCountsStreams = getCountsInStreams(cellData, stepCount)
                self.counts[ID] = CountInCell(cellData['pedCount'], pedCountsStreams, [0, stepCount],
                                                None, None, None)
                self.maxCount = np.maximum(self.maxCount, np.max(cellData['pedCount']))

    def createBoundaryCountData(self):
        self.boundaryCounts = {}

        for ID, boundary in self.resultsDict['boundaries'].items():
            fldValues = {}
            for field in BOUNDARY_FIELD_NMS:
                fldValues[field] = boundary[field]

            self.boundaryCounts[ID] = BoundaryCountInfo(**fldValues)

    def createDcsData(self):
        cell2dcs = {}
        IDorderPerDcs = {}
        maxCellCount = {}

        for dcsID, dcsData in self.resultsDict['networkLayout']['connections']['dcs'].items():
            IDorderPerDcs[dcsID] = list(dcsData.values())
            maxCellCount[dcsID] = 0
            for IDorder in dcsData.values():
                cellCount = len(IDorder)
                maxCellCount[dcsID] = np.maximum(maxCellCount[dcsID], cellCount)
                for ii in range(cellCount):
                    ID = IDorder[ii]
                    if ID not in cell2dcs:
                        cell2dcs[ID] = DcsParent(dcsID, ii)

        self.dcs = DcsData(cell2dcs, IDorderPerDcs, maxCellCount)

    def createXiData(self):
        def addXiData(cellID, portID, conn):
            ID = (cellID, portID)
            if not hasattr(conn, 'xi'):
                return
            xiValues = conn['xi']
            xiKeys = list(xiValues.keys())
            startInd = conn['startTimeInd']
            endInd = startInd + len(xiValues[xiKeys[0]])
            timeInd = np.arange(startInd, endInd, 1, dtype=int)
            if ID in self.xiData:
                timeInd = np.append(self.xiData[ID].timeInd, timeInd)
                xiValuesNew = self.xiData[ID].xi.copy()
                for key in xiKeys:
                    xiValuesNew[key] += xiValues[key]

                self.xiData[ID] = XiData(timeInd, xiValuesNew)
            else:
                self.xiData[ID] = XiData(timeInd, xiValues)


        self.xiData = {}
        if 'connections' not in self.resultsDict:
            return

        for conn in self.resultsDict['connections'].values():
            addXiData(conn['cellID_1'], conn['portID_1'], conn)
            addXiData(conn['cellID_2'], conn['portID_2'], conn)

    def createTravelTimeData(self):
        pedCount = len(self.resultsDict['pedestrians'])

        totalTravelTimes = np.zeros((pedCount,1), dtype=float)
        totalTravelTimes[:] = np.NaN
        departureTimeVsTT = np.zeros((pedCount, 2), dtype=float)
        departureTimeVsTT[:,:] = np.NaN
        departedCount = pedCount
        arrivedCount = 0

        boundaryIDs = self.resultsDict['networkLayout']['boundaries']

        rowInd = 0
        for ped in self.resultsDict['pedestrians']:
            lastCellID = ped.getCellTravelsalField(-1, 'cellID')
            firstCellID = ped.getCellTravelsalField(0, 'cellID')
            if lastCellID == firstCellID:
                continue
            if lastCellID not in boundaryIDs:
                continue

            departureTime = ped.getCellTravelsalField(0, 'departureTime')
            arrivalTime = ped.getCellTravelsalField(-1, 'arrivalTime')
            travelTime = arrivalTime - departureTime
            totalTravelTimes[rowInd] = travelTime
            departureTimeVsTT[rowInd,0] = departureTime
            departureTimeVsTT[rowInd,1] = travelTime
            arrivedCount += 1
            rowInd += 1

        totalTravelTimes = totalTravelTimes[~np.isnan(totalTravelTimes)]
        departureTimeVsTT = departureTimeVsTT[~np.isnan(departureTimeVsTT[:,0]),:]

        self.travelTimes = TravelTimes(totalTravelTimes, departureTimeVsTT, departedCount, arrivedCount)

# ==================================================================================================
# ==================================================================================================

def getCountsInStreams(cellData, stepCount):
    counts = np.zeros(stepCount)
    for key, stream in cellData.items():
        if key in ('pedCount', 'demand', 'transfer'):
            continue
        pedCounts = stream['pedCount']
        if isinstance(pedCounts, dict):
            ind = 0
            for timeInd in pedCounts:
                counts[ind] = counts[ind] + pedCounts[timeInd]
                ind += 1
        else:
            # Assume it is a numpy array of length ==  stepCount
            counts = counts + pedCounts

    return counts

def getConnectionData(cellData, dataType):
    countData = {}
    for inOut, inOutData in cellData[dataType].items():
        countData[inOut] = {}
        for portLetter, portData in inOutData.items():
            countData[inOut][portLetter] = np.array(portData)

    return countData

def getDemandCountData(cellData):
    return getConnectionData(cellData, 'demand')

def getTransferCountData(cellData):
    return getConnectionData(cellData, 'transfer')

def getCumTransferCountData(cellData, transferData=None):
    if transferData is None:
        transferData = getConnectionData(cellData, 'transfer')

    countData = {}
    for inOut, inOutData in transferData.items():
        countData[inOut] = {}
        for portLetter, portData in inOutData.items():
            countData[inOut][portLetter] = np.append(np.zeros(0), np.cumsum(portData))
    return countData

# ==================================================================================================
# ==================================================================================================

FIXED_TYPE = 'fixed'
BOUNDARY_TYPE = 'boundary'
DCS_TYPE = 'dcs'
DYNAMIC_TYPE = 'dynamic'

class Connection2coordinatesCoverter():

    # Rotations are clockwise, an angle of 0 degrees means a cell oriented left to right
    # (length) whereby the width dimension is from top to bottom.
    #

    def __init__(self, networkData):
        self.drawEls = {}
        self.dynamicCellDrawEls = {}

        self.startCellInfo = networkData['connections']['startCell']
        self.connections = networkData['connections']['main']
        self.dcsConnections = networkData['connections']['dcs']
        self.boundaries = networkData['boundaries']
        self.fixedCells = networkData['fixedCells']
        self.dynamicCellSpaces = networkData['dynamicCellSpaces']
        self.dcsDrawEls = {}

        self.xMin = np.inf
        self.yMin = np.inf
        self.xMax = -np.inf
        self.yMax = -np.inf

        self.loopCount = 0

    def convert(self):
        baseCoords = [0,0]
        startCellID = self.startCellInfo['ID']
        angle  = self.startCellInfo['angle']

        #t0 = time.time()
        self.walkConnectionDict(startCellID, None, baseCoords, angle)
        #t1 = time.time()
        #print('walkConnectionDict: {}'.format(t1-t0))
        #t0 = time.time()
        self.createDynamicCells()
        #t1 = time.time()
        #print('createDynamicCells: {}'.format(t1-t0))

        minMax = {'x':(self.xMin, self.xMax), 'y':(self.yMin, self.yMax)}

        return self.drawEls, minMax, self.dynamicCellDrawEls

    def walkConnectionDict(self, cellID, prevCellID, baseCoords, angle):
        self.loopCount += 1
        cellConnections = self.connections[cellID]
        kwargs = {}
        if cellID in self.boundaries:
            cellType = BOUNDARY_TYPE
            cellInfo = None
            if prevCellID is not None:
                otherCellID = prevCellID
            else:
                otherCellID = cellConnections[0]

            if otherCellID in self.dynamicCellSpaces:
                width = self.dynamicCellSpaces[otherCellID]['effWidth']
            else:
                raise Exception('Implement method for other cell type')

            kwargs['width'] = width
        elif cellID in self.fixedCells:
            cellType = FIXED_TYPE
            cellInfo = self.fixedCells[cellID]
        elif cellID in self.dynamicCellSpaces:
            cellType = DCS_TYPE
            cellInfo = self.dynamicCellSpaces[cellID]

        drawEl, baseCoords, angle, minMax = addEl(baseCoords, angle, cellInfo, cellID, cellType, **kwargs)
        try:
            self.drawEls[cellType].append(drawEl)
        except KeyError:
            self.drawEls[cellType] = [drawEl]

        if cellType == DCS_TYPE:
            self.dcsDrawEls[cellID] = drawEl

        self.xMin = np.minimum(self.xMin, minMax[0][0])
        self.yMin = np.minimum(self.yMin, minMax[1][0])
        self.xMax = np.maximum(self.xMax, minMax[0][1])
        self.yMax = np.maximum(self.yMax, minMax[1][1])

        if isinstance(cellConnections, dict):
            for newCellConnections in cellConnections.values():
                if newCellConnections[0] == prevCellID:
                    continue
                baseCoords, angle = self.walkConnectionDict(newCellConnections[0], cellID, baseCoords, angle)
        else:
            if cellConnections[0] != prevCellID:
                baseCoords, angle = self.walkConnectionDict(cellConnections[0], cellID, baseCoords, angle)

        return baseCoords, angle

    def createDynamicCells(self):
        for dcsID, connections in self.dcsConnections.items():
            self.dynamicCellDrawEls[dcsID] = {}
            for timeInd, IDorder in connections.items():
                drawEls = []
                cells = self.dynamicCellSpaces[dcsID]['cellsPerTimeInd'][timeInd]
                baseCoords = self.dcsDrawEls[dcsID].orig
                angle = self.dcsDrawEls[dcsID].angle
                for ID in IDorder:
                    cellInfo = cells[ID]
                    cellInfo['effWidth'] = self.dynamicCellSpaces[dcsID]['effWidth']
                    drawEl, baseCoords = addEl(baseCoords, angle, cellInfo,
                                                              ID, DYNAMIC_TYPE)
                    drawEls.append(drawEl)

                self.dynamicCellDrawEls[dcsID][timeInd] = drawEls

# ==================================================================================================
# ==================================================================================================

def addEl(baseCoords, angle, cellInfo, cellID, cellType, **kwargs):
    drawEl = None
    newBaseCoords = baseCoords
    newAngle = angle

    # baseCoords are always the center of the edge

    if cellType == BOUNDARY_TYPE:
        drawEl, minMax = createBoundary(baseCoords, angle, cellID, **kwargs)
    elif cellType == DCS_TYPE:
        drawEl, newBaseCoords, minMax = createDynamicCellSpace(baseCoords, angle, cellInfo, cellID)
    elif cellType == DYNAMIC_TYPE:
        return createDynamicCell(baseCoords, -angle, cellInfo, cellID)


    return drawEl, newBaseCoords, newAngle, minMax

def createBoundary(baseCoords, angle, cellID, width=None):
    x0 = baseCoords[0] - np.sin(np.deg2rad(angle))*width/2
    x1 = baseCoords[0] + np.sin(np.deg2rad(angle))*width/2
    y0 = baseCoords[1] - np.cos(np.deg2rad(angle))*width/2
    y1 = baseCoords[1] + np.cos(np.deg2rad(angle))*width/2
    boundary = Boundary((x0,x1), (y0,y1), -angle, '{}'.format(cellID))
    minMax = ((np.minimum(x0,x1),np.maximum(x0,x1)), (np.minimum(y0,y1),np.maximum(y0,y1)))
    return boundary, minMax

def createDynamicCellSpace(baseCoords, angle, cellInfo, cellID):
    width = cellInfo['effWidth']
    length = cellInfo['length']

    newBaseCoords = (baseCoords[0] + np.sin(np.deg2rad(angle + 90))*length,
                     baseCoords[1] + np.cos(np.deg2rad(angle + 90))*length)

    x0 = baseCoords[0] - np.sin(np.deg2rad(angle))*width/2
    x1 = newBaseCoords[0] + np.sin(np.deg2rad(angle))*width/2
    y0 = baseCoords[1] - np.cos(np.deg2rad(angle))*width/2
    y1 = newBaseCoords[1] + np.cos(np.deg2rad(angle))*width/2

    dcs = Dcs((x0,y0), length, width, -angle, '{}'.format(cellID))
    minMax = ((np.minimum(x0,x1),np.maximum(x0,x1)), (np.minimum(y0,y1),np.maximum(y0,y1)))
    return dcs, newBaseCoords, minMax

def createDynamicCell(baseCoords, angle, cellInfo, cellID):
    width = cellInfo['effWidth']
    length = cellInfo['length']
    x0 = baseCoords[0]
    y0 = baseCoords[1]

    newBaseCoords = (baseCoords[0] + np.sin(np.deg2rad(angle + 90))*length,
                     baseCoords[1] + np.cos(np.deg2rad(angle + 90))*length)

    centerCoords = (baseCoords[0] + np.sin(np.deg2rad(angle + 90))*0.5*length,
                     baseCoords[1])

    dynamicCell = DynamicCell((x0,y0), length, width, -angle, '{}'.format(cellID),
                              cellID, centerCoords, 1)

    return dynamicCell, newBaseCoords


