""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import datetime
import logging

from pubsub import pub
import wx
from wx.lib.agw import aui

from NOMAD_results_gui.results_gui.data_menu import DataMenu
from NOMAD_results_gui.results_gui.matplotlib_canvas_panel import ON_ENTER_CANVAS
from NOMAD_results_gui.results_gui.navigation_tool_bar import NavigationToolbar
from NOMAD_results_gui.results_gui.tab_controller import TabController


wxEVT_AFTER_RESIZE = wx.NewEventType()
EVT_AFTER_RESIZE = wx.PyEventBinder(wxEVT_AFTER_RESIZE, 1)

class ResultsGui(wx.Frame):
    '''
    classdocs
    '''
    log = logging.getLogger(__name__)

    def __init__(self, appPath):
        '''
        Constructor
        '''
        wx.Frame.__init__(self, None, size=(1000, 600), style=wx.FULL_REPAINT_ON_RESIZE | wx.DEFAULT_FRAME_STYLE)
        self.Bind(wx.EVT_CLOSE, self.onCloseWindow)
        self.Bind(wx.EVT_SIZE, self.onResize)
        self.simResults = None
        self.appPath = appPath

        self._mgr = aui.AuiManager()
        self._mgr.SetManagedWindow(self)

        self.createMenus()
        self.statusBar = self.CreateStatusBar()

        self.createNotebook()

        self.tabController = TabController(self.notebook, self)

        self.progressDialog = None

        self.Centre()
        self.SetTitle('NOMAD results')

        pub.subscribe(self.onChangeWindow, ON_ENTER_CANVAS)

        self.Show(True)

    def createMenus(self):
        self.dataMenu = DataMenu()
        menuBar = wx.MenuBar()
        self.createFileMenu(menuBar)
        self.dataMenu.addMenu(menuBar, self)
        self.createRecordMenu(menuBar)
        self.SetMenuBar(menuBar)

    def createFileMenu(self, menuBar):
        fileMenu = wx.Menu()

        menuExit = fileMenu.Append(wx.ID_EXIT, 'E&xit\tCtrl+Q', ' Terminate the program')
        menuBar.Append(fileMenu, '&File')
        self.Bind(wx.EVT_MENU, self.onClose, menuExit)
        
    def createRecordMenu(self, menuBar):
        recordMenu = wx.Menu()

        self.menuMovie = recordMenu.AppendRadioItem(wx.ID_ANY, 'Movie', ' Movie record mode')
        self.menuPictures = recordMenu.AppendRadioItem(wx.ID_ANY, 'Picture', 'Picture record mode')
        menuBar.Append(recordMenu, '&Record')
    def createNotebook(self):
        self.notebook = Notebook(self, agwStyle=aui.AUI_NB_TOP | aui.AUI_NB_TAB_SPLIT | aui.AUI_NB_TAB_MOVE | aui.AUI_NB_SCROLL_BUTTONS | aui.AUI_NB_CLOSE_ON_ALL_TABS | aui.AUI_NB_WINDOWLIST_BUTTON)
        self._mgr.AddPane(self.notebook, aui.AuiPaneInfo().Name("notebook_content").
                          CenterPane().PaneBorder(False))
        self._mgr.Update()

    def updateProgressDialog(self, progress, message):
        newPerc = progress/100.*80.
        self.progressDialog.Update(newPerc, message)

    def onResize(self, event):
        pass

    def onClose(self, event):
        self.Close(True)

    def onCloseWindow(self, event):
        self.log.info('Closing the gui ...')
        self.tabController.clean()
        self.Destroy()

    def onChangeWindow(self, window):
        self.notebook.SetSelectionToWindow(window)

    def getGapsPerTabType(self):
        return self.tabController.getGapsPerTabType()

    def getTitlesPerTab(self):
        return self.tabController.getTitlesPerTab()

    def getRecordMode(self):
        if self.menuMovie.IsChecked():
            return MOVIE_RECORD_MODE
        if self.menuPictures.IsChecked():
            return PICTURE_RECORD_MODE
                    

class Notebook(aui.AuiNotebook):

    def __init__(self, *args, **kwargs):
        aui.AuiNotebook.__init__(self, style=wx.WANTS_CHARS, *args, **kwargs)

    def SetSelection(self, new_page, force=False):
        """
        Sets the page selection. Calling this method will generate a page change event.

        :param integer `new_page`: the index of the new selection;
        :param bool `force`: whether to force the selection or not.
        """
        wnd = self._tabs.GetWindowFromIdx(new_page)

        #Update page access time
        self._tabs.GetPages()[new_page].access_time = datetime.datetime.now()

        if not wnd or not self.GetEnabled(new_page):
            return self._curpage

        # don't change the page unless necessary
        # however, clicking again on a tab should give it the focus.
        if new_page == self._curpage and not force:

            ctrl, ctrl_idx = self.FindTab(wnd)
            if wx.Window.FindFocus() != ctrl:
                ctrl.SetFocus()

            return self._curpage

        evt = aui.AuiNotebookEvent(aui.wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGING, self.GetId())
        evt.SetSelection(new_page)
        evt.SetOldSelection(self._curpage)
        evt.SetEventObject(self)

        if not self.GetEventHandler().ProcessEvent(evt) or evt.IsAllowed():

            old_curpage = self._curpage
            self._curpage = new_page

            # program allows the page change
            evt.SetEventType(aui.wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGED)
            self.GetEventHandler().ProcessEvent(evt)

            if not evt.IsAllowed(): # event is no longer allowed after handler
                return self._curpage

            ctrl, ctrl_idx = self.FindTab(wnd)

            if ctrl:
                #self._tabs.SetActivePage(wnd)
                ctrl.SetActivePage(ctrl_idx)
                self.DoSizing()
                ctrl.DoShowHide()
                ctrl.MakeTabVisible(ctrl_idx, ctrl)

                # set fonts
                all_panes = self._mgr.GetAllPanes()
                for pane in all_panes:
                    if pane.name == "dummy":
                        continue

                    tabctrl = pane.window._tabs
                    if tabctrl != ctrl:
                        tabctrl.SetSelectedFont(self._normal_font)
                    else:
                        tabctrl.SetSelectedFont(self._selected_font)

                    tabctrl.Refresh()
                    tabctrl.Update()

                # Set the focus to the page if we're not currently focused on the tab.
                # This is Firefox-like behaviour.
                if wnd.IsShownOnScreen() and wx.Window.FindFocus() != ctrl:
                    wnd.SetFocus()

                return old_curpage

        return self._curpage
