""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from pubsub import pub

from NOMAD_results_gui.xml_config_base.xml2config import getElementsValueWithAttr
from NOMAD_results_gui.results_gui.xml_config import XmlConfig
import xml.etree.ElementTree as ET


TITLES_PER_TAB = 'titlesPerTab'
TITLE_EL = 'title'
TAB_ID = 'tabID'
DATA_ID = 'dataID'

UPDATE_TITLES_MSG_ID = 'updateTitles'

class TitleConfig(XmlConfig):

    @staticmethod
    def getConfigFromDict(config):
        return config['titlesPerTab']

    def getConfigDict(self):
        return self.resultsGui.getTitlesPerTab()

    @staticmethod
    def readConfigFromXml(config, collection):
        titlesPerTab = {}
        for tabXml in collection.getElementsByTagName(TITLES_PER_TAB):
            tabID = tabXml.getAttribute(TAB_ID)

            titlesRaw = getElementsValueWithAttr(tabXml, TITLE_EL, DATA_ID, 'str')
            titles = {}
            for ID, val in titlesRaw.items():
                titles[int(ID)] = val

            titlesPerTab[tabID] = titles

        config['titlesPerTab'] = titlesPerTab

    @staticmethod
    def addConfigDict2xml(rootEL, titlesPerTab):
        for tabID, titles in titlesPerTab.items():
            tabEL = ET.SubElement(rootEL, TITLES_PER_TAB, attrib={TAB_ID:tabID})
            for dataID, title in titles.items():
                titleEl = ET.SubElement(tabEL, TITLE_EL, attrib={DATA_ID:str(dataID)})
                titleEl.text = title

    @staticmethod
    def getFile():
        return 'titleConfig.xml'

    @staticmethod
    def sendPubMsg(config):
        pub.sendMessage(UPDATE_TITLES_MSG_ID, titlesPerTab=config['titlesPerTab'])