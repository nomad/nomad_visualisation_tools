""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from pathlib import Path


APP_NAME = 'appName'
APP_PATH = 'appPath'
RECENT_FILES_CONFIG_FLNM = 'recentConfigFlNm'
RECENT_FILES_CONFIG_FILE = 'recentConfigFile'
IMAGE_DIR_PATH = 'imageDirPath'
MOVIE_RECORD_MODE = 'movieRecordMode'
PICTURE_RECORD_MODE = 'pictureRecordMode'

_constants = {
    APP_NAME: None,
    APP_PATH: None,
    RECENT_FILES_CONFIG_FILE: None,
    RECENT_FILES_CONFIG_FLNM: None,
    IMAGE_DIR_PATH: None
    }

def getConstant(key, default=None):
    return _constants.get(key, default)

def initConstants(appPath, appName):
    _constants[APP_PATH] = appPath
    _constants[APP_NAME] = appName
    _constants[RECENT_FILES_CONFIG_FILE] = '.recentfiles'
    _constants[RECENT_FILES_CONFIG_FLNM] = appPath.joinpath(_constants[RECENT_FILES_CONFIG_FILE])
    _constants[IMAGE_DIR_PATH] = Path(__file__).parent.joinpath('images')