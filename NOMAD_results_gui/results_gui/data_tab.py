""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging

from pubsub import pub
import wx

from NOMAD_results_gui.results_gui.cust_wx_elements import Panel, ContextMenu
from NOMAD_results_gui.results_gui.matplotlib_canvas_panel import MatplotLibCanvasPanel
from NOMAD_results_gui.results_gui.navigation_tool_bar import TOOL_ORDER, TOOL_ITEMS


class DataTab(Panel):
    '''classdocs'''

    log = logging.getLogger(__name__)

    minPanelSize = 100
    VERTICAL_SPLIT = 'vertical'
    HORIZONTAL_SPLIT = 'horizontal'
    CONTROL_CONTENT = 'controlContent'
    CONTENT_CONTROL = 'contentControl'

    SPLIT_2_FCN = {VERTICAL_SPLIT:1}

    def __init__(self, ID, parentFrame, dataMenu, splitDirection=VERTICAL_SPLIT, splitOrder=CONTROL_CONTENT, defSize=3, canShowTimeLine=False, *args, **kwargs):
        '''Constructor'''
        Panel.__init__(self, *args, **kwargs)

        self.ID = ID
        self.parentFrame = parentFrame
        self.dataMenu = dataMenu

        self.splitDirection = splitDirection
        self.splitOrder = splitOrder

        self.dataSetIds = []

        self.splitter = wx.SplitterWindow(self)
        self.controlPanel = self.initControlPanel(self.splitter, style=wx.BORDER_DEFAULT)
        self.contentPanel = self.initContentPanel(self.splitter, style=wx.BORDER_DEFAULT)

        self.splitWindow(defSize=defSize)
        self.splitter.SetMinimumPaneSize(self.minPanelSize)

        if isinstance(self.controlPanel, EmptyControlPanel):
            self.splitter.Unsplit(self.controlPanel)
        else:
            self.Bind(wx.EVT_CONTEXT_MENU, self.onContextMenu)

        boxSizer = wx.BoxSizer(wx.VERTICAL)
        boxSizer.Add(self.splitter, 1, wx.EXPAND)
        self.SetAutoLayout(True)
        self.SetSizer(boxSizer)

        self.lastTimeValue = 0
        self.timeLineShown = False
        self.canShowTimeLine = canShowTimeLine

        if canShowTimeLine:
            pub.subscribe(self.onChangeTimeLineStatus, self.getChangeTimeLineStatusMsgID())
            self.addTimeLine()
            from NOMAD_results_gui.results_gui.animation_tab import TIME_UPDATED_MSG_ID
            pub.subscribe(self.onChangeTime, TIME_UPDATED_MSG_ID)

    def contextMenu(self, mousePos):
        contextMenu = ContextMenu(mousePos)
        self.toolIDs = {}
        self.toolID2item = {}

        ids2add = [ID for ID in self.dataMenu.ids if ID not in self.dataSetIds]
        addSubMenu = wx.Menu()
        addMenu = contextMenu.AppendSubMenu(addSubMenu, 'Add data set', 'Add a data set to the tab')
        self.addMenuItem2id = {}
        if len(ids2add) > 0:
            ii = 1
            for ID in ids2add:
                item = addSubMenu.Append(wx.ID_ANY, '{}. {}'.format(ii, self.dataMenu.id2label[ID]), 'Add {}'.format(self.dataMenu.id2label[ID]))
                self.parentFrame.Bind(wx.EVT_MENU, self.onAddDataSet, item)
                self.addMenuItem2id[item.Id] = ID
                ii += 1
        else:
            addMenu.Enable(False)

        removeSubMenu = wx.Menu()
        removeMenu = contextMenu.AppendSubMenu(removeSubMenu, 'Remove data set', 'Remove a data set from the tab')
        self.removeMenuItem2id = {}
        if len(self.dataSetIds) > 0:
            ii = 1
            for ID in self.dataSetIds:
                item = removeSubMenu.Append(wx.ID_ANY, '{}. {}'.format(ii, self.dataMenu.id2label[ID]), 'Add {}'.format(self.dataMenu.id2label[ID]))
                self.parentFrame.Bind(wx.EVT_MENU, self.onRemoveDataSet, item)
                self.removeMenuItem2id[item.Id] = ID
                ii += 1
        else:
            removeMenu.Enable(False)

        contextMenu.AppendSeparator()

        if self.splitter.IsSplit():
            item = contextMenu.Append(wx.ID_ANY, 'Hide control panel', 'Hide control panel')
            self.parentFrame.Bind(wx.EVT_MENU, self.onHideControlPanel, item)
        elif not isinstance(self.controlPanel, EmptyControlPanel):
            item = contextMenu.Append(wx.ID_ANY, 'Show control panel', 'Show control panel')
            self.parentFrame.Bind(wx.EVT_MENU, self.onShowControlPanel, item)

        self.addTabContextMenuItems(contextMenu)

        if isinstance(self.contentPanel, MatplotLibCanvasPanel):
            contextMenu.AppendSeparator()
            # Add zoom, pan, home, back, forward
            for toolID in TOOL_ORDER:
                if toolID is None:
                    contextMenu.AppendSeparator()
                    continue
                toolNumID = wx.NewId()
                self.toolIDs[toolNumID] = toolID
                if TOOL_ITEMS[toolID]['kind'] == wx.ITEM_CHECK:
                    item = contextMenu.AppendCheckItem(toolNumID, TOOL_ITEMS[toolID]['label'],
                                          TOOL_ITEMS[toolID]['toolTip'])
                else:
                    item = contextMenu.Append(toolNumID, TOOL_ITEMS[toolID]['label'],
                                          TOOL_ITEMS[toolID]['toolTip'])

                self.toolID2item[toolID] = item
                self.parentFrame.Bind(wx.EVT_MENU, self.onToolBarAction, item)
                if TOOL_ITEMS[toolID]['kind'] == wx.ITEM_CHECK:
                    contextMenu.Check(toolNumID, self.contentPanel.toolBar.isActive(toolID))

        self.parentFrame.PopupMenu(contextMenu)

        contextMenu.Destroy()

    def onContextMenu(self, event):
        self.contextMenu(event.Position)

    def onHideControlPanel(self, event):
        self.controlPanelSize = self.controlPanel.GetSize()
        self.splitter.Unsplit(self.controlPanel)

    def onShowControlPanel(self, event):
        sashPosition = 0
        if self.splitDirection == self.VERTICAL_SPLIT:
            if self.GetSize()[0]*0.8 > self.controlPanelSize[0]:
                if self.splitOrder == self.CONTROL_CONTENT:
                    sashPosition = self.controlPanelSize[0]
                else:
                    sashPosition = self.GetSize()[0] - self.controlPanelSize[0]
        elif self.splitDirection == self.HORIZONTAL_SPLIT:
            if self.GetSize()[1]*0.8 > self.controlPanelSize[1]:
                if self.splitOrder == self.CONTROL_CONTENT:
                    sashPosition = self.controlPanelSize[1]
                else:
                    sashPosition = self.GetSize()[1] - self.controlPanelSize[1]

        self.splitWindow(sashPosition=sashPosition)

    def splitWindow(self, defSize=3, sashPosition=None):
        if self.splitOrder == self.CONTROL_CONTENT:
            panels = (self.controlPanel, self.contentPanel)
        elif self.splitOrder == self.CONTENT_CONTROL:
            panels = (self.contentPanel, self.controlPanel)
            defSize = -defSize
        else:
            raise Exception('Unknown splitOrder "{}"'.format(self.splitOrder))

        if sashPosition is None:
            sashPosition = defSize

        if self.splitDirection == self.VERTICAL_SPLIT:
            self.splitter.SplitVertically(*panels, sashPosition=sashPosition)
        elif self.splitDirection == self.HORIZONTAL_SPLIT:
            self.splitter.SplitHorizontally(*panels, sashPosition=sashPosition)
        else:
            raise Exception('Unknown splitDirection "{}"'.format(self.splitDirection))

    def addTabContextMenuItems(self, contextMenu):
        return

    def initControlPanel(self, *args, **kwargs):
        raise Exception('Method should be implemented!')

    def initContentPanel(self, *args, **kwargs):
        raise Exception('Method should be implemented!')

    def getDividerGaps(self):
        if hasattr(self.contentPanel, 'axesGrid'):
            return self.contentPanel.axesGrid.getGaps()
        else:
            return {}

    def updateDividerGaps(self, dividerGaps):
        if hasattr(self.contentPanel, 'axesGrid'):
            self.contentPanel.axesGrid.adaptGapSizes(dividerGaps)
            self.contentPanel.canvas.draw()

    def setSimResults(self, simResults):
        self.simResults = simResults

    def onAddDataSet(self, event=None, ID=None):
        if event is not None:
            ID = self.addMenuItem2id[event.Id]

        assert(ID is not None), 'ID is none!'

        self.addDataSets([ID])

    def addDataSets(self, IDs):
        self.preAddDataSets()

        for ID in IDs:
            if ID in self.dataSetIds:
                self.log.warning('Tried to add an already added data set!')
                continue

            self.dataSetIds.append(ID)
            self.addDataSet(ID)

        self.postAddDataSets()
        self.updateControlPanelSize()

    def onRemoveDataSet(self, event=None, ID=None):
        if event is not None:
            ID = self.removeMenuItem2id[event.Id]

        assert(ID is not None), 'ID is none!'

        self.removeDataSets([ID])

    def removeDataSets(self, IDs):
        self.preRemoveDataSets()
        for ID in IDs:
            if ID not in self.dataSetIds:
                self.log.warning('Tried to remove an already removed data set!')
                continue

            self.dataSetIds.remove(ID)
            self.removeDataSet(ID)

        self.postRemoveDataSets()
        self.updateControlPanelSize()

    def updateControlPanelSize(self):
        self.Layout()
        minWidth = self.minPanelSize
        for child in self.controlPanel.GetChildren():
            childWidth = child.GetSize()[0]
            if childWidth > minWidth:
                minWidth = childWidth

        if self.controlPanel.GetSize()[0] == self.minPanelSize:
            self.splitter.SetSashPosition(minWidth)
        self.Layout()

    def reloadDataSets(self, IDs):
        for ID in IDs:
            self.reloadDataSet(ID)

        self.postReloadDataSets()

    def preAddDataSets(self):
        raise Exception('Method should be implemented!')

    def preRemoveDataSets(self):
        raise Exception('Method should be implemented!')

    def preReloadDataSets(self):
        raise Exception('Method should be implemented!')

    def postAddDataSets(self):
        raise Exception('Method should be implemented!')

    def postRemoveDataSets(self):
        raise Exception('Method should be implemented!')

    def postReloadDataSets(self):
        raise Exception('Method should be implemented!')

    def addDataSet(self, ID):
        raise Exception('Method should be implemented!')

    def removeDataSet(self, ID):
        raise Exception('Method should be implemented!')

    def reloadDataSet(self, ID):
        raise Exception('Method should be implemented!')

    def clean(self):
        raise Exception('Method should be implemented!')

    def onDelete(self):
        from NOMAD_results_gui.results_gui.animation_tab import TIME_UPDATED_MSG_ID
        pub.unsubscribe(self.onChangeTime, TIME_UPDATED_MSG_ID)
        pub.unsubscribe(self.onChangeTimeLineStatus, self.getChangeTimeLineStatusMsgID())

    def onToolBarAction(self, event):
        mousePos = event.EventObject.mousePos
        self.contentPanel.toolBar.executeAction(self.toolIDs[event.Id], self.contentPanel, mousePos)

    def getChangeTimeLineStatusMsgID(self):
        raise Exception('Method should be implemented!')

    def onChangeTimeLineStatus(self, showTimeLine):
        if showTimeLine:
            self.contentPanel.showTimeLine(self.lastTimeValue)
        else:
            self.contentPanel.hideTimeLine()

        self.timeLineShown = showTimeLine

    def onChangeTime(self, timeInSeconds):
        self.lastTimeValue = self.getTimeValue(timeInSeconds)
        if self.timeLineShown:
            self.contentPanel.updateTimeLine(self.lastTimeValue)

    def getTimeValue(self, timeInSeconds):
        return timeInSeconds

    def addTimeLine(self):
        raise Exception('Method should be implemented!')

    @staticmethod
    def getType():
        raise Exception('Method should be implemented!')

    @staticmethod
    def getLabel():
        raise Exception('Method should be implemented!')

    @staticmethod
    def getShortLabel():
        raise Exception('Method should be implemented!')

    @staticmethod
    def createID(tabClass, IDnr):
        return '{}_{}'.format(tabClass.getType(), IDnr)

class EmptyControlPanel(wx.Panel):
    '''
    Empty control panel
    '''
