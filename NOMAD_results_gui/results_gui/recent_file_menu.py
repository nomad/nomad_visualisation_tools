""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import copy
import os.path
from pathlib import Path

import wx

import numpy as np
from NOMAD_results_gui.results_gui.constants import getConstant, RECENT_FILES_CONFIG_FILE, RECENT_FILES_CONFIG_FLNM, APP_NAME

from NOMAD import LOCAL_DIR


class RecentFileMenu():
    '''
    classdocs
    '''

    fileKeyPreFix = 'File_'
    fileCountKey = 'FileCount'
    maxFileCount = 8

    def __init__(self, parentMenu, startPos, bindFcn, onClickFcn):
        '''
        Constructor
        '''
        self.files = {}
        self.menus = []
        self.menuId2fileID = {}

        self.parentMenu = parentMenu
        self.bindFcn = bindFcn
        self.onClickFcn = onClickFcn

        self.separators = None
        self.startPos = startPos


        configFile = getConstant(RECENT_FILES_CONFIG_FILE)
        configFlNmBase = getConstant(RECENT_FILES_CONFIG_FLNM)
        
        if LOCAL_DIR.is_dir():
            configFilename = LOCAL_DIR.joinpath(configFile)                
        else:
            configFilename = configFlNmBase;
            
        doInit = not(configFilename.is_file())
        self.config = wx.FileConfig(appName=getConstant(APP_NAME), localFilename=str(configFilename))

        if doInit:
            self.config.WriteInt(self.fileCountKey, 0)
            self.config.Flush()

        # Load config file is exists
        fileCount = self.config.ReadInt(self.fileCountKey, 0)
        fileIDidx = 0
        for ii in range(1, fileCount+1):
            fileID = self.getFileID(ii)
            file_ii = Path(self.config.Read(fileID, ''))
            if not file_ii.is_file:
                continue

            fileIDidx += 1
            fileID = self.getFileID(fileIDidx)
            self.files[fileID] = file_ii

        self.updateMenu()

    def fillHasSimilarName(self):
        self.names = {}
        self.hasSimilarName = {}
        for ID, filePath in self.files.items():
            if filePath.name in self.names:
                self.hasSimilarName[ID] = True
                for otherID in self.names[filePath.name]:
                    self.hasSimilarName[otherID] = True

                self.names[filePath.name].append(ID)
            else:
                self.hasSimilarName[ID] = False
                self.names[filePath.name] = [ID]

    def add(self, filePath):
        # Build in check: If filePath is already in files put as first
        for fileID, otherFilePath in self.files.items():
            if otherFilePath == filePath:
                self.setFileAsFirst(fileID)
                self.updateConfigFile()
                self.updateMenu()
                return

        orgFiles = copy.copy(self.files)
        self.files = {}
        flCnt2copy = np.minimum(len(orgFiles), self.maxFileCount)
        for ii in range(1, flCnt2copy+1):
            self.files[self.getFileID(ii+1)] = orgFiles[self.getFileID(ii)]

        ID = self.getFileID(1)
        self.files[ID] = filePath

        self.updateConfigFile()
        self.updateMenu()

    def updateConfigFile(self):
        for ii in range(1, self.maxFileCount+1):
            self.config.DeleteEntry(self.getFileID(ii))

        self.config.WriteInt(self.fileCountKey, len(self.files))

        for ii in range(1, len(self.files)+1):
            fileID = self.getFileID(ii)
            self.config.Write(fileID, str(self.files[fileID]))

        self.config.Flush()

    def updateMenu(self):
        self.clean()
        if len(self.files) == 0:
            return

        self.fillHasSimilarName()

        menuPos = self.startPos
        self.separators = [self.parentMenu.InsertSeparator(menuPos)]
        menuPos += 1
        for ii in range(1, len(self.files)+1):
            fileID = self.getFileID(ii)
            filePath = self.files[fileID]
            fileLabel = filePath.name
            if self.hasSimilarName[fileID]:
                dirParts = os.path.split(filePath.parent)
                fileLabel = '{} ({})'.format(fileLabel, dirParts[-1])

            label = 'File &{:.0f}: {}\tCtrl+{:.0f}'.format(ii, fileLabel, ii)
            helpLabel = 'Open {}'.format(filePath)
            menu = self.parentMenu.Insert(menuPos, wx.ID_ANY, label, helpLabel)
            self.menus.append(menu)
            self.menuId2fileID[menu.GetId()] = fileID
            self.bindFcn(wx.EVT_MENU, self.onMenuClick, menu, id=fileID)
            menuPos += 1
        self.separators.append(self.parentMenu.InsertSeparator(menuPos))

    def clean(self):
        for menu in self.menus:
            self.parentMenu.Remove(menu)

        if self.separators is not None:
            for separator in self.separators:
                self.parentMenu.Remove(separator)
        self.menus = []
        self.menuId2fileID = {}
        self.separators = None


    def getFileID(self, index):
        return '{}{:.0f}'.format(self.fileKeyPreFix, index)

    def setFileAsFirst(self, fileID):
        orgFiles = copy.copy(self.files)
        firstFile = orgFiles[fileID]
        orgFiles.pop(fileID)
        self.files = {}
        fileInd = 2
        for ii in range(1, len(orgFiles)+2):
            flNm_ii = orgFiles.get(self.getFileID(ii))
            if flNm_ii is not None:
                self.files[self.getFileID(fileInd)] = orgFiles[self.getFileID(ii)]
                fileInd += 1

        self.files[self.getFileID(1)] = firstFile

    def onMenuClick(self, event):
        fileID = self.menuId2fileID[event.GetId()]
        filePath = self.files[fileID]
        if filePath.is_file():
            self.setFileAsFirst(fileID)
            self.onClickFcn(filePath)
            event.Skip()
        else:
            self.files.pop(fileID)
            errStr = 'The file {} no longer exists!'.format(filePath)
            dialog = wx.MessageDialog(None, errStr, 'Error', wx.OK | wx.ICON_ERROR)
            dialog.ShowModal()
            dialog.Destroy()

        self.updateConfigFile()
        self.updateMenu()
