""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from collections import namedtuple, Counter
import itertools
import logging

from matplotlib.lines import Line2D
from pubsub import pub
import wx

import numpy as np
from NOMAD_results_gui.results_gui.control_panels import addSelectDeselectButtons, \
    MultiCollapsiblePanel
from NOMAD_results_gui.results_gui.cust_wx_elements import CheckListBox
from NOMAD_results_gui.results_gui.data_tab import DataTab
from NOMAD_results_gui.results_gui.matplotlib_canvas_panel import MatplotLibCanvasPanel


Line = namedtuple('Line', ['ID', 'dataID', 'label', 'labelShort', 'legendLabel', 'type', 'lineCreateFcn', 'kwargs'])

class LineGraphTab(DataTab):
    '''classdocs'''

    def __init__(self, ID, parentFrame, dataMenu, *args, canShowTimeLine=False, **kwargs):
        '''Constructor'''
        DataTab.__init__(self, ID, parentFrame, dataMenu, canShowTimeLine=canShowTimeLine, *args, **kwargs)

        self.line2IDs = {}
        self.lines = {}

        pub.subscribe(self.onChangeLinesDisplayed, self.getChangeLinesMsgID())

    def clean(self):
        self.line2IDs = {}
        self.controlPanel.clean()
        self.contentPanel.clean()

    def onChangeLinesDisplayed(self, dataID, lineID, clearAxes):
        if clearAxes:
            self.contentPanel.clean()
            self.line2IDs = {}

        if dataID is None or lineID is None:
            return

        self.onChangeTimeLineStatus(self.timeLineShown)
        lines = self.lines[dataID]

        if lineID in self.line2IDs:
            self.contentPanel.removeLines(self.line2IDs[lineID])
            self.line2IDs.pop(lineID)
        else:
            line = lines[lineID]
            mlpLine = line.lineCreateFcn(self.dataMenu.dataSets[dataID], line)
            if isinstance(mlpLine, list):
                self.line2IDs[lineID] = [mlpLineInst.ID for mlpLineInst in mlpLine]
            else:
                self.line2IDs[lineID] = [mlpLine.ID]

            self.contentPanel.plotLine(mlpLine)

    def onDelete(self):
        super(LineGraphTab, self).onDelete()
        pub.unsubscribe(self.onChangeLinesDisplayed, self.getChangeLinesMsgID())
        self.controlPanel.plotControlPanel.onDelete()

    def preAddDataSets(self):
        pass

    def preRemoveDataSets(self):
        pass

    def preReloadDataSets(self):
        pass

    def postAddDataSets(self):
        pass

    def postRemoveDataSets(self):
        pass

    def postReloadDataSets(self):
        pass

    def addDataSet(self, ID):
        self.setLines(ID)
        label = self.dataMenu.id2label[ID]
        self.controlPanel.createLineSelectPanel(ID, label)
        self.controlPanel.addLineSelectPanelContent(ID, self.lines[ID])

    def removeDataSet(self, ID):
        label = self.dataMenu.id2label[ID]
        self.controlPanel.removeDataSet(ID, label)
        IDs = []
        IDs2pop = []
        for lineID in self.line2IDs:
            if (isinstance(lineID, tuple) and lineID[0] == ID) or \
                (isinstance(lineID, str) and lineID.startswith(str(ID))) :
                IDs += self.line2IDs[lineID]
                IDs2pop.append(lineID)

        if len(IDs):
            self.contentPanel.removeLines(IDs)
            for lineID in IDs2pop:
                self.line2IDs.pop(lineID)

        self.lines.pop(ID)

    def reloadDataSet(self, ID):
        if ID in self.dataSetIds:
            self.removeDataSet(ID)
            self.addDataSet(ID)

    def addTimeLine(self):
        self.controlPanel.addTimeLine(self.getChangeTimeLineStatusMsgID())

    def initControlPanel(self, *args, **kwargs):
        raise Exception('Implement method')

    def initContentPanel(self, *args, **kwargs):
        raise Exception('Implement method')

    def setLines(self):
        raise Exception('Implement method')

    def getSetLabelsMsgID(self):
        return 'setXyValues_{}_{}'.format(self.getType(), self.ID)

    def getCleanMsgID(self):
        return 'clean_{}_{}'.format(self.getType(), self.ID)

    def getChangeLinesMsgID(self):
        return 'changeLine_{}_{}'.format(self.getType(), self.ID)

    def getChangeTimeLineStatusMsgID(self):
        return 'changeTimeLineStatus_{}_{}'.format(self.getType(), self.ID)

    @staticmethod
    def getType():
        return 'line_graph'

    @staticmethod
    def getLabel():
        return 'Line graph'

    @staticmethod
    def getShortLabel():
        return 'Line graph'

class LineGraphControlPanel(wx.Panel):

    def __init__(self, plotMsgID, setLabelsMsgID, cleanMsgID, *args, **kwargs):
        wx.Panel.__init__(self, *args, **kwargs)
        self.plotMsgID = plotMsgID

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)

        from results_gui.control_panels import PlotControlPanel
        self.plotControlPanel = PlotControlPanel(self, setLabelsMsgID, cleanMsgID)
        sizer.Add(self.plotControlPanel, 0, wx.LEFT|wx.RIGHT, 7)

        self.dataComboBox = wx.Choice(self, choices=[])
        self.dataComboBox.Bind(wx.EVT_CHOICE, self.onChangeLineSelectPanel)
        self.dataComboBox.Enable(False)
        sizer.Add((0,10),0)
        sizer.Add(self.dataComboBox, 0, wx.ALL|wx.EXPAND, 5)

        addSelectDeselectButtons(self, sizer, None, self.onDeselectAll)
        self.Layout()
        self.SetMinSize((self.GetBestVirtualSize()[0] ,-1))
        self.sizer = sizer

        self.lineSelectPanels = {}
        self.label2ID = {}
        self.curID = None

    def addTimeLine(self, msgID):
        self.plotControlPanel.addShowTimeLineCheckBox(msgID)

    def createLineSelectPanel(self, ID, label):
        labels = self.dataComboBox.GetStrings()
        labels.append(label)
        self.dataComboBox.Clear()
        labels.sort()

        self.dataComboBox.AppendItems(labels)
        self.dataComboBox.Enable(True)
        self.label2ID[label] = ID
        self.lineSelectPanels[ID] = MultiCollapsiblePanel(self)
        self.sizer.Add(self.lineSelectPanels[ID], 1, wx.EXPAND|wx.TOP, 5)

        self.Layout()
        self.SetMinSize((self.GetBestVirtualSize()[0] ,-1))

        self.dataComboBox.SetSelection(labels.index(label))
        self.onChangeLineSelectPanel(label=label)

    def removeDataSet(self, ID, label):
        curLabel = self.dataComboBox.GetStringSelection()
        labels = self.dataComboBox.GetStrings()
        labels.remove(label)
        self.dataComboBox.Clear()
        labels.sort()
        self.dataComboBox.AppendItems(labels)

        self.label2ID.pop(label)
        self.lineSelectPanels[ID].Destroy()
        self.lineSelectPanels.pop(ID)

        if len(labels) == 0:
            self.dataComboBox.Enable(False)
            self.curID = None

        self.Layout()
        self.SetMinSize((self.GetBestVirtualSize()[0] ,-1))

        if len(labels) > 0 and self.curID == ID:
            self.curID = None
            self.dataComboBox.SetSelection(0)
            self.onChangeLineSelectPanel(label=labels[0])
        elif len(labels) > 0:
            self.dataComboBox.SetSelection(labels.index(curLabel))

    def createLineCheckListBox(self, parent, lines):
        elements = [(ID, line.labelShort) for ID, line in lines.items()]
        checkBoxList = CheckListBox(elements, parent, style=wx.LB_MULTIPLE|wx.LB_HSCROLL|wx.LB_NEEDED_SB)
        self.Bind(wx.EVT_CHECKLISTBOX, self.onPlot, checkBoxList)
        return checkBoxList

    def onChangeLineSelectPanel(self, event=None, label=None):
        if label is None:
            label = self.dataComboBox.GetStringSelection()

        assert(label is not None), 'Label is None!'

        ID = self.label2ID[label]
        if ID == self.curID:
            return

        if self.curID is not None:
            self.lineSelectPanels[self.curID].Hide()

        self.lineSelectPanels[ID].Show()
        self.curID = ID
        self.Layout()
        self.SetMinSize((self.GetBestVirtualSize()[0] ,-1))

    def clean(self):
        self.plotControlPanel.clean()

    def onDeselectAll(self, event):
        for lineSelectPanel in self.lineSelectPanels.values():
            for contentPanel in lineSelectPanel.contentPanels:
                checkedItems = contentPanel.GetCheckedItems()
                for item in checkedItems:
                    contentPanel.Check(item, check=False)

        pub.sendMessage(self.plotMsgID, dataID=None, lineID=None, clearAxes=True)

    def onPlot(self, event):
        lineID = event.EventObject.IDs[event.GetInt()]
        pub.sendMessage(self.plotMsgID, dataID=self.curID, lineID=lineID, clearAxes=False)

class LineGraphContentPanel(MatplotLibCanvasPanel):

    log = logging.getLogger(__name__)
    GAP_FACTORS = {'x':0.05, 'y':0.05}

    def __init__(self, tabParent, *args, **kwargs):
        '''Constructor'''
        MatplotLibCanvasPanel.__init__(self, *args, **kwargs)
        self.tabParent = tabParent

        # Support for clickable lines or can overwritten to support other types as well
        self.labels = {}
        self.points = None
        self.xBuf = None
        self.yBuf = None
        self.xNorm = None
        self.yNorm = None
        self.curLocPointer = None
        self.curIdx = None
        self.timeLine = None
        self.lastTimeValue = None
        self.curPoint = [np.NaN, np.NaN]
        self.canvas.mpl_connect('button_press_event', self.onClick)
        self.curDataLims = [[np.inf, -np.inf], [np.inf, -np.inf]]

        self.legendInfo = {'handles':[], 'labels':[]}

        self.makersBase = ['o', 's', 'x', '*', 'D', '+']
        self.colorList = ['r', 'b', 'y', 'c', 'm', 'g']
        self.makers = []
        for ii in range(len(self.colorList)):
            for jj in range(len(self.makersBase)):
                markerInd = jj + ii
                if markerInd >= len(self.makersBase):
                    markerInd = markerInd - len(self.makersBase)
                self.makers.append(self.makersBase[markerInd])

        self.colorMarkerCombs = []
        self.colorCycler = itertools.cycle(self.colorList)
        self.markerCycler = itertools.cycle(self.makers)
        self.linesPlotted = {}

        xLabel = self.getXlabel()
        yLabel = self.getYlabel()

        from results_gui.navigation_tool_bar import GRID_ID
        for dataAx in self.axesGrid.dataAxes.values():
            dataAx.xLabel = xLabel
            dataAx.yLabel = yLabel

            self.toolBar.executeAction(GRID_ID, self, None, dataAx.ax.get_label())

    def createDcsLineGroup(self, simResults, line):
        IDorders = simResults.dcs.IDorderPerDcs[line.dataID]
        lines = []
        IDsProcessed = []

        colorMarkerComb = []

        if len(self.colorMarkerCombs) >= simResults.dcs.maxCellCount[line.dataID]:
            colorMarkerComb = self.colorMarkerCombs[:simResults.dcs.maxCellCount[line.dataID]]
            self.colorMarkerCombs = self.colorMarkerCombs[simResults.dcs.maxCellCount[line.dataID]:]
        else:
            for _ in range(simResults.dcs.maxCellCount[line.dataID]):
                colorMarkerComb.append((self.colorCycler.__next__(),
                                        self.markerCycler.__next__()))

        for IDorder in IDorders:
            for ii in range(1, len(IDorder)+1):
                dataID = IDorder[-ii]
                ID = (line.ID[0], dataID)
                if ID in IDsProcessed:
                    continue
                IDsProcessed.append(ID)
                mplLine = self.createLine(simResults, Line(ID, dataID, '{}'.format(ID), '{}'.format(ID),
                                                           '{}: {}'.format(simResults.shortLabel, ID), None, None, {}))
                mplLine.set_color(colorMarkerComb[ii-1][0])
                mplLine.set_marker(colorMarkerComb[ii-1][1])
                mplLine.colorMarkerSet = True
                lines.append(mplLine)

        return lines

    def doPlot(self, line, doCycle=True, isLast=True):
        self.axesGrid.getDataAx().ax.add_artist(line)
        if isinstance(line, ClickableLine):
            self.addLineData(line.get_xdata(), line.get_ydata(), line.get_label())

        if doCycle and not (isinstance(line, ClickableLine) and line.colorMarkerSet):
            if len(self.colorMarkerCombs):
                line.set_color(self.colorMarkerCombs[0][0])
                line.set_marker(self.colorMarkerCombs[0][1])
                self.colorMarkerCombs.pop(0)
            else:
                line.set_color(self.colorCycler.__next__())
                line.set_marker(self.markerCycler.__next__())
        line.set_markersize(9)
        line.set_zorder(1)

        self.add2Legend(line)
        self.setDataLim(line)

        self.linesPlotted[line.ID] = line

        if isLast:
            self.finalizePlot(True, True, {'x':self.curDataLims[0], 'y':self.curDataLims[1]},
                              gapFactors=self.GAP_FACTORS, legendInfo=self.legendInfo)

            self.canvas.draw()
            self.canvas._onSize(None)

    def removeLines(self, IDs):
        if Counter(IDs) == Counter(self.linesPlotted.keys()):
            self.hideTimeLine()
            self.clean()
            return

        for ID in IDs:
            colorMarkerComb = (self.linesPlotted[ID].get_color(), self.linesPlotted[ID].get_marker())
            if colorMarkerComb not in self.colorMarkerCombs:
                self.colorMarkerCombs.append(colorMarkerComb)
            self.linesPlotted[ID].remove()
            self.linesPlotted.pop(ID)

        self.legendInfo = {'handles':[], 'labels':[]}

        self.labels = {}
        self.points = None
        self.xBuf = None
        self.yBuf = None
        self.xNorm = None
        self.yNorm = None
        self.curIdx = None
        self.curDataLims = [[np.inf, -np.inf], [np.inf, -np.inf]]

        for line in self.linesPlotted.values():
            self.addLineData(line.get_xdata(), line.get_ydata(), line.get_label())
            self.add2Legend(line)
            self.setDataLim(line)

        self.finalizePlot(True, True, {'x':self.curDataLims[0], 'y':self.curDataLims[1]},
                    gapFactors=self.GAP_FACTORS, legendInfo=self.legendInfo)

        self.canvas.draw()
        self.canvas._onSize(None)

    def setDataLim(self, line):
        self.curDataLims[0][0] = np.minimum(self.curDataLims[0][0], np.min(line.get_xdata()))
        self.curDataLims[1][0] = np.minimum(self.curDataLims[1][0], np.min(line.get_ydata()))
        self.curDataLims[0][1] = np.maximum(self.curDataLims[0][1], np.max(line.get_xdata()))
        self.curDataLims[1][1] = np.maximum(self.curDataLims[1][1], np.max(line.get_ydata()))

    def add2Legend(self, line):
        self.legendInfo['handles'].append(line)
        self.legendInfo['labels'].append(line.get_label())

    def plotLine(self, lines):
        if not isinstance(lines, list):
            lines = [lines]

        for ii in range(len(lines)):
            self.doPlot(lines[ii], len(lines) == 1 , len(lines) - 1 == ii)

    def addLineData(self, xData, yData, label):
        if self.points is None:
            self.points = np.column_stack((xData, yData))
            self.xBuf = np.inf
            self.yBuf = np.inf
            startInd = 0
        else:
            startInd = len(self.points)
            self.points = np.append(self.points, np.column_stack((xData, yData)), axis=0)

        for ii in range(startInd, len(self.points)):
            self.labels[ii] = label

        if self.curLocPointer is None:
            self.curLocPointer = self.axesGrid.getDataAx().ax.plot([np.NaN], [np.NaN], marker='o',
                                                                       markeredgecolor='k', markeredgewidth=2,
                                                                       markersize=11, markerfacecolor='None',
                                                                       zorder=2)[0]

        uniqueCnt_y = len(np.unique(yData))
        uniqueCnt_x = len(np.unique(xData))
        yLength = np.nanmax(yData) - np.nanmin(yData)
        xLength = np.nanmax(xData) - np.nanmin(xData)

        if yLength == 0:
            yLength = 1

        if xLength == 0:
            xLength = 1

        self.xBuf = np.minimum(self.xBuf, xLength/uniqueCnt_x*5)
        self.yBuf = np.minimum(self.yBuf, yLength/uniqueCnt_y*5)

        self.xNorm = xLength
        self.yNorm = yLength

    def clean(self):
        self.resetToDefault()

        self.labels = {}
        self.points = None
        self.xBuf = None
        self.yBuf = None
        self.xNorm = None
        self.yNorm = None
        self.curIdx = None

        self.curLocPointer = None
        self.curPoint = [np.NaN, np.NaN]
        pub.sendMessage(self.tabParent.getCleanMsgID())

        self.curDataLims = [[np.inf, -np.inf], [np.inf, -np.inf]]
        self.legendInfo = {'handles':[], 'labels':[]}
        self.colorCycler = itertools.cycle(self.colorList)
        self.markerCycler = itertools.cycle(self.makers)
        self.linesPlotted = {}
        self.dataLims = {}
        self.colorMarkerCombs = []

        self.finalizePlot()
        self.canvas.draw()

    # Support for clickable lines
    def onClick(self, event):
        x, y = event.xdata, event.ydata
        if x is None or self.points is None:
            return
        idx = np.nanargmin((np.divide(self.points - (x,y), (self.xNorm, self.yNorm))**2).sum(axis = -1))
        xP, yP = self.points[idx]
        if np.abs(xP - x) < self.xBuf and np.abs(yP - y) < self.yBuf:
            self.setPoint(idx)
        elif not np.all(np.isnan(self.curPoint)):
            idx = None
            self.setPoint(idx)

    def setPoint(self, idx):
        if idx is None:
            xP = np.NaN
            yP = np.NaN
            label = 'NA'
        else:
            xP, yP = self.points[idx]
            label = self.labels[idx]

        pub.sendMessage(self.tabParent.getSetLabelsMsgID(), label=label, fieldValues={'x':xP, 'y':yP})
        self.curPoint = [xP, yP]
        self.curLocPointer.set_data(xP, yP)
        self.curIdx = idx
        self.canvas.draw()

    def selectNext(self):
        if self.curIdx is None or self.curIdx == len(self.points) - 1:
            return

        self.setPoint(self.curIdx + 1)

    def selectPrevious(self):
        if self.curIdx is None or self.curIdx == 0:
            return

        self.setPoint(self.curIdx - 1)

    def onKeyPress(self, event):
        MatplotLibCanvasPanel.onKeyPress(self, event)
        if event.key == 'left':
            self.selectPrevious()
        elif event.key == 'right':
            self.selectNext()

    def getXlabel(self):
        raise Exception('Implement method!')

    def getYlabel(self):
        raise Exception('Implement method!')


def getLabel(label, ind):
    if isinstance(label, (list, tuple)):
        return label[ind]
    else:
        return label

class ClickableLine(Line2D):

    def __init__(self, ID, dataID, xData, yData, label, *args, **kwargs):
        Line2D.__init__(self, xData, yData, label=label, *args, **kwargs)
        self.ID = ID
        self.dataID = dataID
        self.colorMarkerSet = False

