""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from NOMAD_results_gui.results_gui.line_graph_tab import LineGraphTab, Line, \
    LineGraphControlPanel, LineGraphContentPanel, ClickableLine
from NOMAD_results_gui.results_gui.load_output import BOUNDARY_SENT, \
    BOUNDARY_RECEIVED, BOUNDARY_CREATED, BOUNDARY_QUEUE
import numpy as np


DSC_LINE_TYPE = 'dcs'
DEMAND_LINE_TYPE = 'demand'
TRANSFER_LINE_TYPE = 'transfer'
CUM_TRANSFER_LINE_TYPE = 'cumTransfer'
BOUNDARY_LINE_TYPE = 'boundary'


class CountGraphTab(LineGraphTab):
    '''classdocs'''

    def __init__(self, ID, parentFrame, dataMenu, *args, **kwargs):
        '''Constructor'''
        LineGraphTab.__init__(self, ID, parentFrame, dataMenu, *args,
                                canShowTimeLine=True, **kwargs)

    def initControlPanel(self, *args, **kwargs):
        return CountGraphControlPanel(self.getChangeLinesMsgID(),
                                     self.getSetLabelsMsgID(),
                                     self.getCleanMsgID(), *args, **kwargs)

    def initContentPanel(self, *args, **kwargs):
        return CountGraphContentPanel(self, self.parentFrame, self.ID, *args, **kwargs)

    def setLines(self, dataID):
        lines = {}

        simResults = self.dataMenu.dataSets[dataID]
        for ID in simResults.counts:
            if ID not in simResults.dcs.cell2dcs:
                lineID = (dataID, ID)
                label = '{}'.format(ID)
                lines[lineID] = Line(lineID, ID, label, label, self.contentPanel.createLine, {})

            for inOut, inOutData in simResults.counts[ID].demand.items():
                for portLetter, countData in inOutData.items():
                    if np.all(countData == 0):
                        continue
                    lineID = '{}_{}_d_{}_{}'.format(dataID, ID, inOut, portLetter)
                    label = '{} - demand - {} - {}'.format(ID, inOut, portLetter)
                    labelShort = '{} - {} - {}'.format(ID, inOut, portLetter)
                    legendLabel = '{}: d - {}'.format(simResults.shortLabel, labelShort)
                    lines[lineID] = Line(lineID, ID, label, labelShort, legendLabel, DEMAND_LINE_TYPE,
                                         self.contentPanel.createDemandLine,
                                         {'inOut':inOut, 'portLetter':portLetter})

            for inOut, inOutData in simResults.counts[ID].transfer.items():
                for portLetter, countData in inOutData.items():
                    if np.all(countData == 0):
                        continue
                    lineID = '{}_{}_t_{}_{}'.format(dataID, ID, inOut, portLetter)
                    label = '{} - transfer - {} - {}'.format(ID, inOut, portLetter)
                    labelShort = '{} - {} - {}'.format(ID, inOut, portLetter)
                    legendLabel = '{}: t - {}'.format(simResults.shortLabel, labelShort)
                    lines[lineID] = Line(lineID, ID, label, labelShort, legendLabel, TRANSFER_LINE_TYPE,
                                         self.contentPanel.createTransferLine,
                                         {'inOut':inOut, 'portLetter':portLetter})

            for inOut, inOutData in simResults.counts[ID].cumTransfer.items():
                for portLetter, countData in inOutData.items():
                    if np.all(countData == 0):
                        continue
                    lineID = '{}_{}_ct_{}_{}'.format(dataID, ID, inOut, portLetter)
                    label = '{} - cum. transfer - {} - {}'.format(ID, inOut, portLetter)
                    labelShort = '{} - {} - {}'.format(ID, inOut, portLetter)
                    legendLabel = '{}: ct - {}'.format(simResults.shortLabel, labelShort)
                    lines[lineID] = Line(lineID, ID, label, labelShort, legendLabel, CUM_TRANSFER_LINE_TYPE,
                                         self.contentPanel.createCumTransferLine,
                                         {'inOut':inOut, 'portLetter':portLetter})


        for dcsID in simResults.dcs.IDorderPerDcs:
            lineID = (dataID, dcsID)
            label = '{}'.format(dcsID)
            legendLabel = '{}: {}'.format(simResults.shortLabel, label)
            lines[lineID] = Line(lineID, dcsID, label, label, legendLabel, DSC_LINE_TYPE,
                                 self.contentPanel.createDcsLineGroup, {})

        BOUNDARY_FLD_2_FCN = {
            BOUNDARY_SENT:self.contentPanel.createSentLine,
            BOUNDARY_RECEIVED:self.contentPanel.createReceivedLine,
            BOUNDARY_CREATED:self.contentPanel.createCreatedLine,
            BOUNDARY_QUEUE:self.contentPanel.createQueueLine
            }

        for ID, boundaryCounts in simResults.boundaryCounts.items():
            for field in boundaryCounts._fields:
                lineID = '{}_{}_{}'.format(dataID, ID, field)
                label = '{} - {}'.format(ID, field)
                legendLabel = '{}: {}'.format(simResults.shortLabel, label)
                lines[lineID] = Line(lineID, ID, label, label, legendLabel, BOUNDARY_LINE_TYPE,
                                     BOUNDARY_FLD_2_FCN[field], {})

        self.lines[dataID] = lines

    def onDelete(self):
        LineGraphTab.onDelete(self)

    @staticmethod
    def getType():
        return 'counts_graph'

    @staticmethod
    def getLabel():
        return 'Counts graphs'

    @staticmethod
    def getShortLabel():
        return 'Counts'

class CountGraphControlPanel(LineGraphControlPanel):

    def __init__(self, plotMsgID, setLabelsMsgID, *args, **kwargs):
        LineGraphControlPanel.__init__(self, plotMsgID, setLabelsMsgID, *args, **kwargs)

    def addLineSelectPanelContent(self, dataID, lines):
        dcsLines = {key:line for key, line in lines.items() if line.type == DSC_LINE_TYPE}
        self.lineSelectPanels[dataID].addPane("DCSs", False,
                                           self.createLineCheckListBox, dcsLines)

        boundaryLines = {key:line for key, line in lines.items() if line.type == BOUNDARY_LINE_TYPE}
        self.lineSelectPanels[dataID].addPane("Boundaries", False,
                                           self.createLineCheckListBox, boundaryLines)

        demandLines = {key:line for key, line in lines.items() if line.type == DEMAND_LINE_TYPE}
        self.lineSelectPanels[dataID].addPane("Demands", True,
                                           self.createLineCheckListBox, demandLines)

        transferLines = {key:line for key, line in lines.items() if line.type == TRANSFER_LINE_TYPE}
        self.lineSelectPanels[dataID].addPane("Transfers", True,
                                           self.createLineCheckListBox, transferLines)

        cumTransferLines = {key:line for key, line in lines.items() if line.type == CUM_TRANSFER_LINE_TYPE}
        self.lineSelectPanels[dataID].addPane("Cum. transfers", True,
                                           self.createLineCheckListBox, cumTransferLines)


class CountGraphContentPanel(LineGraphContentPanel):

    COUNT_TYPE = 'count'
    STREAM_TYPE = 'stream'
    RECEIVED_TYPE = 'received'
    SENT_TYPE = 'sent'
    QUEUE_TYPE = 'queue'
    CREATED_TYPE = 'created'
    DEMAND_TYPE = 'demand'
    TRANSFER_TYPE = 'transfer'
    CUM_TRANSFER_TYPE = 'cumTransfer'


    def __init__(self, *args, **kwargs):
        '''Constructor'''
        LineGraphContentPanel.__init__(self, *args, **kwargs)

    def createLine(self, simResults, line, lineType=COUNT_TYPE):
        timeVec = simResults.timeVec

        if lineType in (self.COUNT_TYPE, self.STREAM_TYPE, self.DEMAND_TYPE,
                        self.TRANSFER_TYPE, self.CUM_TRANSFER_TYPE):
            countInCell = simResults.counts[line.dataID]
            xData = timeVec[countInCell.timeInd[0]:countInCell.timeInd[1]]
            if lineType == self.COUNT_TYPE:
                yData = countInCell.count
            elif lineType == self.STREAM_TYPE:
                yData = countInCell.countStreams
            elif lineType == self.DEMAND_TYPE:
                yData = countInCell.demand[line.kwargs['inOut']][line.kwargs['portLetter']]
            elif lineType == self.TRANSFER_TYPE:
                yData = countInCell.transfer[line.kwargs['inOut']][line.kwargs['portLetter']]
            elif lineType == self.CUM_TRANSFER_TYPE:
                yData = countInCell.cumTransfer[line.kwargs['inOut']][line.kwargs['portLetter']]
        elif lineType in (self.RECEIVED_TYPE, self.SENT_TYPE, self.CREATED_TYPE, self.QUEUE_TYPE):
            xData = timeVec
            if lineType == self.RECEIVED_TYPE:
                yData = simResults.boundaryCounts[line.dataID].received
            elif lineType == self.SENT_TYPE:
                yData = simResults.boundaryCounts[line.dataID].sent
            elif lineType == self.CREATED_TYPE:
                yData = simResults.boundaryCounts[line.dataID].created
            elif lineType == self.QUEUE_TYPE:
                yData = simResults.boundaryCounts[line.dataID].queueLength
        else:
            raise Exception('Unknown lineType "{}"'.format(lineType))

        if isinstance(yData, list):
            yData = np.array(yData)

        #TEMP
        if len(yData) == len(xData) + 1:
            yData = yData[1:]

        return ClickableLine(line.ID, line.dataID, xData, yData, line.legendLabel)

    def createStreamLine(self, simResults, line):
        return self.createLine(simResults, line, lineType=self.STREAM_TYPE)

    def createReceivedLine(self, simResults, line):
        return self.createLine(simResults, line, lineType=self.RECEIVED_TYPE)

    def createSentLine(self, simResults, line):
        return self.createLine(simResults, line, lineType=self.SENT_TYPE)

    def createCreatedLine(self, simResults, line):
        return self.createLine(simResults, line, lineType=self.CREATED_TYPE)

    def createQueueLine(self, simResults, line):
        return self.createLine(simResults, line, lineType=self.QUEUE_TYPE)

    def createDemandLine(self, simResults, line):
        return self.createLine(simResults, line, lineType=self.DEMAND_TYPE)

    def createTransferLine(self, simResults, line):
        return self.createLine(simResults, line, lineType=self.TRANSFER_TYPE)

    def createCumTransferLine(self, simResults, line):
        return self.createLine(simResults, line, lineType=self.CUM_TRANSFER_TYPE)

    def getXlabel(self):
        return 'Time [s]'

    def getYlabel(self):
        return 'Counts [#ped]'