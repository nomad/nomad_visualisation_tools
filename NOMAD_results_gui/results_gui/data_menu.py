""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging
from math import floor
from pathlib import Path

from pubsub import pub
import wx

from NOMAD import LOCAL_DIR
from NOMAD_results_gui.results_gui.constants import getConstant, APP_PATH
from NOMAD_results_gui.results_gui.load_output import LOAD_PROGRESS, loadNomadData
from NOMAD_results_gui.results_gui.recent_file_menu import RecentFileMenu


DATA_ADDED_MSG_ID = 'dataSet_Added'
DATA_REMOVED_MSG_ID = 'dataSet_Removed'
DATA_RELOADED_PRE_MSG_ID = 'dataSet_pre_Reload'
DATA_RELOADED_MSG_ID = 'dataSet_Reloaded'

NOMAD_OUTPUT_EXT = '.scen'

class DataMenu():
    '''classdocs'''

    log = logging.getLogger(__name__)

    def __init__(self):
        '''Constructor'''
        self.dataSets = {}
        self.ids = []
        self.id2label = {}
        self.loadedFiles = {}

        self.parentFrame = None
        self.progressBase = 0
        self.progressRange = 0

        pub.subscribe(self.updateProgressDialog, LOAD_PROGRESS)

    def loadDataSet(self, filePath, ID=None):
        # Load data into data structure
        self.log.info('Loading data set from "{}" ...'.format(filePath))
        if filePath.suffix == NOMAD_OUTPUT_EXT:
            simResults = loadNomadData(filePath)
        else:
            raise Exception('Unsupported file extension "{}"'.format(''))

        self.log.info('Successfully loaded data set')
        self.log.debug('Updating list menus ...')

        if ID is None:
            ID = self.getID()
            self.ids.append(ID)
        else:
            self.id2label.pop(ID)

        label = self.getLabel(simResults.label, filePath)
        self.id2label[ID] = label
        self.loadedFiles[ID] = filePath
        self.dataSets[ID] = simResults

    def setListMenus(self):
        self.listMenuItem2id = {}
        for menu, menuInfo in self.listMenus.items():
            for menuItem in menuInfo['subMenu'].GetMenuItems():
                menuInfo['subMenu'].DestroyItem(menuItem)

            if len(self.ids) == 0:
                menu.Enable(False)
                continue

            ii = 1
            for ID in self.ids:
                item = menuInfo['subMenu'].Append(wx.ID_ANY, '{}. {}'.format(ii, self.id2label[ID]), '{} {}'.format(menuInfo['helpString'], self.id2label[ID]))
                menuInfo['subMenu'].Bind(wx.EVT_MENU, menuInfo['handler'], item)
                self.listMenuItem2id[item.Id] = ID
                ii += 1

            menu.Enable(True)

    def unloadDataSet(self, ID):
        self.log.info('Unloading data set "{}" ...'.format(ID))

        # Send message to inform any listeners that a data set has been removed
        pub.sendMessage(DATA_REMOVED_MSG_ID, ID=ID)
        self.log.info('Successfully unloaded the data set')

        # Remove
        self.id2label.pop(ID)
        self.loadedFiles.pop(ID)
        self.dataSets.pop(ID)
        self.ids.remove(ID)

        # Update all list sub menus
        self.setListMenus()

    def addMenu(self, menuBar, parentFrame):
        self.parentFrame = parentFrame

        self.listMenus = {}

        self.dataMenu = wx.Menu()

        def append(itemString, helpString, handler):
            """ Create menu events. """
            item = self.dataMenu.Append(wx.ID_ANY, itemString, helpString)
            self.dataMenu.Bind(wx.EVT_MENU, handler, item)
            return item

        append('&Open and show data set\tCtrl+O', 'Open and show a data set', self.onOpenDataSet)
        append('&Load data set\tCtrl+L', 'Load a data set', self.onLoadDataSet)
        self.addListSubMenu('&Unload data set', 'Unload a data set', self.onUnloadDataSet, 'Unload')
        self.dataMenu.AppendSeparator()
        append('&Reload all data sets\tCtrl+R', 'Reload all data sets', self.onReloadAllDataSets)
        self.addListSubMenu('Reload data set', 'Reload a data set', self.onReloadDataSet, 'Reload')
        self.dataMenu.AppendSeparator()
        recentFilePos = self.dataMenu.MenuItemCount
        self.recentFileMenu = RecentFileMenu(self.dataMenu, recentFilePos, self.dataMenu.Bind, self.loadRecentFile)

        self.listMenuItem2id = {}
        menuBar.Append(self.dataMenu, '&Data')

    def addListSubMenu(self, itemString, helpString, subMenuHandler, subMenuHelpString):
        listSubMenu = wx.Menu()
        parentMenu = self.dataMenu.AppendSubMenu(listSubMenu, itemString, helpString)
        parentMenu.Enable(False)
        self.listMenus[parentMenu] = {'subMenu':listSubMenu, 'handler':subMenuHandler, 'helpString':subMenuHelpString}

    def loadRecentFile(self, filePath):
        self.onLoadDataSet(filePath=filePath, display=True)

    def onOpenDataSet(self, event):
        self.onLoadDataSet(display=True)

    def onLoadDataSet(self, event=None, IDs=None, filePath=None, display=False):
        if IDs is not None:
            filePaths = [self.loadedFiles[ID] for ID in IDs]
            IDs = {self.loadedFiles[ID]:ID for ID in IDs}
            isReload = True
            addToAll = False
        else:
            if filePath is not None:
                filePaths = [filePath]
            else:
                filePaths = self.aksForFile()

            if filePaths is None:
                return

            addToAll = len(self.loadedFiles) == 0
            isReload = False
            IDs = {filePath:None for filePath in filePaths}

        self.createProgressDialog(0, 'Loading data')
        self.progressBase = 0
        self.progressRange = (100.0-10*addToAll)/len(filePaths)

        anyNewLoaded = False
        for filePath in filePaths:
            self.progressDialog.Update(self.progressBase, 'Loading data from {}'.format(filePath.name))
            if filePath in self.loadedFiles.values() and not isReload:
                msg = 'Data set {} has already been loaded.'.format(filePath.name)
                dialog = wx.MessageDialog(self.parentFrame, msg, "Data set already loaded", wx.OK|wx.ICON_INFORMATION)
                lastMessage = self.progressDialog.GetMessage()
                lastValue = self.progressDialog.GetValue()
                self.progressDialog.Destroy()
                dialog.ShowModal()
                dialog.Destroy()
                self.createProgressDialog(lastValue, lastMessage)
                self.progressBase += self.progressRange
                continue
            try:
                self.loadDataSet(filePath, IDs[filePath])
                anyNewLoaded = True
                succes = True
            except Exception as err:
                self.log.error(str(err.args[0]), exc_info=True)
                dialog = wx.MessageDialog(self.parentFrame, str(err.args[0]), 'Error', wx.OK |wx.ICON_ERROR)
                lastMessage = self.progressDialog.GetMessage()
                lastValue = self.progressDialog.GetValue()
                self.progressDialog.Destroy()
                dialog.ShowModal()
                dialog.Destroy()
                self.createProgressDialog(lastValue, lastMessage)
                succes = False

            self.progressBase += self.progressRange

            if succes:
                self.recentFileMenu.add(filePath)

        if (anyNewLoaded or display) and not isReload:
            if display and not isReload:
                addToAll = True
            # Update all list sub menus
            self.setListMenus()
            self.log.debug('List menus updated')
            # Send message to inform any listeners that a data set has been added
            pub.sendMessage(DATA_ADDED_MSG_ID, addToAll=addToAll)
            if addToAll:
                # Add data sets to all existing tabs
                self.progressDialog.Update(90, 'Plotting data')
                self.progressDialog.Destroy()

        self.progressDialog.Destroy()

    def createProgressDialog(self, value=None, msg=None):
        self.progressDialog = wx.ProgressDialog('', '', parent=self.parentFrame)
        if value is not None and msg is not None:
            self.progressDialog.Update(value, msg)

    def updateProgressDialog(self, progress, message):
        newPerc = floor(self.progressBase + self.progressRange*progress/100)
        self.log.debug('{} - {}'.format(newPerc, message))
        self.progressDialog.Update(newPerc, message)

    def onUnloadDataSet(self, event):
        self.unloadDataSet(self.listMenuItem2id[event.Id])

    def onReloadDataSet(self, event):
        pub.sendMessage(DATA_RELOADED_PRE_MSG_ID)
        ID = self.listMenuItem2id[event.Id]
        self.reloadDataSet(ID)
        pub.sendMessage(DATA_RELOADED_MSG_ID, ids=[ID])

    def onReloadAllDataSets(self, event):
        if len(self.ids) == 0:
            return

        pub.sendMessage(DATA_RELOADED_PRE_MSG_ID)
        self.onLoadDataSet(IDs=self.ids)
        pub.sendMessage(DATA_RELOADED_MSG_ID, ids=self.ids)

    def reloadDataSet(self, ID):
        self.onLoadDataSet(IDs=[ID])

    def aksForFile(self):
        wildcard = 'All results (*' + NOMAD_OUTPUT_EXT + ')|*' + NOMAD_OUTPUT_EXT +'|'\
        'NOMAD results (*' + NOMAD_OUTPUT_EXT +')|*' + NOMAD_OUTPUT_EXT +'|' \
        'All files (*.*)|*.*'
        defDir = getConstant(APP_PATH)
        dialog = wx.FileDialog(None, "Choose a file", str(defDir), "", wildcard, wx.FD_OPEN|wx.FD_MULTIPLE)
        if dialog.ShowModal() == wx.ID_OK:
            filePaths = [Path(filename) for filename in dialog.GetPaths()]
            dialog.Destroy()
            return filePaths
        else:
            return None

    def getDataSetInfo(self, dataSetID):
        return self.dataSets[dataSetID], self.id2label[dataSetID]

    def getID(self):
        ii = 1
        while ii in self.ids:
            ii += 1

        return ii

    def getLabel(self, label, filePath):
        if label in self.id2label.values():
            newLabel = '{} ({})'.format(label, filePath.parent.name)
            if newLabel in list(self.id2label.values()):
                label = '{} ({})'.format(label, filePath)
            else:
                label = newLabel

        return label