""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging
import math

import wx


class Panel(wx.Panel):

    def __init__(self, *args, **kwargs):
        if 'style' in kwargs:
            kwargs['style'] = kwargs['style']|wx.WANTS_CHARS
        else:
            kwargs['style'] = wx.WANTS_CHARS
        wx.Panel.__init__(self, *args, **kwargs)

        self.Bind(wx.EVT_KEY_DOWN, self.onKeyDown)
        #self.Bind(wx.EVT_NAVIGATION_KEY, self.onNavKey)

    def onNavKey(self, event):
        print('OnNavKey')
        event.Skip()

    def onKeyDown(self, event):
        key = event.GetUnicodeKey()
        if key == wx.WXK_NONE:
            key = event.GetKeyCode()

        print('Key: {}'.format(key))


wxEVT_TIMESLIDER_SCROLL_EVENT = wx.NewEventType()
EVT_TIMESLIDER_SCROLL_EVENT = wx.PyEventBinder(wxEVT_TIMESLIDER_SCROLL_EVENT, 1)

class TimeSlider(Panel):

    log = logging.getLogger(__name__)

    TOP_ID = 'top'
    LEFT_ID = 'left'
    RIGHT_ID = 'right'

    MAX_TICK_COUNT = 100

    def __init__(self, parent, panelProps={}, labelProps={}, sliderProps={}):
        Panel.__init__(self, parent, **panelProps)

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)

        if 'style' in sliderProps:
            sliderProps.pop('style')
            self.log.warn('The slider style property is ignored!')

        if 'style' in labelProps:
            labelProps.pop('style')
            self.log.warn('The label style property is ignored!')

        self.slider = wx.Slider(self, style=wx.SL_AUTOTICKS, **sliderProps)
        self.slider.Bind(wx.EVT_SCROLL, self.onScrollChanged)

        self.topLabel = wx.StaticText(self, style=wx.ALIGN_CENTRE_HORIZONTAL, **labelProps)
        self.leftLabel = wx.StaticText(self, style=wx.ALIGN_CENTRE_HORIZONTAL, **labelProps)
        self.rightLabel = wx.StaticText(self, style=wx.ALIGN_CENTRE_HORIZONTAL, **labelProps)

        self.type2label = {
            self.TOP_ID:self.topLabel,
            self.LEFT_ID:self.leftLabel,
            self.RIGHT_ID:self.rightLabel,
            }

        bottomSizer = wx.BoxSizer(wx.HORIZONTAL)
        bottomSizer.Add(self.leftLabel, 0)
        bottomSizer.Add(self.slider, 1, wx.EXPAND)
        bottomSizer.Add(self.rightLabel, 0)

        sizer.Add(self.topLabel, 0, wx.ALIGN_CENTER)
        sizer.Add(bottomSizer, 1, wx.EXPAND)

        self.setTime(0, 0)

    def setLabel(self, labelType, timeVal):
        time = timeVal*self.stepSize # time in seconds
        timeLabel = getTimeLabel(time)
        self.type2label[labelType].SetLabel(timeLabel)

    def setTime(self, stepCount, stepSize):
        self.stepSize = stepSize
        self.slider.SetMin(0)
        maxVal = stepCount - 1
        self.slider.SetMax(maxVal)

        self.slider.SetTickFreq(math.floor(stepCount/self.MAX_TICK_COUNT))

        self.setLabel(self.LEFT_ID, 0)
        self.setLabel(self.RIGHT_ID, maxVal)
        self.setLabel(self.TOP_ID, self.slider.GetValue())

        self.Layout()

    def Enable(self, enable=True):
        self.slider.Enable(enable)

    def GetValue(self):
        return self.slider.GetValue()

    def SetValue(self, value):
        self.slider.SetValue(value)
        self.setLabel(self.TOP_ID, self.slider.GetValue())

    def onScrollChanged(self, event):
        self.setLabel(self.TOP_ID, self.slider.GetValue())
        e = event.Clone()
        e.SetEventType(wxEVT_TIMESLIDER_SCROLL_EVENT)
        self.GetEventHandler().ProcessEvent(e)

def getTimeLabel(timeInSeconds):
    isNegative = timeInSeconds < 0
    if isNegative:
        timeInSeconds = -timeInSeconds

    minutes = int(math.floor(timeInSeconds/60))
    seconds = int(math.floor(timeInSeconds - minutes*60))
    thousandsOfAsecond = int((timeInSeconds - minutes*60 - seconds)*10000)
    if isNegative:
        minutes = -float(minutes)

    return '{:-4.0f}:{:02.0f}.{:02.0f}'.format(minutes, seconds, thousandsOfAsecond)

class CheckListBox(wx.CheckListBox):

    # elements is a list whereby each elements is a tuple (ID, label)

    def __init__(self, items, parent, *args, **kwargs):
        wx.CheckListBox.__init__(self, parent, choices=[item[1] for item in items], *args, **kwargs)
        self.IDs = [item[0] for item in items]

    def addItems(self, items):
        if len(items) == 2 and not isinstance(items[0], (list,tuple)):
            items = [items]
        self.AppendItems([item[1] for item in items])
        self.IDs += [item[0] for item in items]

    def removeItem(self, label, ID):
        labels = self.GetItems()
        self.Delete(labels.index(label))
        self.IDs.remove(ID)

class ContextMenu(wx.Menu):

    def __init__(self, mousePos, *args, **kwargs):
        wx.Menu.__init__(self, *args, **kwargs)
        self.mousePos = mousePos