""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging

from matplotlib import cbook
import matplotlib
from matplotlib.backend_bases import NavigationToolbar2, _Mode
from matplotlib.backends.backend_wx import _load_bitmap
import wx

from NOMAD_results_gui.results_gui.constants import getConstant, IMAGE_DIR_PATH


matplotlib.use('WXAgg')

HOME_ID = 'home'
BACK_ID = 'back'
FORWARD_ID = 'forward'
PAN_ID = 'pan'
ZOOM_ID = 'zoom'
GRID_ID = 'grid'
MINOR_GRID_ID = 'minor_grid'
SAVE_FIG_ID = 'saveFig'
SAVE_AXES_ID = 'saveAxes'

TOOL_ITEMS = {
    HOME_ID:{'label':'Home', 'toolTip':'Reset original view', 'imageFl':'home', 'kind':wx.ITEM_NORMAL},
    BACK_ID:{'label':'Back', 'toolTip':'Back to  previous view', 'imageFl':'back', 'kind':wx.ITEM_NORMAL},
    FORWARD_ID:{'label':'Forward', 'toolTip':'Forward to next view', 'imageFl':'forward', 'kind':wx.ITEM_NORMAL},
    PAN_ID:{'label':'Pan', 'toolTip':'Pan axes with left mouse, zoom with right', 'imageFl':'move', 'kind':wx.ITEM_CHECK},
    ZOOM_ID:{'label':'Zoom', 'toolTip':'Zoom to rectangle', 'imageFl':'zoom_to_rect', 'kind':wx.ITEM_CHECK},
    GRID_ID:{'label':'Grid', 'toolTip':'Show grid', 'imageFl':'grid.png', 'kind':wx.ITEM_CHECK},
    #MINOR_GRID_ID:{'label':'Minor grid', 'toolTip':'Show minor grid', 'imageFl':None, 'kind':wx.ITEM_CHECK},
    SAVE_FIG_ID:{'label':'Save the figure', 'toolTip':'Save the figure to file', 'imageFl':'filesave_fig.png', 'kind':wx.ITEM_NORMAL},
    SAVE_AXES_ID:{'label':'Save the axes', 'toolTip':'Save the axes to file', 'imageFl':'filesave_ax.png', 'kind':wx.ITEM_NORMAL},
    }

#TOOL_ORDER = (HOME_ID, BACK_ID, FORWARD_ID, None, PAN_ID, ZOOM_ID)
TOOL_ORDER = (HOME_ID, BACK_ID, FORWARD_ID, None, PAN_ID, ZOOM_ID, None, GRID_ID, SAVE_FIG_ID, SAVE_AXES_ID) #MINOR_GRID_ID)

class NavigationToolbar():
    '''
    classdocs
    '''

    def __init__(self, parent, parentFrame, canvas):
        '''
        Constructor
        '''
        self.wxToolBar = wx.ToolBar(parent)
        self.mplToolBar = NavigationToolbarBase(canvas, parentFrame, self)
        self.initToolBar()

        self.parent = parent

    def initToolBar(self):
        self.toolID2wxID = {}
        imageDirPath = getConstant(IMAGE_DIR_PATH)
        from NOMAD_results_gui.results_gui.animation_tab import getScaledBitmapImage

        for toolID in TOOL_ORDER:
            if toolID is None:
                self.wxToolBar.AddSeparator()
                continue
            self.toolID2wxID[toolID] = wx.NewId()

            if TOOL_ITEMS[toolID]['imageFl'].endswith('.png'):
                bitmap = getScaledBitmapImage(str(imageDirPath.joinpath(TOOL_ITEMS[toolID]['imageFl'])))
            else:
                bitmap = _load_bitmap(TOOL_ITEMS[toolID]['imageFl'] + '.png')


            tool = self.wxToolBar.AddTool(self.toolID2wxID[toolID], TOOL_ITEMS[toolID]['label'],
                                     bitmap, TOOL_ITEMS[toolID]['toolTip'], TOOL_ITEMS[toolID]['kind'])
            self.wxToolBar.Bind(wx.EVT_TOOL, self.onToolClick, tool)

        self.wxID2toolID = {value:key for key, value in self.toolID2wxID.items()}
        self.wxToolBar.Realize()

    def onToolClick(self, event):
        toolID = self.wxID2toolID[event.Id]
        canvasPanel = self.parent
        mousePos = wx.GetMousePosition()
        args = ()

        self.executeAction(toolID, canvasPanel, mousePos, *args)

    def isActive(self, toolID):
        return self.mplToolBar.isActive(toolID)

    def executeAction(self, toolID, canvasPanel, mousePos, *args):
        if toolID == PAN_ID:
            if self.isActive(PAN_ID):
                self.wxToolBar.ToggleTool(self.toolID2wxID[PAN_ID], False)
            else:
                self.wxToolBar.ToggleTool(self.toolID2wxID[ZOOM_ID], False)
                self.wxToolBar.ToggleTool(self.toolID2wxID[PAN_ID], True)
        elif toolID == ZOOM_ID:
            if self.isActive(ZOOM_ID):
                self.wxToolBar.ToggleTool(self.toolID2wxID[ZOOM_ID], False)
            else:
                self.wxToolBar.ToggleTool(self.toolID2wxID[PAN_ID], False)
                self.wxToolBar.ToggleTool(self.toolID2wxID[ZOOM_ID], True)
        elif toolID == GRID_ID:
            if self.isActive(GRID_ID):
                self.wxToolBar.ToggleTool(self.toolID2wxID[GRID_ID], False)
            else:
                self.wxToolBar.ToggleTool(self.toolID2wxID[GRID_ID], True)

        self.mplToolBar.executeAction(toolID, canvasPanel, mousePos, *args)

    @property
    def gridOn(self):
        return self.mplToolBar.gridOn

class NavigationToolbarBase(NavigationToolbar2):

    log = logging.getLogger(__name__)

    # The overridden functions are copies of NavigationToolbar2Wx

    def __init__(self, canvas, parentFrame, parentToolBar):
        NavigationToolbar2.__init__(self, canvas)
        self.parentFrame = parentFrame
        self.parentToolBar = parentToolBar
        self._retinaFix = 'wxMac' in wx.PlatformInfo
        self.gridOn = False

    def isActive(self, toolID):
        if toolID == PAN_ID:
            return self.mode == _Mode.PAN
        elif toolID == ZOOM_ID:
            return self.mode == _Mode.ZOOM
        elif toolID == GRID_ID:
            return self.gridOn

    def executeAction(self, toolID, canvasPanel, mousePos, *args):
        curAxes = self.getCurAxes(mousePos)
        if curAxes is not None:
            axLabel = curAxes.get_label()
        else:
            axLabel = None

        self.log.debug('Ax label:{}'.format(axLabel))

        if toolID == HOME_ID:
            canvasPanel.home(axLabel)
        elif toolID == BACK_ID:
            self.back(*args)
        elif toolID == FORWARD_ID:
            self.forward(*args)
        elif toolID == PAN_ID:
            self.pan(*args)
        elif toolID == ZOOM_ID:
            self.zoom(*args)
        elif toolID == GRID_ID:
            onOff = not(self.gridOn)
            self.gridOn = onOff
            canvasPanel.gridOnOff(axLabel, self.gridOn)
        elif toolID == SAVE_FIG_ID:
            canvasPanel.saveFigure()
        elif toolID == SAVE_AXES_ID:
            canvasPanel.saveAxes(axLabel)

    def getCurAxes(self, mousePos):
        if self.canvas.mouse_grabber is None and mousePos is not None:
            mouseEvent = wx.MouseEvent()
            mouseScreenPos = self.canvas.ScreenToClient(mousePos)
            mouseMlpPos = wx.Point(mouseScreenPos[0], self.canvas.GetSize()[1] - mouseScreenPos[1])
            mouseEvent.SetPosition(mouseMlpPos)
            axes_list = [a for a in self.canvas.figure.get_axes()
                         if a.in_axes(mouseEvent)]
        elif mousePos is None:
            axes_list = []
        else:
            axes_list = [self.canvas.mouse_grabber]

        if axes_list:
            return cbook._topmost_artist(axes_list)

        return None

    def press_zoom(self, event):
        super().press_zoom(event)
        if self.mode.name == 'ZOOM':
            if not self._retinaFix:
                self._wxoverlay = wx.Overlay()
            else:
                if event.inaxes is not None:
                    self._savedRetinaImage = self.canvas.copy_from_bbox(
                        event.inaxes.bbox)
                    self._zoomStartX = event.xdata
                    self._zoomStartY = event.ydata
                    self._zoomAxes = event.inaxes

    def release_zoom(self, event):
        super().release_zoom(event)
        if self.mode.name == 'ZOOM':
            # When the mouse is released we reset the overlay and it
            # restores the former content to the window.
            if not self._retinaFix:
                self._wxoverlay.Reset()
                del self._wxoverlay
            else:
                del self._savedRetinaImage
                if self._prevZoomRect:
                    self._prevZoomRect.pop(0).remove()
                    self._prevZoomRect = None
                if self._zoomAxes:
                    self._zoomAxes = None

    def draw_rubberband(self, event, x0, y0, x1, y1):
        if self._retinaFix:  # On Macs, use the following code
            # wx.DCOverlay does not work properly on Retina displays.
            rubberBandColor = '#C0C0FF'
            if self._prevZoomRect:
                self._prevZoomRect.pop(0).remove()
            self.canvas.restore_region(self._savedRetinaImage)
            X0, X1 = self._zoomStartX, event.xdata
            Y0, Y1 = self._zoomStartY, event.ydata
            lineX = (X0, X0, X1, X1, X0)
            lineY = (Y0, Y1, Y1, Y0, Y0)
            self._prevZoomRect = self._zoomAxes.plot(
                lineX, lineY, '-', color=rubberBandColor)
            self._zoomAxes.draw_artist(self._prevZoomRect[0])
            self.canvas.blit(self._zoomAxes.bbox)
            return

        # Use an Overlay to draw a rubberband-like bounding box.

        dc = wx.ClientDC(self.canvas)
        odc = wx.DCOverlay(self._wxoverlay, dc)
        odc.Clear()

        # Mac's DC is already the same as a GCDC, and it causes
        # problems with the overlay if we try to use an actual
        # wx.GCDC so don't try it.
        if 'wxMac' not in wx.PlatformInfo:
            dc = wx.GCDC(dc)

        height = self.canvas.figure.bbox.height
        y1 = height - y1
        y0 = height - y0

        if y1 < y0:
            y0, y1 = y1, y0
        if x1 < x0:
            x0, x1 = x1, x0

        w = x1 - x0
        h = y1 - y0
        rect = wx.Rect(x0, y0, w, h)

        rubberBandColor = '#C0C0FF'  # or load from config?

        # Set a pen for the border
        color = wx.Colour(rubberBandColor)
        dc.SetPen(wx.Pen(color, 1))

        # use the same color, plus alpha for the brush
        r, g, b, _ = color.Get(True)
        color.Set(r, g, b, 0x60)
        dc.SetBrush(wx.Brush(color))
        dc.DrawRectangle(rect)

    def set_message(self, s):
        self.parentFrame.statusBar.PushStatusText(s)