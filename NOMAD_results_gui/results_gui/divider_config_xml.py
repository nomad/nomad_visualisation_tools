""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from pubsub import pub

from NOMAD_results_gui.axes_divider import GAPS, AUX_GAPS
from NOMAD_results_gui.xml_config_base.xml2config import getElementsValueWithAttr
from NOMAD_results_gui.results_gui.tab_controller import TAB_TYPES
from NOMAD_results_gui.results_gui.xml_config import XmlConfig
import xml.etree.ElementTree as ET


GAPS_PER_TAB = 'gapsPerTab'
TAB_TYPE = 'tabType'
GAP_EL = 'gap'
GAP_ID = 'gapID'

UPDATE_GAPS_MSG_ID = 'updateGaps'

class DividerConfig(XmlConfig):

    @staticmethod
    def getConfigFromDict(config):
        return config['gapsPerTabType']

    def getConfigDict(self):
        return self.resultsGui.getGapsPerTabType()

    @staticmethod
    def readConfigFromXml(config, collection):
        gapsPerTabType = {}
        for tabTypeXml in collection.getElementsByTagName(GAPS_PER_TAB):
            tabType = tabTypeXml.getAttribute(TAB_TYPE)
            if tabType not in TAB_TYPES:
                raise Exception('The tab type "{}" is unknown!'.format(tabType))

            if tabType in gapsPerTabType:
                raise Exception('The tab type "{}" already exists'.format(tabType))

            gaps = getElementsValueWithAttr(tabTypeXml, GAP_EL, GAP_ID, 'float')
            for gapKey in gaps.keys():
                if gapKey not in GAPS and gapKey not in AUX_GAPS:
                    raise Exception('The gap ID "{}" is unknown!'.format(gapKey))

            gapsPerTabType[tabType] = gaps

        config['gapsPerTabType'] = gapsPerTabType

    @staticmethod
    def addConfigDict2xml(rootEL, gapsPerTabType):
        for tabType, gaps in gapsPerTabType.items():
            tabEL = ET.SubElement(rootEL, GAPS_PER_TAB, attrib={TAB_TYPE:tabType})
            for gapID, gap in gaps.items():
                gapEl = ET.SubElement(tabEL, GAP_EL, attrib={GAP_ID:gapID})
                gapEl.text = getFloatValueStr(gap, 4)

    @staticmethod
    def getFile():
        return 'dividerConfig.xml'

    @staticmethod
    def sendPubMsg(config):
        pub.sendMessage(UPDATE_GAPS_MSG_ID, gapsPerTabType=config['gapsPerTabType'])

def getFloatValueStr(value, precision):
    formatString = '{{:.{:d}f}}'.format(precision)
    return formatString.format(value)
