""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

from matplotlib.legend import Legend
from matplotlib.lines import Line2D

from NOMAD_results_gui.axes_divider.data_ax import DataAx
import numpy as np


class MainDrawAxes(DataAx):
    '''
    classdocs
    '''

    def __init__(self, ID, xLabel, yLabel, *args, **kwargs):
        '''
        Constructor
        '''
        DataAx.__init__(self, ID, *args, **kwargs)
        self.xLabel = xLabel
        self.yLabel = yLabel
        self.dataLimX = [0,1]
        self.dataLimY = [0,1]
        self.gapFactorX = 0
        self.gapFactorY = 0

        self.legend = None
        self.isInHomeView = True

    def clean(self):
        self.ax.cla()
        self.legend = None

    def plotLine(self, x, y, **kwargs):
        line = Line2D(x, y, **kwargs)
        self.ax.add_artist(line)
        return line

    def setLims(self, dataLimX, dataLimY, gapFactorX=0, gapFactorY=0):
        xLim, yLim = self.calcLims(dataLimX, dataLimY, gapFactorX, gapFactorY)

        self.ax.set_xlim(xLim, emit=False)
        self.ax.set_ylim(yLim, emit=False)

        self.dataLimX = dataLimX
        self.dataLimY = dataLimY
        self.gapFactorX = gapFactorX
        self.gapFactorY = gapFactorY

        return {'x':xLim, 'y':yLim}

    def calcLims(self, dataLimX, dataLimY, gapFactorX, gapFactorY):
        if not isinstance(gapFactorX, (list, tuple)):
            gapFactorX = [gapFactorX,gapFactorX]
        if not isinstance(gapFactorY, (list, tuple)):
            gapFactorY = [gapFactorY,gapFactorY]

        xDiff = np.abs(dataLimX[1] - dataLimX[0])
        xLim = (dataLimX[0] - gapFactorX[0]*xDiff, dataLimX[1] + gapFactorX[1]*xDiff)

        yDiff = np.abs(dataLimY[1] - dataLimY[0])
        yLim = (dataLimY[0] - gapFactorY[0]*yDiff, dataLimY[1] + gapFactorY[1]*yDiff)

        if xLim[0] == xLim[1]:
            xLim = (-1e-3, 1e-3)

        if yLim[0] == yLim[1]:
            yLim = (-1e-3, 1e-3)

        return xLim, yLim

    def updateLims(self, add2Top=0, add2Bottom=0, add2Left=0, add2Right=0):
        dataLimX = (self.dataLimX[0] - add2Left, self.dataLimX[1] + add2Right)
        dataLimY = (self.dataLimY[0] - add2Bottom, self.dataLimY[1] + add2Top)

        xLim, yLim = self.calcLims(dataLimX, dataLimY, self.gapFactorX, self.gapFactorY)
        self.ax.set_xlim(xLim, emit=False)
        self.ax.set_ylim(yLim, emit=False)

    def onScrollZoom(self, scale_factor, xdata, ydata, emit=True):
        cur_xlim = self.ax.get_xlim()
        cur_ylim = self.ax.get_ylim()
        cur_xrange = (cur_xlim[1] - cur_xlim[0])*.5
        cur_yrange = (cur_ylim[1] - cur_ylim[0])*.5
        # set new limits
        self.ax.set_xlim([xdata - cur_xrange*scale_factor,
                     xdata + cur_xrange*scale_factor], emit=emit)
        self.ax.set_ylim([ydata - cur_yrange*scale_factor,
                     ydata + cur_yrange*scale_factor], emit=emit)


    def finalizePlot(self, showLegend, doRelim, dataLims, gapFactors_ii, aspect, gridOn=False, legendInfo=None):
        if doRelim:
            self.ax.relim()
        if showLegend:
            if isinstance(self.legend, Legend):
                self.legend.remove()

            if legendInfo is not None:
                self.legend = self.ax.legend(legendInfo['handles'], legendInfo['labels'], loc='best')
            else:
                self.legend = self.ax.legend(loc='best')
            self.legend.draggable()

        self.ax.set_aspect(aspect)
        newLims = self.setLims(dataLims['x'], dataLims['y'], gapFactors_ii['x'], gapFactors_ii['y'])
        self.ax.set_xlabel(self.xLabel)
        self.ax.set_ylabel(self.yLabel)

        if gridOn:
            self.ax.grid(gridOn)

        return newLims