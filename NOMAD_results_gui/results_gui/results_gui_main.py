""" 
Copyright (C) 2024 Martijn Sparnaaij - All Rights Reserved

This file is part of the NOMAD visualisation tools.

the NOMAD visualisation tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

the NOMAD visualisation tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the NOMAD visualisation tools. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging.config
from pathlib import Path
import sys

import wx

from NOMAD_results_gui.results_gui.gui import ResultsGui
from argparse import ArgumentError
from NOMAD_results_gui.results_gui import constants

def main(*args):
    appPath = Path(args[0])
    if not appPath.is_dir():
        raise ArgumentError('A valid app path must be provided')

    logger = logging.getLogger(__name__)

    constants.initConstants(appPath, 'Results Gui')

    try:
        app = wx.App(False)
        ResultsGui(appPath)
        logger.info('Created resultsGui instance')
        app.MainLoop()
    except Exception as err:
        logger.critical(str(err.args[0]), exc_info=True)

if __name__ == '__main__':
    main(*sys.argv[1:])